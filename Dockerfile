FROM aporeba/alma9-docker-php-nginx:latest
 
USER root
# Add needed directories for Athena
RUN mkdir /data && chown -R nobody.nobody /data && chmod -R 777 /data
RUN mkdir /\.asetup && chown -R nobody.nobody /\.asetup && chmod -R 777 /\.asetup

# Add CERN's EPEL
RUN dnf remove -y epel-release
RUN dnf remove -y remi
RUN dnf install -y https://linuxsoft.cern.ch/cern/alma/9/CERN/x86_64/Packages/e/epel-release-9-5.al9.cern.noarch.rpm

# Install auth-get-sso-cookie & cern certificates
RUN dnf install -y https://linuxsoft.cern.ch/internal/repos/authz9al-stable/x86_64/os/Packages/auth-get-sso-cookie-2.2.1-1.el9.noarch.rpm
RUN dnf install -y https://linuxsoft.cern.ch/cern/alma/9/CERN/x86_64/Packages/c/CERN-CA-certs-20230421-1.al9.cern.noarch.rpm

# Install dependencies for Athena as per /cvmfs/atlas.cern.ch/repo/containers/def/singularity/atlas-centos7.def
RUN echo "Installing the packages inside the container"
RUN rpm --rebuilddb
#; exit 0
#RUN mv /var/lib/rpmrebuilddb.* /var/lib/rpm
RUN dnf install -y vim-minimal
RUN echo "Installing Development tools"
RUN dnf groupinstall -y "Development Tools"

RUN echo "Installing basic packages"
RUN dnf install -y vim-enhanced man-db wget chrony autofs nfs-utils git perl perl-Data-Dumper automake autoconf libtool gcc gcc-c++ glibc flex make autofs
RUN dnf install -y supervisor

RUN echo "Installing Atlas packages"
RUN dnf install -y time alsa-lib alsa-lib audit-libs autoconf automake bzip2-devel bzip2-libs cracklib cyrus-sasl-lib  e2fsprogs-libs elfutils-libelf expat fontconfig fontconfig-devel freetype freetype-devel freetype-devel gdbm giflib giflib glib2 glibc glibc-devel glibmm24 gmp keyutils-libs krb5-libs libICE libICE libSM libSM libX11 libX11-devel libX11-devel libXau libXau-devel libXau-devel libXcursor libXdamage libXdamage-devel libXdamage-devel libXdmcp libXdmcp libXdmcp-devel libXdmcp-devel libXext libXext-devel libXext-devel libXfixes libXfixes-devel libXfixes-devel libXft libXft-devel libXi libXinerama libXmu libXmu libXp libXp libXpm libXpm libXpm-devel libXrandr libXrender libXrender-devel libXt libXt libXtst libXtst libXxf86vm libXxf86vm libXxf86vm-devel libXxf86vm-devel libaio libcom_err libdrm libdrm-devel libdrm-devel libgcc libgfortran libgfortran libidn libjpeg-turbo libpciaccess libpng libpng-devel libpng-devel libselinux libsepol libsigc++20 libstdc++ libtiff libtool libtool-ltdl libtool-ltdl libuuid libuuid-devel libuuid-devel libxcb libxcb-devel libxcb-devel libxml2 libxml2-devel libxml2-devel libxslt mesa-dri-drivers mesa-dri-drivers mesa-dri-filesystem mesa-dri-filesystem mesa-libGL mesa-libGL mesa-libGL-devel mesa-libGL-devel mesa-libGLU mesa-libGLU mesa-libGLU-devel mesa-libGLU-devel ncurses-devel ncurses-libs nspr nss nss-softokn nss-softokn-freebl nss-util openldap openmotif openmotif openssl pam perl-CGI perl-ExtUtils-Embed perl-ExtUtils-MakeMaker perl-ExtUtils-ParseXS perl-Test-Harness perl-devel procmail readline sqlite tcl tcsh tk tk xorg-x11-proto-devel zlib zlib-devel zsh expat libblkid libuuid nss nss-sysinit nss-tools nss-util util-linux-ng
# not available for alma 9
# compat-db compat-db compat-db42 compat-db42 compat-db43 compat-db43 compat-expat1 compat-expat1 compat-glibc compat-glibc-headers compat-libf2c-34 compat-libf2c-34 compat-libgfortran-41 compat-libgfortran-41 compat-libstdc++-33 compat-libstdc++-33 compat-libtermcap compat-libtermcap compat-openldap compat-openldap compat-readline5 db4 db4-cxx db4-devel gamin mesa-dri1-drivers mesa-private-llvm openmotif22 openssl098e Updating

# Additional packages for belle
RUN dnf install -y binutils-devel python-devel
RUN dnf install -y wget
RUN wget https://linuxsoft.cern.ch/wlcg/el9/x86_64/HEP_OSlibs-9.1.2-2.el9.x86_64.rpm
RUN dnf localinstall -y HEP_OSlibs-9.1.2-2.el9.x86_64.rpm
RUN rm -f HEP_OSlibs*rpm
RUN dnf install -y xrootd-client
RUN dnf install -y http://linuxsoft.cern.ch/wlcg/el9/x86_64/wlcg-repo-1.0.0-1.el9.noarch.rpm
#RUN dnf install -y gfal2-all gfal2-util
RUN dnf update -y

# Create dir for SLES11 systems
RUN mkdir -p /scratch /gpfs/work /gpfs/scratch /cvmfs /etc/grid-security/certificates

# Copy web files
COPY . /var/www/html

# Chown needed directories for TrigCostWeb
RUN chown -R 1001 /var/www/html && chown -R 1001 /run && chown -R 1001 /var/lib/nginx && chown -R 1001 /var/log/nginx
RUN chmod -R 777 /var/lib/nginx && chmod -R 777 /var/log/nginx && chmod -R 777 /var/log/php-fpm && chmod 777 -R /run && chmod -R 777 /var/www/html  && chmod -R 777 /var/log/supervisor

# Change timezone
RUN cp /etc/localtime /root/old.timezone
RUN rm /etc/localtime
RUN ln -s /usr/share/zoneinfo/Europe/Zurich /etc/localtime

USER 1001

# Setup ENV for auth-get-sso-cookie, so it knows where to pickup ticket from
ENV KRB5CCNAME=/tmp/krb5cc_1010050000

EXPOSE 8080

CMD ["./run.sh"]
