#!/bin/bash

exec >> /data/cronManual.log 2>&1

menuLocation="/eos/atlas/atlascerngroupdisk/trig-rates/rate_json/2025"

# Uncomment to debug
# echo "Execute] Rates directory is: $1"
# echo "Execute] Release is: $2"

if [ -z "$1" ]; then
  echo "Execute] No rates prediction directory path was not supplied, exit."
  exit
elif [ -z "$2" ]; then
  echo "Execute] No release supplied, exit."
  exit
fi

ratesFile="$1/rates.json"
metadataFile="$1/metadata.json"
release="$2"

runNumberCommand="import sys, json; children=json.load(open(\"${metadataFile}\"))[\"children\"]; runNumber=[childDict[\"RunNumber\"] for childDict in children if \"RunNumber\" in childDict]; print(runNumber[0])"
runNumber=$(curl -s 'https://api.github.com/users/lambda' | tac | \
    python -c "${runNumberCommand}")

if [ -z "$runNumber" ]; then
  echo "Execute] Run number was not found in the metadata file, exit."
  exit
fi

smkCommand="import sys, json; children=json.load(open(\"${metadataFile}\"))[\"children\"]; smk=[childDict[\"SMK\"] for childDict in children if \"SMK\" in childDict]; print(smk[0])"
smk=$(curl -s 'https://api.github.com/users/lambda' | tac | \
    python -c "${smkCommand}")

if [ -z "$smk" ]; then
  echo "Execute] Super Master Key was not found in the metadata file, exit."
  exit
fi

# Read the menu name from the database
source /cvmfs/atlas.cern.ch/repo/sw/tdaq/tools/cmake_tdaq/bin/cm_setup.sh tdaq-11-02-00
export TRIGGER_DB_ART=1
cp /data/dblookup.xml .
cp /data/authentication.xml .
menuType=$(listMenusBySMK.py --smk $smk --dbalias "TRIGGERDBREPR_RUN3" | grep -m 1 -o "([A-Za-z0-9_]*" | cut -c 2-)

ratesName="rates_${release}_${runNumber}.json"

echo "Copying $ratesFile to $menuLocation/$menuType/$ratesName"

cp $ratesFile "$menuLocation/$menuType/$ratesName"
