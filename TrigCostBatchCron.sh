#!/bin/bash
exec >> /data/cron.log 2>&1

#setup eos
# export EOS_MGM_URL=root://eosatlas.cern.ch
# export HOME=/opt/trig-cost-sw
# Authenticate
# kinit -5 attradm@CERN.CH -k -t $HOME/keytabs/attradm.keytab

# This bash script looks in predetermined locations for new input data and runs TrigCost over it.
# It should be run by a CRON job every ~30m
echo "###### Executing TrigCostBatchCron on `hostname` - `TZ='Europe/Zurich' date`"

# No longer
# Exit if the program is still running from another instance
#if [ "$(/sbin/pidof RunTrigCostD3PD)" ]
#then
#  echo "RunTrigCostD3PD is still running - won't update SW."
#else 
#  # Check SW
#  /var/www/html/TrigCostBatchUpdateSoftware.sh
#fi

################################################## ##################################################
# Config search locations

# SEARCH_LOCATION[0]="/eos/atlas/atlastier0/tzero/prod/caf/HLT/data24_comm/calibration_CostMonitoring/"
# OUTPUT_LOCATION[0]="/data/data24_cos/"
# TAG_LOCATION[0]="data24-cos"
# DETAILS_LOCATION[0]="P1 commisioning run."

SEARCH_LOCATION[0]="/eos/atlas/atlastier0/tzero/prod/caf/HLT/data24_13p6TeV/calibration_CostMonitoring/"
OUTPUT_LOCATION[0]="/data/data24_13p6TeV/"
TAG_LOCATION[0]="data24-13p6TeV"
DETAILS_LOCATION[0]="P1 pp run."

SEARCH_LOCATION[1]="/eos/atlas/atlastier0/tzero/prod/caf/HLT/data24_hi/calibration_CostMonitoring/"
OUTPUT_LOCATION[1]="/data/data24_hi/"
TAG_LOCATION[1]="data24-hi"
DETAILS_LOCATION[1]="P1 HI run."

# LS 2 directory
# SEARCH_LOCATION[0]="/eos/atlas/atlastier0/tzero/prod/caf/HLT/data21_calib/calibration_CostMonitoring/"
# OUTPUT_LOCATION[0]="/data/Technical_and_M_weeks/"
# TAG_LOCATION[0]="data21-calib"
# DETAILS_LOCATION[0]="Test data"

# Run 2 directories
# SEARCH_LOCATION[0]="/eos/atlas/atlastier0/tzero/caf/HLT/temp/data18_hi/calibration_CostMonitoring/"
# OUTPUT_LOCATION[0]="/data/data18_hi/"
# TAG_LOCATION[0]="data18-hi"
# DETAILS_LOCATION[0]="P1 PbPb run with %e HLT events in %l lumi blocks"

# SEARCH_LOCATION[1]="/eos/atlas/atlastier0/tzero/caf/HLT/temp/data18_cos/calibration_CostMonitoring/"
# OUTPUT_LOCATION[1]="/data/data18_cos/"
# TAG_LOCATION[1]="data18-cos"
# DETAILS_LOCATION[1]="P1 cosmic or commisioning run with %e HLT events in %l lumi block"

# Config file of already processed locations
ALREADY_PROCESSED_FILELIST=/data/processed.txt
FILELIST_A=/data/trig-cost-sw/fileList.txt
FILELIST_B=/data/trig-cost-sw/fileListB.txt
################################################## ##################################################

# Loop over locations
NLOCATIONS=${#SEARCH_LOCATION[@]}
COUNTER=0
#echo "Investigating $NLOCATIONS Locations:"
for LOCATION in "${SEARCH_LOCATION[@]}"; do # Look at each search location
  #echo "-- Searching Location: $LOCATION"
  # eos find -d $LOCATION | grep "\.merge\."
  # find $LOCATION -type d -name "*.merge.*"
  for DIR in `find $LOCATION -type d -name "*.merge.NTUP_TRIGCOST.c*_m*"`; do # Look at each dir in a search location, for most recent first "| sort -r"
    #echo "-- -- Found Directory: $DIR"
    # DIRFIX=`echo $DIR | sed -e 's/path=//g'`
    HAVE_PROCESSED=false
    while read PROCESSED; do # Check each of these against the list of processed directories
      if [ "$DIR" == "$PROCESSED" ]; then
        # We have already processed this directory
        #echo "-- -- -- Match found in already-proccessed list."
        HAVE_PROCESSED=true
        break
      fi
    done < $ALREADY_PROCESSED_FILELIST
    # Did we find that we had processed this directory?
    if [ "$HAVE_PROCESSED" = true ]; then
      #echo "-- -- -- We have already processed $DIR, continue."
      continue
    fi
    # Does this new directory have root files?
    # Get a file list
    # eos find -f $DIRFIX > $FILELIST_A
    find $DIR -type f > $FILELIST_A
    NROOT=`cat $FILELIST_A | wc -l`
    echo "-- -- Directory contains $NROOT files."
    if [ "$NROOT" -eq "0" ]; then
      echo "-- -- No files, continue."
      continue;
    fi
    # Is this file list the same as the one we saw 30m ago?
    if ! cmp $FILELIST_A $FILELIST_B >/dev/null 2>&1
    then
      echo "-- -- FilelistA / FilelistB missmatch. A -> B and wait 30m. (printing path to one file)"
      cp $FILELIST_A $FILELIST_B
      tail -n 1 $FILELIST_B
      # We exit rather than continuing as we know there is a run in progress and we want to check it out soon 
      exit
    fi
    # New directory! Start processing it
    TO_PROCESS=$DIR
    TAG=${TAG_LOCATION[$COUNTER]}
    DETAILS=${DETAILS_LOCATION[$COUNTER]}
    OUTPUT=${OUTPUT_LOCATION[$COUNTER]}
    break;
  done
  if [ ! -z "$TO_PROCESS" ]; then # If TO_PROCESS has been set then break outer loop
    break
  fi
  let COUNTER=COUNTER+1
done

if [ -z "$TO_PROCESS" ]; then # If TO_PROCESS has *not* been set then nothing new to process
  echo "No new directories to process. Exit."
  exit
fi

echo "Will Process this directory: $TO_PROCESS"
echo "With FileList: $FILELIST_A"
echo "With this tag: $TAG"
echo "And these details: $DETAILS"
echo "And this output directory: $OUTPUT"
echo $TO_PROCESS >> $ALREADY_PROCESSED_FILELIST

/var/www/html/TrigCostBatchExecute.sh $FILELIST_A $OUTPUT "$TAG" "$DETAILS"

echo "Removing processed data from EOS - SUSPENDED FOR NOW"
# Authenticate
#kinit -5 attradm@CERN.CH -k -t $HOME/keytabs/attradm.keytab
#eos rm -r $TO_PROCESS
#touch /var/www/html/breakSleep

echo "Processing done. Exit."
