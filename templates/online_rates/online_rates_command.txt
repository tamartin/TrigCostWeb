echo ""
echo ""
echo "Starting the script" "$(date)"

export SITE_NAME=CERN-PROD
export REPO=/cvmfs/atlas.cern.ch/repo
export ATLAS_LOCAL_ROOT_BASE=$REPO/ATLASLocalRootBase
source /cvmfs/atlas.cern.ch/repo/sw/tdaq/tools/cmake_tdaq/bin/cm_setup.sh tdaq-11-02-00 # libpbeastpy library
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
asetup !!ATHENA_RELEASE!!
export PBEAST_SERVER_SSO_SETUP_TYPE=AutoUpdateKerberos # pbeast authentication

echo "Running the processing"
RatesAnalysisOnlineProcessing.py --runNumber !!RUN_NUMBER!! --lbStart !!START_LB!! --lbEnd !!END_LB!!

echo "Moving the output files"
cp -r costMonitoring_OnlineTRPRates*!!RUN_NUMBER!! !!OUTPUT_LOCATION!!
rm -rf costMonitoring_OnlineTRPRates*!!RUN_NUMBER!!
echo "Files moved! End of processing"