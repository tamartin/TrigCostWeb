## Templates for the ProcessingRequest page

The page consists of three views: 1. Unvalidated form 2. Validated form 3. Submit form

Unvalidated form contains just inputf for user to pass values to be used in postprocessing script.

Validated form contains two child templates, validated cost command and validated rates command. In one processing request user can request just rates, just cost or both - because of that the children templates can be included or not.

Submit form, similarly to validated form, contains two children templates, with result of scheduling rates and/or cost and list of scrpit input files. Additionally the result of saving the script can be successful and not (therefore there are two more templates).


Schematic templates structure:
```
Unvalidated Form (processing_request_unvalidated_form.html)

|
V

Validated form (processing_request_validated_form.html)
    Cost command (processing_request_validated_cost.html)
    Rates command (processing_request_validated_rates.html)

|
v

Submit form (processing_request_submit_form.html)
    Cost container with result of submitting (processing_request_submit_details.html)
        Inputs to the processing script (processing_request_submit_inputs.html)
        Result of saving script (processing_request_save_script_success.html or processing_request_save_script_failure.html)
    Rates container with result of submitting (processing_request_submit_details.html)
        Inputs to the processing script (processing_request_submit_inputs.html)
        Result of saving script (processing_request_save_script_success.html or processing_request_save_script_failure.html)
```


Moreover, there is additional set of templates to generate the processing script, in the `command` subdirectory.
```
Command base
    Commands to execute on processing success (separate for rates and cost)
    Commands to execute on processing failure
```