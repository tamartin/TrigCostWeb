## Templates for the DisplayRuns page

Schematic templates structure:
```
Container for all runs (display_runs_view)
    Container for subdirectory (display_runs_subdirectory)
        Run header (with button to hide/unhide runs or simple) (display_runs_header_btn/display_runs_header_simple)
        Table with runs within subdirectory (display_runs_runs_table)
            Row in the table with details of run (display_runs_runs_table_row)
                Buttons to edit metadata and hide/unhide run (available only for experts) (display_runs_runs_table_row_buttons)
            ...
    ...
```

One view can contain many subdirectories, one table can contain many rows