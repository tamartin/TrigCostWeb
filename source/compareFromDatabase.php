<?php 
// Set of helper functions supporting saving and reading table comparisons from the database


class compareDB extends SQLite3 {
    function __construct() {
      $exists = is_readable("./data/ratesPagesSQLite.db");
      $this->open("./data/ratesPagesSQLite.db");
      if ($exists == FALSE) {
        $sql =<<<EOF
          CREATE TABLE LINKS
          (ID INTEGER PRIMARY KEY   AUTOINCREMENT,
          KEYVALUE        TEXT   NOT NULL,
          COMPA           TEXT   NOT NULL,
          COMPB           TEXT   NOT NULL,
          LUMI            TEST   NOT NULL);
  EOF;
        $ret = $this->exec($sql);
        if(!$ret) echo $this->lastErrorMsg();
      }
    }
  }

  

function checkIfInDatabase($saveKey) {
    $db = new compareDB();
    if (!$db) echo $db->lastErrorMsg();
    if ($saveKey != NULL && $db) {
      $sql = "SELECT * from LINKS where KEYVALUE='{$saveKey}'";
      //echo "<p>Executing {$sql} </p>";
      $ret = $db->query($sql);
      $row = $ret->fetchArray(SQLITE3_ASSOC);
      return $row;
    }
    return NULL;
}

function addToDB($saveKey, $compA, $compB, $lumi) {
    $db = new compareDB();
    if (!$db) echo $db->lastErrorMsg();
    if ($saveKey == NULL && $db) {
        $sql = "SELECT * from LINKS where COMPA='{$compA}' AND COMPB='{$compB}' AND LUMI='{$lumi}'";
        #echo "<p>Executing {$sql} </p>";
        $ret = $db->query($sql);
        $row = $ret->fetchArray(SQLITE3_ASSOC);
        if ($row === FALSE) { // We did not have a DB entry, create one
          $saveKey = uniqid();
          $sql = "INSERT INTO LINKS (KEYVALUE,COMPA,COMPB,LUMI) VALUES ('{$saveKey}', '{$compA}', '{$compB}', '{$lumi}')";
          #echo "<p>Executing {$sql} </p>";
          $ret = $db->exec($sql);
          if (!$ret) {
            echo $db->lastErrorMsg();
            $saveKey = NULL;
          }
        } else { // We did have a DB entry, get the link
          $saveKey = $row['KEYVALUE'];
        }
    }

    return $saveKey;
}

?>