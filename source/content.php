<?php 

/* Print out home page links to directories and to user's data*/
function content_homePage() {
  global $dir, $dataDirs2015, $dataDirs2016, $dataDirs2017, $dataDirs2018, $dataDirs2020, $dataDirs2022, $dataDirs2023, $dataDirs2024, $dataDirs2025, $USERNAME, $USER, $addLink;

  function generateStandardDirectoryList($dataDir){
    $dirList = "";
    foreach ($dataDir as $theDirectory => $symLinkLocation) {
      $dirList .= "  <li><a href='" . getLinkDir($theDirectory) . "'>{$theDirectory}</a></li>\n";
    }
    return $dirList;
  }

  $homePageTemplate = new TemplateWrapper("home_page/home_page.html");

  if ($_SERVER["HTTP_HOST"] == "atlas-trig-cost.web.cern.ch") {
    $matomoTemplate = new TemplateWrapper("home_page/matomo.html");
    $matomoTemplate->setParam("USER_ID", sanitise($_SERVER['HTTP_X_FORWARDED_PREFERRED_USERNAME']));
    $homePageTemplate->setChild("MATOMO", $matomoTemplate);
  } else {
    $homePageTemplate->setParam("MATOMO", "");
  }
  $runs2025Template = new TemplateWrapper("home_page/runs_list.html");
  $runs2025Template->setParams(array("LIST_NAME" => "2025_list", "LIST_TITLE" => "Available 2025 Directories:", "DIRECTORY_LIST" => generateStandardDirectoryList($dataDirs2025),  "COMMENT" => ""));
  $homePageTemplate->setChild("2025_RUNS_LIST", $runs2025Template);

  $runs2024Template = new TemplateWrapper("home_page/runs_list.html");
  $runs2024Template->setParams(array("LIST_NAME" => "2024_list", "LIST_TITLE" => "Available 2024 Directories:", "DIRECTORY_LIST" => generateStandardDirectoryList($dataDirs2024),  "COMMENT" => ""));
  $homePageTemplate->setChild("2024_RUNS_LIST", $runs2024Template);

  $runs2023Template = new TemplateWrapper("home_page/runs_list.html");
  $runs2023Template->setParams(array("LIST_NAME" => "2023_list", "LIST_TITLE" => "Available 2023 Directories:", "DIRECTORY_LIST" => generateStandardDirectoryList($dataDirs2023),  "COMMENT" => ""));
  $homePageTemplate->setChild("2023_RUNS_LIST", $runs2023Template);


  $runs2022Template = new TemplateWrapper("home_page/runs_list.html");
  $runs2022Template->setParams(array("LIST_NAME" => "2022_list", "LIST_TITLE" => "Available 2022 Directories:", "DIRECTORY_LIST" => generateStandardDirectoryList($dataDirs2022),  "COMMENT" => ""));
  $homePageTemplate->setChild("2022_RUNS_LIST", $runs2022Template);


  $runsLS2Template = new TemplateWrapper("home_page/runs_list.html");
  $runsLS2Template->setParams(array("LIST_NAME" => "ls2_list", "LIST_TITLE" => "Available Long Shutdown 2 Directories", "DIRECTORY_LIST" => generateStandardDirectoryList($dataDirs2020),  "COMMENT" => ""));
  $homePageTemplate->setChild("LS2_RUNS_LIST", $runsLS2Template);


  $runs2018Template = new TemplateWrapper("home_page/runs_list.html");
  $dirList = "";
  foreach ($dataDirs2018 as $theDirectory => $symLinkLocation) {
    $dirList .= "  <li><a href='" . getLinkDir($theDirectory) . "'>{$theDirectory}</a></li>\n";
  }
  $runs2018Template->setParams(array("LIST_NAME" => "2018_list", "LIST_TITLE" => "Available 2018 Directories", "DIRECTORY_LIST" => generateStandardDirectoryList($dataDirs2018),  "COMMENT" => ""));
  $homePageTemplate->setChild("2018_RUNS_LIST", $runs2018Template);


  $runs2017Template = new TemplateWrapper("home_page/runs_list.html");
  $runs2017Template->setParams(array("LIST_NAME" => "2017_list", "LIST_TITLE" => "Available 2017 Directories", "DIRECTORY_LIST" => generateStandardDirectoryList($dataDirs2017),  "COMMENT" => ""));
  $homePageTemplate->setChild("2017_RUNS_LIST", $runs2017Template);


  $runs2016Template = new TemplateWrapper("home_page/runs_list.html");
  $runs2016Template->setParams(array("LIST_NAME" => "2016_list", "LIST_TITLE" => "Available 2016 Directories", "DIRECTORY_LIST" => generateStandardDirectoryList($dataDirs2016),  "COMMENT" => ""));
  $homePageTemplate->setChild("2016_RUNS_LIST", $runs2016Template);


  $runs2015Template = new TemplateWrapper("home_page/runs_list.html");
  $runs2015Template->setParams(array("LIST_NAME" => "2015_list", "LIST_TITLE" => "Available 2015 Directories", "DIRECTORY_LIST" => generateStandardDirectoryList($dataDirs2015),  "COMMENT" => ""));
  $homePageTemplate->setChild("2015_RUNS_LIST", $runs2015Template);

  if ( is_link("data/" . $USERNAME) == true ) { // User has a simlink
    $linkLoc = readlink("data/" . $USERNAME);
    $nRunsUser = checkRun($USERNAME);

    $userLinkTemplate = new TemplateWrapper("home_page/user_data_linked.html");
    $userLinkTemplate->setParams(array("USER" => $USER, "USERNAME" => $USERNAME, "USER_SYMLINK" => $linkLoc,
                                      "N_RUNS_USER" => $nRunsUser, "USER_LINK" => getLinkDir($USERNAME)));

    $homePageTemplate->setChild("USER_COST_DATA", $userLinkTemplate);
  } else { // User does not have a simlink
    if (isset($addLink)) {
      $boxText = $addLink; // If the user messed up the prev. link.
    } else {
      $boxText = "Enter public /cvmfs or /eos location";
    }

    $userLinkTemplate = new TemplateWrapper("home_page/user_add_link.html");
    $userLinkTemplate->setParams(array("USER" => $USER, "BOX_TEXT" => $boxText));
    $homePageTemplate->setChild("USER_COST_DATA", $userLinkTemplate);
  }

  $runsLegacyTemplate = new TemplateWrapper("home_page/runs_list.html");
  $dirList = "";
  $legacyRuns = array('0212967' => '-BegLB108-EndLB123', '0213155' => '', '0214777' => '-BegLB267-EndLB282');
  foreach ($legacyRuns as $legacyRunNum => $legacyRunRange)  {
    $dirList .= "  <li>Run Number:{$legacyRunNum} <a href='online/L2CostMonitoring/{$legacyRunNum}-L2CostMonitoring{$legacyRunRange}/'>L2CostMonitoring{$legacyRunRange}</a> <a href='online/EFCostMonitoring/{$legacyRunNum}-EFCostMonitoring{$legacyRunRange}/'>EFCostMonitoring{$legacyRunRange}</a>\n";
    $dirList .= "  <div class='subtitle'>Lookup in <a href='http://atlas-runquery.cern.ch/query.py?q=find+run+{$legacyRunNum}+/+show+all' class='linkExternal'>ATLAS Run Query</a></div>"; 
    $dirList .= "  </li>";
  }
  $runsLegacyTemplate->setParams(array("LIST_NAME" => "legacy_list", "LIST_TITLE" => "Legacy Data", "DIRECTORY_LIST" => $dirList,
                                        "COMMENT" => "<p>The majority of legacy run-1 data is located in the <a href='online'><strong>online</strong></a> directory in compressed archives. A small number of runs remain uncompressed.</p>"));
  $homePageTemplate->setChild("LEGACY_RUNS_LIST", $runsLegacyTemplate);

  $homePageTemplate->render();
}



function content_displayRuns() {
  global $dir, $dirPostPatters, $showHidden, $TT_RTTPath, $ID_RTTPath, $P1_RTTPath, $P1_RTTPath_dev, $TT_RTTPath_T0, $TT_RTTPath_mc16a,  $TT_RTTPath_dev;

  function getHeaderWithBtn($title) {
    global $dir, $showHidden;
    
    $headerTemplate = new TemplateWrapper("display_runs/display_runs_header_btn.html");
    $headerTemplate->setParams(array("TITLE" => $title, "DIR" => $dir));
    if ($showHidden) { // add option to hide runs
      $headerTemplate->setParams(array("ACTION" => "hideHidden", "BTN_VALUE" => "Hide Runs"));
    } else {
      $headerTemplate->setParams(array("ACTION" => "showHidden", "BTN_VALUE" => "Show Hidden Runs"));
    }

    return $headerTemplate;
  }

  if (isset($_POST['action']) and $_POST['action'] == "hideRun") {
    hideRun($_POST['runName']);
    $showHidden = filter_var($_POST['showHiddenRuns'], FILTER_VALIDATE_BOOLEAN); 
  }

  if (isset($_POST['action']) and $_POST['action'] == "unhideRun") {
    unhideRun($_POST['runName']);
    $showHidden = filter_var($_POST['showHiddenRuns'], FILTER_VALIDATE_BOOLEAN);
  }

  if (isset($_POST['action']) and $_POST['action'] == "showHidden") {
    $showHidden = true;
  }

  if (isset($_POST['action']) and $_POST['action'] == "hideHidden") {
    $showHidden = false;
  }

  if (isset($_POST['action']) and $_POST['action'] == "saveMetadata") {
    include_once "source/EditMetadata.php";
    saveDetails();
  }

  if ($dir == "TriggerTest-RTT") {
    $title = "Displaying {$dir} data from '<span class='codePara'>{$TT_RTTPath}</span>'";
  } else if ($dir == "ID-RTT") {
    $title = "Displaying {$dir} data from '<span class='codePara'>{$ID_RTTPath}</span>'";
  } else if ($dir == "TrigP1Test-RTT") {
    $title = "Displaying {$dir} data from '<span class='codePara'>{$P1_RTTPath}</span>'";
  } else if ($dir == "TrigP1Test-RTT-dev") {
    $title = "Displaying {$dir} data from '<span class='codePara'>{$P1_RTTPath_dev}</span>'";
  } else if ($dir == "TrigP1Test-RTT-mc16a") {
    $title = "Displaying {$dir} data from '<span class='codePara'>{$P1_RTTPath_mc16a}</span>'";
  } else if ($dir == "TriggerTest-RTT-T0") {
    $title = "Displaying {$dir} data from '<span class='codePara'>{$TT_RTTPath_T0}</span>'";
  } else if ($dir == "TriggerTest-RTT-dev") {
    $title = "Displaying {$dir} data from '<span class='codePara'>{$TT_RTTPath_dev}</span>'";
  }

  $displayRunsTemplate = new TemplateWrapper("display_runs/display_runs_view.html");
  $displayRunsTemplate->setParam("HEADER", isset($title) ? $title : "");

  // data sources
  $dirName = str_replace("data/", "", $dir);
  $hiddenRuns = readHiddenRuns();
  #$dir = "data/" . $dir;
  if ( isset( $dirPostPatters[ $dir ] ) ) { // This directory has sub-directories

    //natsort($dirPostPatters[$dir]);
    //$reversed = array_reverse($dirPostPatters[$dir], true);
    $showBtn = true;
    $anyRunsDisplayed = false;
    $runsToDisplay = array();
    foreach ( $dirPostPatters[$dir] as $theType => $theSubDir) {
      $runTemplate = new TemplateWrapper("display_runs/display_runs_subdirectory.html");
      $availableData = getRuns($dir, $theType);
      if (!checkIfAnyRunToDisplay($availableData, $dir, $hiddenRuns)) continue;
      $anyRunsDisplayed = true;
      if ($showBtn){
        $headerTemplate = getHeaderWithBtn("Runs in {$theType}:");
        $showBtn = false;
      } else {
        $headerTemplate = new TemplateWrapper("display_runs/display_runs_header_simple.html");
        $headerTemplate->setParam("TITLE", "Runs in {$theType}:");
      }

      $runTemplate->setChild("HEADER", $headerTemplate);
      if (checkIfAnyRunToDisplay($availableData, $dir, $hiddenRuns)){
        $runTemplate->setChild("RUNS_TABLE", echoTagRunDetails($availableData, $dir, $hiddenRuns, $theType));
      } else {
        $runTemplate->setParam("RUNS_TABLE", "<p><em>None Found.</em></p>");
      }

      array_push($runsToDisplay, $runTemplate);
    }

    if (!$anyRunsDisplayed) {
      $headerTemplate = getHeaderWithBtn("Runs in {$dir}:");
      $displayRunsTemplate->setParam("RUNS_LIST", "<p><em>None Found.</em></p>");
    } else {
      $displayRunsTemplate->setChild("RUNS_LIST", $runsToDisplay);
    }

  } else { // Simple case, no sub dirs
    $availableData = getRuns($dir);
    $runTemplate = new TemplateWrapper("display_runs/display_runs_subdirectory.html");
    $runTemplate->setChild("HEADER", getHeaderWithBtn("Runs in {$dirName}:"));

    if (checkIfAnyRunToDisplay($availableData, $dir, $hiddenRuns)){
      $runTemplate->setChild("RUNS_TABLE", echoTagRunDetails($availableData, $dir, $hiddenRuns));
    } else {
      $runTemplate->setParam("RUNS_TABLE", "<p><em>None Found.</em></p>");
    }

    $displayRunsTemplate->setChild("RUNS_LIST", $runTemplate);
  }

  $displayRunsTemplate->render();
}



function content_displayRunRanges() {
  global $dir, $type, $tag, $run, $levels, $costData, $err, $isExpert, $page, $isAdmin, $local;  

  if (isset($_POST['action']) and $_POST['action'] == "unhideRun") {
    unhideRun($_POST['runName']);
  }

  $hiddenRuns = readHiddenRuns();
  $runName = $dir . "/" . $tag . "/" . $run;
  if (in_array($runName, $hiddenRuns)) {
    $err = array(); // Reset errors - previous were already printed
    if (!$isExpert){
      $err[] = "This run is hidden. Please choose a different run.";
    } else {
      $errTemplate = new TemplateWrapper("display_run_ranges/display_run_ranges_hidden_run.html");
      $errTemplate->setParams(array("RUN_NAME" => $runName, "RUN_LINK" => getLinkRun($dir, $type, $tag, $run)));

      $err[] = $errTemplate->renderToString();
    }
    echoErrorMsgs();
  }

  if ($page == "forceAllRanges") {
    $athRelease = "Athena,23.0,r21";
    $nProcessings = 7; # will process 1750 lbs. if possible, to be read from the root file - getRunRanges check only existing csv directories
  
    $costCommandTemplate = new TemplateWrapper("display_run_ranges/display_run_ranges_force_all_ranges_monitor.txt");
    $costCommandTemplate->setParams(array("ATHENA_RELEASE" => $athRelease, 
                                          "COST_DIR" => "/var/www/html/" . getRun($dir, $type, $tag, $run),
                                          "NPROCESSINGS" => $nProcessings));

    $commandsRoot = $local ? "./" : "/var/www/html/commands/";
    $timestamp = time();
    $costCommandFile = "{$commandsRoot}cost_{$timestamp}.sh";
    file_put_contents($costCommandFile, $costCommandTemplate->renderToString());
    file_put_contents("/var/www/html/breakSleepManual", "breakSleepManual"); // Break sleep

    $forceEventsOutcome = '<p style="color: gray;">Request was scheduled and the full range will be available in ~1h. You can monitor the progess in "View request logs" tab</p>';
  } else {
    $forceEventsOutcome = '';
  }

  // Run ranges
  $runRangesList = array();
  $rangesTemplate = new TemplateWrapper("display_run_ranges/display_run_ranges_view.html");

  $availableData = getRunRanges($dir, $type, $tag, $run);

  $headTxt = "Run Ranges:";
  if (!(strpos($dir, 'data') === FALSE)) $headTxt = $headTxt . " <small>(Each SMK+L1PS+HLTPS monitors the first few contiguous LB which used that configuration)</small>";

  $rangesListTemplate = new TemplateWrapper("display_run_ranges/display_run_ranges_list.html");
  $rangesListTemplate->setParam("RANGE_NAME", $headTxt);

  $rangeList = array();
  foreach($availableData as &$theRange) {
    $rangesListEntry = new TemplateWrapper("display_run_ranges/display_run_ranges_list_entry.html");
    $rangesListEntry->setParams(array("RANGE_LINK" => getLinkRange($dir,$type,$tag,$run,$theRange), "RANGE_NAME" => str_replace("_"," ",$theRange), "RANGE_DETAILS" => getRangeDetails($dir,$type,$tag,$run,$theRange)));
    array_push($rangeList, $rangesListEntry);
  }
  $rangesListTemplate->setChild("RUNS_LIST", $rangeList);
  array_push($runRangesList, $rangesListTemplate);

  foreach ($levels as $theLevel) { // Link to single events (skip the summary selection screen)
    $fullEvents = getFullEventSummary( $dir, $type, $tag, $run, $theLevel );
    if ( count( $fullEvents) === 0 ) continue;

    $rangesListTemplate = new TemplateWrapper("display_run_ranges/display_run_ranges_list.html");
    $rangesListTemplate->setParam("RANGE_NAME", "Single Random {$theLevel} Events");

    $rangeList = array();
    foreach($fullEvents as &$theEvent) {
      $rangesListEntry = new TemplateWrapper("display_run_ranges/display_run_ranges_list_entry.html");
      $rangesListEntry->setParams(array("RANGE_LINK" => getLinkSummary($dir,$type,$tag,$run,$theEvent,$theLevel,'Full_Evnt'), "RANGE_NAME" => str_replace("_"," ",$theEvent), "RANGE_DETAILS" => ""));
      array_push($rangeList, $rangesListEntry);
    }
    $rangesListTemplate->setChild("RUNS_LIST", $rangeList);
    array_push($runRangesList, $rangesListTemplate);
  }

  foreach ($levels as $theLevel) { // Link to single slow events (skip the summary selection screen)
    $slowEvents = getSlowEventSummary( $dir, $type, $tag, $run, $theLevel );
    if ( count( $slowEvents) === 0 ) continue;

    $rangesListTemplate = new TemplateWrapper("display_run_ranges/display_run_ranges_list.html");
    $rangesListTemplate->setParam("RANGE_NAME", "Single Slow {$theLevel} Events:");

    $rangeList = array();
    foreach($slowEvents as &$theEvent) {
      $rangesListEntry = new TemplateWrapper("display_run_ranges/display_run_ranges_list_entry.html");
      $rangesListEntry->setParams(array("RANGE_LINK" => getLinkSummary($dir,$type,$tag,$run,$theEvent,$theLevel,'Slow_Evnt'), "RANGE_NAME" => str_replace("_"," ",$theEvent), "RANGE_DETAILS" => ""));
      array_push($rangeList, $rangesListEntry);
    }

    $rangesListTemplate->setChild("RUNS_LIST", $rangeList);
    array_push($runRangesList, $rangesListTemplate);
  }

  $rangesTemplate->setChild("RUN_RANGES", $runRangesList);

  // Configuration
  $configTemplate = new TemplateWrapper("display_run_ranges/display_run_ranges_configuration.html");
  $configs = getTrigConfigs($dir, $type, $tag, $run);
  if (count($configs) > 0) {

    $configList = array();
    foreach($configs as &$theConfig) {
      // Get array of the keys
      preg_match_all('!\d+!', $theConfig, $keyArray);
      $keys = $keyArray[0];
      $lumiBlocks = getLumiBlocksFromMeta($dir, $type, $tag, $run, $theConfig);

      $configEntry = new TemplateWrapper("display_run_ranges/display_run_ranges_configuration_entry_xml.html");
      $configEntry->setParams(array("SMK" => $keys[0], "L1" => $keys[2], "HLT" => $keys[3],
                                    "LUMIBLOCKS" => $lumiBlocks, "CONFIG_LINK" => getLinkConfig($dir,$type,$tag,$run,$theConfig), "CONFIG" => str_replace("_"," ",$theConfig)));

      array_push($configList, $configEntry);
    }
    $configTemplate->setChild("CONFIGURATION_LIST", $configList);
  } else {
    # Try the Run 3 configuration
    $configs = getTrigConfigsFromMetadata($dir, $type, $tag, $run);

    if (count($configs) === 0){
      $configTemplate->setParam("CONFIGURATION_LIST", "<p>Configuration not found!</p>");
    } else {
      $configList = array();
      foreach($configs as &$theConfig) {
        require_once('source/DatabaseConfigStruct.php');
        $configEntry = new TemplateWrapper("display_run_ranges/display_run_ranges_configuration_entry.html");
        $configEntry->setParams(array("SMK" => $theConfig->getSMK(), "L1" => $theConfig->getL1PSK(), "HLT" => $theConfig->getHLTPSK(),
                                      "LB_START" => $theConfig->getLbStart(), "LB_END" => $theConfig->getLbStop(), "DB_ALIAS" => $theConfig->getFormattedDb()));
        array_push($configList, $configEntry);
      }
      $configTemplate->setChild("CONFIGURATION_LIST", $configList);
    }
  }
  $rangesTemplate->setChild("TRIGGER_CONFIGURATION", $configTemplate);

  
  // Metadata
  if (checkMetadata($dir, $type, $tag, $run) != false) {
    $runMetadata = json_decode ( file_get_contents(getMetadata($dir, $type, $tag, $run)), true );
    $md = $runMetadata["children"];
  }

  if (isset($md)) {
    $mdTemplate = new TemplateWrapper("display_run_ranges/display_run_ranges_metadata.html");

    $mdList = array();
    foreach( $md as $mdEntry) {
      foreach( $mdEntry as $key => $value) {
        if ($key != "Date of Processing" && $key != "Version" && $key != "host" && $key != "pid"
          && $key != "AtlasProject" && $key != "CMTPATH" && $key != "SOURCE" && $key != "PredictionLumi"
          && $key != "Details" && $key != "SMK" && $key != "Release" && $key != "AMITag" && $key != "JIRA" && $key != "runNumber") continue;

        $mdEntry = new TemplateWrapper("display_run_ranges/display_run_ranges_metadata_entry.html");
        if ( $key == "JIRA" ) {
          $mdEntry->setParams(array("NAME" => "JIRA", "VALUE" => "<a href='https://its.cern.ch/jira/browse/{$value}' class='linkExternal'>{$value}</a>"));
        }
        elseif ( $key == "runNumber" ) {
          $mdEntry->setParams(array("NAME" => "Run Number", "VALUE" => "$value (<a href='http://atlas-runquery.cern.ch/query.py?q=find+run+{$value}+/+show+all' class='linkExternal'>Run Query</a>)"));
        }
        elseif ( $key == "AMITag" ) {
          $mdEntry->setParams(array("NAME" => "Ami Tag", "VALUE" => "<a href='https://ami.in2p3.fr/?subapp=tagsShow&userdata={$value}' class='linkExternal'>{$value}</a>"));
        } 
        elseif ( $key == "Details" && !empty($value)) {
          $value = str_replace("<", "&lt", $value);
          $value = str_replace(">", "&gt", $value);
          $mdEntry->setParams(array("NAME" => $key, "VALUE" => $value));
        } 
        else {
          $mdEntry->setParams(array("NAME" => $key, "VALUE" => $value));
        }
        array_push($mdList, $mdEntry);
      }
    }

    $mdTemplate->setChild("METADATA_LIST", $mdList);
    $mdTemplate->setParam("METADATA_LINK", getLinkMetadata($dir, $type, $tag, $run));
    $rangesTemplate->setChild("METADATA", $mdTemplate);
  } else {
    $rangesTemplate->setParam("METADATA", ""); // Metadata is not obligatory
  }

  // Show root file link
  if (checkRoot($dir, $type, $tag, $run)) {
    $histPath = getRoot($dir, $type, $tag, $run);
    $histNameSplit = explode('/', $histPath);

    $forceEventsLink = ($isAdmin && str_starts_with($tag, "data")) // Only for online cost data
            ? '<p><a href="' . getLinkForceAllRanges($dir, $type, $tag, $run) . '">Force monitor all events</a></p>' : "";
    $histogramTemplate = new TemplateWrapper("display_run_ranges/display_run_ranges_histograms.html");
    $histogramTemplate->setParams(array("HISTOGRAM_PATH" => $histPath, "HISTOGRAM_NAME" => end($histNameSplit),
                                        "FORCE_EVENTS_LINK" => $forceEventsLink, "FORCE_EVENTS_OUTCOME" => $forceEventsOutcome));


    $rangesTemplate->setChild("HISTOGRAMS", $histogramTemplate);
  } else {
    $rangesTemplate->setParam("HISTOGRAMS", ""); // Root file with histograms is not really required (online rate?)
  }

  $rangesTemplate->render();
}

function content_displayPerformancePlots() {
  global $dir, $type, $tag, $run, $levels, $costData, $forceRerun;

  $filename=getAlgSlotsPlot($dir, $type, $tag, $run);
  $filename2=getExpensiveAlgSlotsPlot($dir, $type, $tag, $run);
  if (file_exists("./".$filename) && file_exists("./".$filename2) && $forceRerun !== TRUE) {
    $fileTs = filemtime("./".$filename);
    echo "Using a cached files. Last modified: ".date("F d Y H:i:s.", $fileTs)."<br>";
    echo "<img src='" . $filename . "?=". $fileTs . "' width=800px/><br>";
    echo "<a href='" . $filename . "'>Full size image link</a><br>";

    echo "<img src='" . $filename2 . "?=". $fileTs . "' width=800px/><br>";
    echo "<a href='" . $filename2 . "'>Full size image link</a><br>";

    echo "<a href='" . getLinkPerformancePlotsForceRerun($dir, $type, $tag, $run) . "'>Refresh the plot</a>";
  } else {
    echo  "Preparing plots for run " . $run . " with tag " . $tag . "<br>";

    $runMetadata = json_decode ( file_get_contents(getMetadata($dir, $type, $tag, $run)), true );
    $md = $runMetadata["children"];
    if (isset($md)) {
      foreach( $md as $mdEntry) {
        foreach( $mdEntry as $key => $value) {
          if( $key == "DataRangeStart" ) $dataStart = $value;
          if( $key == "DataRangeEnd" ) $dataEnd = $value;
        }
      }
    }

    if (isset($dataStart) && isset($dataEnd)){
      $command = 'source ./TrigCostPerformancePlots.sh ' . intval($run) . ' ' . $tag . ' "' . $dataStart . '" "' . $dataEnd . '" > /data/perfPlot.log &';
      echo "Executing " . $command . "<br>";
      $output1 =  exec($command);
    
      //Print the return value
      #echo $output1;
      echo "The website will automatically refresh when the plot is ready (1 minute). <br><a href='" . $filename . "'>plot file </a>, manual <a href='" . getLinkPerformancePlots($dir, $type, $tag, $run) ."'>refresh</a>";
    } else {
      echo "Couldn't find start and end of range in metadata!<br>";
    }

    # Refresh the page when directory content changed
    $redirectUrl = str_replace("&amp;", "&", getLinkPerformancePlots($dir, $type, $tag, $run)); //sanitize link for js
    echo  <<< REFRESHPAGE
    <script language="javascript">

    function getTs(filename) {
      ts = false;
      $.ajax({
        url: 'source/getPerformancePlotTs.php',
        data:{'filename': "$filename2"}, 
        async: false,
        success: function(data) {
          ts = parseInt(data);
        }
      });
      return ts;
    }

    let originalValue = getTs("$filename2");

    var plotCheckInterval=setInterval(function(){checkUpdate()},10*1000); // at 10 seconds intervals
    function checkUpdate() {
      let newValue = getTs("$filename2");
      console.log(newValue, originalValue);
      if (!isNaN(newValue) && newValue != originalValue){
        console.debug("Timestamps are different - file was updated");
        clearInterval(plotCheckInterval);
        location.assign('$redirectUrl');
      } else {
        console.debug("Timestamps are the same or the file is not ready yet");
      }
    }

    </script>
    REFRESHPAGE;
  }
}

function content_displayMetadata() {
  global $dir, $type, $tag, $run, $levels, $costData;  

  $runMetadata = json_decode ( file_get_contents(getMetadata($dir, $type, $tag, $run)), true );
  $md = $runMetadata["children"];

  # Only for online data
  if (str_starts_with($tag, 'data')) {
    echo "<a href='" . getLinkPerformancePlots($dir, $type, $tag, $run) . "'>Performance Plots</a>";
  }

  if (isset($md)) {

    $mdChildren = array();
    foreach( $md as $mdEntry) {
      foreach( $mdEntry as $key => $value) {
        if (is_null($value)) $value = "-";
        if (is_bool($value)) $value = $value ? "true" : "false";
        unset($valueShort);
        unset($showMoreBtnStyle);
        if( $key == "HLTMenu" ) continue;
        if( $key == "Histogram under/overflows" ) {
          if (empty($value)) $value = "None";
          else {
            $valueArr = $value;
            $value = "<br> <div class='metadataList'> Summary: <br></div> <div class='metadataListItem'>" . implode("<br/>",array_pop($valueArr)["Summary"]);
            $value .= "</div><div class='metadataList'> Details: <br></div>";
            $value .=  "<div class='metadataListItem'>" . implode("<br/>", $valueArr) . "<br>";
          }
        }
        elseif(  $key == "OKS configuration" ) {
          $valueArr = $value;
          $value = "";
          foreach( $valueArr as $id => $config) {

            $value .= "<div class='metadataList'> Rack:" . $config[0]["Hostname"] . "<br></div>";
            $value .=  "<div class='metadataListItem'>Forks:" . $config[1]["Forks"] . "<br></div>";
            $value .=  "<div class='metadataListItem'>Threads:" . $config[2]["Threads"] . "<br></div>";
            $value .=  "<div class='metadataListItem'>Slots:" . $config[3]["Slots"] . "<br></div>";
            $value .=  "<div class='metadataListItem'>Computers:" . $config[4]["Computers"] . "<br></div>";
          }
        }
        elseif ( $key == "JIRA" ) {
          $JIRA = $value;  
          $value = "<a href='https://its.cern.ch/jira/browse/{$JIRA}' class='linkExternal'>{$JIRA}</a>";
        }
        elseif ( $key == "runNumber" ) {
          $key = "Run Number";
          $runNumber = $value;  
          $value = "$runNumber (<a href='http://atlas-runquery.cern.ch/query.py?q=find+run+{$runNumber}+/+show+all' class='linkExternal'>Run Query</a>)\n";
        }
        elseif(  $key == "AMITag" ) {
          $ami = $value;  
          $value = "<a href='https://ami.in2p3.fr/?subapp=tagsShow&userdata={$ami}' class='linkExternal'>{$ami}</a>";
        }
        elseif ( $key == "Details" ) {
          $value = str_replace("<", "&lt", $value);
          $value = str_replace(">", "&gt", $value); 
        } 
        elseif(  $key == "LumiblockDetails" ) {
          $lbStr="";
          foreach ($value as $arrId => $keyDict) {
            $lbStr .= "<div class='metadataList'>" . $arrId . ": {physics deadtime=" . $keyDict["deadtime"] . " average mu=" . $keyDict["avgPileup"] .
                " max mu=" . $keyDict["maxPileup"] . " min mu=" . $keyDict["minPileup"] . "}, </div>";
          }

          $value = $lbStr;
        }
        elseif (is_array($value)) {
          $pskStr = "";
          foreach ($value as $arrId => $keyDict) {
            $pskStr .= "" .  $keyDict[0] . ": " . $keyDict[1] . "-" . $keyDict[2] . ", ";
          }
          $value = $pskStr;
        } else {
          $valueShort = (strlen($value) > 1000) ? substr($value, 0, 1000) : $value;
          $showMoreBtnStyle = (strlen($value) > 1000) ? "" : 'style="display: none"';
        }

        $newItem = new TemplateWrapper("display_runs/display_runs_metadata_item.html");

        // Prepare shorten varsion of metadata - for now only for the basic ones
        $valueShort =  isset($valueShort) ? $valueShort : $value;
        $showMoreBtnStyle = isset($showMoreBtnStyle) ? $showMoreBtnStyle : 'style="display: none"';
        $newItem->setParams(array("KEY" => $key, "KEY_ID" => str_replace("/", "", str_replace(" ", "", $key)), "VALUE" => $valueShort, "VALUE_FULL" => $value, "SHOW_MORE" => $showMoreBtnStyle));
        array_push($mdChildren, $newItem);
      }
    }

    $metadataTemplate = new TemplateWrapper("display_runs/display_runs_metadata.html");
    $metadataTemplate->setChild("METADATA_LIST", $mdChildren);
    $metadataTemplate->render();

  }
}



function content_displayRangeSummaries() {
  global $levels, $dir, $type, $tag, $run, $range, $summaryList;  

  $runRangeSummaries = array();
  foreach ($levels as $theLevel) { 
    foreach($summaryList as $summaryTypeName => $summaryItems) {
      $availableData = getRangeSummary($dir, $type, $tag, $run, $range, $theLevel, $summaryItems);
      if ( count( $availableData) === 0 ) continue;

      $summaryLinks = array();
      foreach($availableData as &$theSummary) {
          $newItem = new TemplateWrapper("display_run_ranges/display_run_ranges_range_summary_item.html");
          $newItem->setParams(array("LEVEL" => $theLevel, "SUMMARY_NAME" => str_replace("_"," ",$theSummary), 
                "SUMMARY_LINK"=>getLinkSummary($dir,$type,$tag,$run,$range,$theLevel,$theSummary)));
          array_push($summaryLinks, $newItem);
      }

      $newSummary = new TemplateWrapper("display_run_ranges/display_run_ranges_range_summary.html");
      $newSummary->setParams(array("LEVEL" => $theLevel, "TYPE" => $summaryTypeName));
      $newSummary->setChild("SUMMARY_LIST", $summaryLinks);
      array_push($runRangeSummaries, $newSummary);
    }
  }

  $rangeSummariesTemplate = new TemplateWrapper("display_run_ranges/display_run_ranges_range_summaries_view.html");
  $rangeSummariesTemplate->setChild("SUMMARY_LIST", $runRangeSummaries);
  $rangeSummariesTemplate->render();
}


function content_displayConfiguration() { 
  global $dir, $type, $tag, $run, $config, $search;  

  echoHeader(3, "Trigger Configuration Summary:");
  $configTable = new ConfigurationTable( getConfig($dir, $type, $tag, $run, $config) );
  $configTable->setSearch($search);
  $configTable->insertConfigTable();
}



function content_displaySummary() {
  global $dir, $type, $tag, $run, $range, $level, $summary;
  
  //echoHeader(3, "{$level} {$summary} Summary:");
  $csv = getCsv($dir, $type, $tag, $run, $range, $level, $summary);
  if (strpos($summary, "Rate_") !== FALSE) {
    $costTable = new RatesDataTable();
  }
  else {
    $costTable = new CostDataTable();
  }
  $costTable->setTitle("$summary Summary");
  $costTable->setDataSource( $csv, $dir, $type, $tag, $run, $range, $level, $summary );

  if ($summary == "Full_Evnt" || $summary == "Slow_Evnt") {
    $costTable->addSearchCol(2); // Chain name
    $costTable->addSearchCol(3); // Alg name
    $costTable->addSearchCol(4); // Seq name
    $costTable->addSearchCol(14); // ROS
    $costTable->setSortCol(1,"asc");
    echo "<p>Each algorithm may be run many times in an event, the number in square brackets at the end of the algorithm's name indicates which execution it was.</p>";
  }

  // Preformat the Rates Table
  if (strpos($summary, "Rate_") !== FALSE) {
    $costTable->hideColumn(1); // Time
    $costTable->addSearchCol(2); // Group
    # Print out the L
    $lumiVal = 0;
    $cmtStr = "";
    $lumiStr = "";
    getRatesLumiPoint($dir, $type, $tag, $run, $lumiVal, $lumiStr, $cmtStr);

    $predStr = (strpos($dir, "Online") !== FALSE) ? "for" : "prediction for " . $lumiStr;
    $formHeader = ($lumiVal > 0) ? "Rates " . $predStr ." L = " . sprintf("%.2e",$lumiVal) . " cm<sup>-2</sup>s<sup>-1</sup>" : "";
    $cacheStr = ($cmtStr != "") ? "Cache: {$cmtStr}" : "";

    $doComparisonFormTemplate = new TemplateWrapper("comparison/do_comparison_form_rates.html");
    $doComparisonFormTemplate->setParam("FORM_HEADER", $formHeader);
    $doComparisonFormTemplate->setParam("CACHE", $cacheStr);
    $doComparisonFormTemplate->setParam("SERVER_REQUEST_URI", $_SERVER['REQUEST_URI']);
    $doComparisonFormTemplate->setParam("LUMI", sprintf("%.2e", $lumiVal));
    $doComparisonFormTemplate->render();
  }

  // Preformat the comparable Cost Tables
  if (isSummaryComparable($summary)) {
    $doComparisonFormTemplate = new TemplateWrapper("comparison/do_comparison_form_cost.html");
    $doComparisonFormTemplate->setParam("SERVER_REQUEST_URI", $_SERVER['REQUEST_URI']);
    $doComparisonFormTemplate->setParam("SUMMARY_NAME", $summary);
    $doComparisonFormTemplate->render();
  }

  if (strpos($summary, "Chain") !== FALSE) {
    $costTable->addSearchCol(1); // Group
  }

  $costTable->insertTable();

  // These ones get a fancy pi chart
  if (strpos($summary, "Group") !== FALSE) $costTable->insertRatesPiChart();
  if (strpos($summary, "SliceCPU") !== FALSE) $costTable->insertCPUPiChart();
}



function content_displayItem() {
  global $dir, $type, $tag, $run, $range, $level, $summary, $summaryDisplay, $item, $err;
 
  if ( checkRoot($dir, $type, $tag, $run) == 0 ) {
    $err[] = "ERROR: Cannot find ROOT file for this run, unable to show histograms.";
    echoErrorMsgs(); 
  }

  $csv = getCsv($dir, $type, $tag, $run, $range, $level, $summary);
  $costTable = new CostDataTable();
  $costTable->setDataSource( $csv, $dir, $type, $tag, $run, $range, $level, $summary, $item );
  $itemDataTable = $costTable->insertItemDetail();

  if (empty($itemDataTable)) {
    $err[] = "ERROR: Unable to find details for this item.";
    echoErrorMsgs(); 
  }

  $detailViewTemplate = new TemplateWrapper("item_detail/item_detail_view.html");
  $detailViewTemplate->setParam("SUMMARY_NAME", $summary);
  $detailViewTemplate->setParam("ITEM_NAME", $item);
  $detailViewTemplate->setParam("ITEM_DETAIL_TABLE", $itemDataTable);

  $configs = getTrigConfigs($dir, $type, $tag, $run);
  
  // Display secondary table
  if ($summary == 'Sequence' || $summary == 'Chain' || $summary == 'Algorithm') {
    // Use config table if any, otherwise use metadata
    if (count($configs) > 0) {
      $config = $configs[0];
      $configTable = new ConfigurationTable( getConfig($dir, $type, $tag, $run, $config ) );
    } else {
      $configTable = new ConfigurationTable( getMetadata($dir, $type, $tag, $run), "json" );
    }
    if ($summary == 'Chain') {
      $myAlgs = $configTable->getChainAlgs( $item );
      $configOutputTable = $costTable->insertAlgLinkTable($myAlgs, "Algorithm");
    }
    else if ($summary == 'Algorithm') {
      $myAlgs = $configTable->getAlgChains( $item );
      $configOutputTable = $costTable->insertAlgLinkTable($myAlgs, "Chain");
    }
    else {
      $myAlgs = $configTable->getSeqAlgs( $item );
      $configOutputTable = $costTable->insertAlgLinkTable($myAlgs, "Algorithm");
    }
  }
  $detailViewTemplate->setParam("CONFIG_MAPPING_TABLE", isset($configOutputTable) ? $configOutputTable : "");
  
  // Display D3 graph
  if (strpos($summary, "Rate_") !== FALSE) {
    if (checkRatesGraph($dir, $type, $tag, $run) != 0) {
      $ratesGraph = json_decode ( file_get_contents(getRatesGraph($dir, $type, $tag, $run)), true );
      $chainArray = $ratesGraph["children"];
      foreach ($chainArray as $key => &$element) {
        if ($element["text"] != $item) continue;
        $ratesGraph = new RatesGraph();
        $groupRatesGraph = $ratesGraph->insertRatesGraph( json_encode($element["children"]) );
        break;
      }
    }
  }
  $detailViewTemplate->setParam("GROUP_RATES_GRAPH", isset($groupRatesGraph) ? $groupRatesGraph : "");
  
  if ($summary == 'Algorithm' || $summary == 'Sequence' || $summary == 'Chain' || $summary == 'Rate_ChainHLT' || $summary == 'Rate_ChainL1') {
    $triggerConfig = "<h3>Trigger Configurations:</h3><ul>\n";
    // Check if this item has an instance name on the end, e.g. [1]
    $pos = strpos($item, "[");
    if ($pos !== FALSE) $itemNoInstance = substr($item, 0, $pos);
    else $itemNoInstance = $item;
    foreach($configs as &$theConfig) {
      $triggerConfig .= "  <li><a href='" . getLinkConfigSearch($dir,$type,$tag,$run,$theConfig,$itemNoInstance) . "'> Lookup {$itemNoInstance} in " . str_replace("_"," ",$theConfig) . "</a>";
      $lumiBlocks = getLumiBlocksFromMeta($dir, $type, $tag, $run, $theConfig);
      if ($lumiBlocks !== "")  $triggerConfig .= " (Lumi Blocks:" . $lumiBlocks . ")";
      $triggerConfig .= "</li>\n"; 
    }
    $triggerConfig .= "</ul>\n";
  }
  $detailViewTemplate->setParam("ITEM_TRIGGER_CONFIGURATION", (isset($triggerConfig) && sizeof($configs)) ? $triggerConfig : "");

  $costHisto = new CostHistograms( getRoot($dir, $type, $tag, $run) );
  $costHisto->addDirectory($range);
  $costHisto->addDirectory("{$summary}_{$level}");
  $costHisto->addDirectory($item);
  $histograms = $costHisto->insertHistograms();
  $detailViewTemplate->setChild("ITEM_HISTOGRAMS", $histograms);

  $detailViewTemplate->render();
}


function page_about() {
  $pageAbout = new TemplateWrapper("about_view.html");
  $pageAbout->render();
}


function page_symlinks() {
  echo "<p>Running Setup...</p>";
}


function page_log() {
  global $page, $key;

  $logViewTemplate = new TemplateWrapper("log_view.html");

  if ($page == 'log') {
    $logViewTemplate->setParam("DATA_TYPE", "P1 Data");
    $logName = '/data/cron.log';
    if ($key == 'breakSleep') {
      file_put_contents("/var/www/html/breakSleep", "breakSleep");
      $logViewTemplate->setParam("WAKE_UP_NOTIFICATION", "<p>Wake-up sent. It will be picked up within 60s.</p>");
    } else {
      $logViewTemplate->setParam("WAKE_UP_NOTIFICATION", "");
    }
  } else {
    $logViewTemplate->setParam("DATA_TYPE", "Manual Requests");
    $logName = '/data/cronManual.log';
    if ($key == 'breakSleep') {
      file_put_contents("/var/www/html/breakSleepManual", "breakSleepManual");
      $logViewTemplate->setParam("WAKE_UP_NOTIFICATION", "<p>Wake-up sent. It will be picked up within 60s.</p>");
    } else {
      $logViewTemplate->setParam("WAKE_UP_NOTIFICATION", "");
    }
  }
  
  $logStr = "";
  $log = new SplFileObject($logName, "r");
  if ($log !== FALSE) { 
    // here we get count of lines: go to EOF and get line number
    $log->seek($log->getSize());
    $total = $log->key()+1;
    $toGet = min($total, 1000);
  
    $logLines = array();
    foreach( new LimitIterator($log, $total-$toGet) as $line) {
      array_push($logLines, $line);
    }

    $logLines = array_reverse($logLines);
    foreach ( $logLines as $line ){
      $logStr .= htmlentities($line) . "<br />";
    }
  } else {
    $logStr = "<center><div class='err'>ERROR: Log file not found!</div></center>";
  }


  $logViewTemplate->setParams(array("WAKE_UP_PAGE" => getLinkPage($page), "LOG_CONTENT" => $logStr));
  $logViewTemplate->render();
}

function page_readOnlineRatesLog() {
  global $page, $key;

  $logViewTemplate = new TemplateWrapper("log_view.html");
  $logViewTemplate->setParam("DATA_TYPE", "Read Online Rates");
  $logName = '/data/onlineRates.log';
  $logViewTemplate->setParam("WAKE_UP_NOTIFICATION", "");

  $logStr = "";
  $log = new SplFileObject($logName, "r");
  if ($log !== FALSE) { 
    // here we get count of lines: go to EOF and get line number
    $log->seek($log->getSize());
    $total = $log->key()+1;
    $toGet = min($total, 1000);
  
    $logLines = array();
    foreach( new LimitIterator($log, $total-$toGet) as $line) {
      array_push($logLines, $line);
    }

    $logLines = array_reverse($logLines);
    foreach ( $logLines as $line ){
      $logStr .= htmlentities($line) . "<br />";
    }
  } else {
    $logStr = "<center><div class='err'>ERROR: Log file not found!</div></center>";
  }


  $logViewTemplate->setParams(array("WAKE_UP_PAGE" => "?page=ReadOnlineRatesLog", "LOG_CONTENT" => $logStr));
  $logViewTemplate->render();
}




function page_software() {
  echo "<h3>Installed Software:</h3>";
  echo "<p class='codePara'>";
  echo exec("svn info /opt/trig-cost-sw/TrigCostRootAnalysis/ | grep ^URL") . "<br />";
  echo exec("svn info /opt/trig-cost-sw/TrigCostD3PD/ | grep ^URL") . "<br />";
  echo exec("svn info /opt/trig-cost-sw/TrigRootAnalysis/ | grep ^URL") . "<br />";
  echo "</p>";
}



function page_unknown() {
  echo "<p>Unknown Page</p>";
}

?>
