<?php

// Helper class to store config retireven from the database
class DatabaseConfigStruct {
    var $smk;
    var $db;
    var $hltpsk;
    var $l1psk;
    var $lb_start;
    var $lb_end;
        
    function __construct($smk, $db, $l1psk, $hltpsk, $lb_start, $lb_end) {
        $this->smk = $smk;
        $this->db = $db;
        $this->hltpsk = $hltpsk;
        $this->l1psk = $l1psk;
        $this->lb_start = $lb_start;
        $this->lb_end = $lb_end;
    }

    public function getSMK(){
        return $this->smk;
    }  

    public function getL1PSK(){
        return $this->l1psk;
    }  

    public function getHLTPSK(){
        return $this->hltpsk;
    }  

    public function getLbStart(){
        return $this->lb_start;
    }  

    public function getLbStop(){
        if ($this->lb_end == 4294967295) return "(-1)";
        return $this->lb_end;
    }  

    public function getFormattedDb(){
        $dbFromatted = str_replace("TRIGGERDB", "", $this->db);
        $dbFromatted = str_replace("_RUN3", "", $dbFromatted);
        if (empty($dbFromatted)) $dbFromatted = "RUN3";
        return strtolower($dbFromatted);
    }
}


?>
