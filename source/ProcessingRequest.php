<?php 

function page_processingRequest() {
  global $err, $isAdmin, $local;
  $commandsRoot = $local ? "~/" : "/var/www/html/commands/";
  $costRoot = "/data/CostProcessings_2025/";
  $ratesRoot = "/data/RateProcessings_2025/";
  $timestamp = time();

  // First submit data
  $rateFiles = isset($_POST['rateFiles']) ? sanitise($_POST['rateFiles']) : NULL;
  $costFiles = isset($_POST['costFiles']) ? sanitise($_POST['costFiles']) : NULL;
  $nFiles = 0;
  $doRates = isset($_POST['doRates']) ? TRUE : FALSE;
  $doCost = isset($_POST['doCost']) ? TRUE : FALSE;
  $doValidate = isset($_POST['doValidate']) ? TRUE : FALSE;
  $skipValidate = (isset($_POST['skipValidate']) and $isAdmin == TRUE) ? TRUE : FALSE;
  $prescaleTag = isset($_POST['prescaleTag']) ? sanitise($_POST['prescaleTag']) : 'noPS';
  $JIRA = isset($_POST['JIRA']) ? sanitise($_POST['JIRA']) : NULL;
  $JIRAFull = NULL;
  $release = isset($_POST['release']) ? sanitise($_POST['release']) : NULL;
  $runNumber = isset($_POST['runNumber']) ? intval(sanitise($_POST['runNumber'])) : NULL;
  $amiTag = isset($_POST['amiTag']) ? sanitise($_POST['amiTag']) : NULL;
  $description = isset($_POST['description']) ? sanitise($_POST['description']) : NULL;

  // Second submit data
  $commandRates = isset($_POST['commandRates']) ? sanitise($_POST['commandRates']) : NULL;
  $commandCost = isset($_POST['commandCost']) ? sanitise($_POST['commandCost']) : NULL;
  $rateFilesSubmit = isset($_POST['rateFilesSubmit']) ? sanitise($_POST['rateFilesSubmit']) : NULL;
  $costFilesSubmit = isset($_POST['costFilesSubmit']) ? sanitise($_POST['costFilesSubmit']) : NULL;
  $costInputFile = isset($_POST['costInputFile']) ? sanitise($_POST['costInputFile']) : NULL;
  $rateInputFile = isset($_POST['rateInputFile']) ? sanitise($_POST['rateInputFile']) : NULL;
  $costDirName = isset($_POST['costDirName']) ? sanitise($_POST['costDirName']) : NULL;
  $rateDirName = isset($_POST['rateDirName']) ? sanitise($_POST['rateDirName']) : NULL;
  $ratesCommandFile = isset($_POST['ratesCommandFile']) ? sanitise($_POST['ratesCommandFile']) : NULL;
  $copyToMenuScriptArgs = isset($_POST['menuCopyArgs']) ? sanitise($_POST['menuCopyArgs']) : NULL;
  $costCommandFile = isset($_POST['costCommandFile']) ? sanitise($_POST['costCommandFile']) : NULL;
  $release = isset($_POST['release']) ? sanitise($_POST['release']) : NULL;
  $doSubmit = isset($_POST['doSubmit']) ? TRUE : FALSE;

  $status = "unvalidated";

  //\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
  if ($skipValidate == TRUE) {
  //\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    $status = "validated"; // Yeah, it's fine I'm sure
  //\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
  } else if ($doValidate == TRUE) {
  //\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

    // CHECK INPUT FILES
    if ( ($costFiles == NULL or $costFiles == "") and ($rateFiles == NULL or $rateFiles == "") ) {
      $err[] = "ERROR: No files supplied";
    }
    $fileArray = explode ("\n", $costFiles);
    $nFiles = count($fileArray);
    $hasCostFiles = FALSE;
    foreach ($fileArray as $line) {
      if ($line == "") continue;

      if (substr($line,0,5) != "/eos/" and substr($line,0,7) != "/cvmfs/") {
        $err[] = "ERROR: Invalid input file '{$line}'";
        break;
      };
      // Work out the file type
      if (strpos($line, "NTUP_TRIGCOST") !== FALSE or strpos($line, "trig_cost") !== FALSE) {
        $hasCostFiles = TRUE;
        $costFilesSubmit[] = $line;
      } else {
        $err[] = "ERROR: Cost input file name must contain either 'NTUP_TRIGCOST' or 'trig_cost'.";
        break;
      }
    }

    $fileArray = explode ("\n", $rateFiles);
    $nFiles += count($fileArray);
    $hasRateFiles = FALSE;
    foreach ($fileArray as $line) {
      if ($line == "") continue;

      if (substr($line,0,5) != "/eos/" and substr($line,0,7) != "/cvmfs/" and substr($line,0,21) != "root://eosatlas//eos/") {
        $err[] = "ERROR: Invalid input file '{$line}'";
        break;
      };

      if (strpos($line, "NTUP_TRIGRATE") !== FALSE) {
        $hasRateFiles = TRUE;
        $rateFilesSubmit[] = $line;
      } else {
        $err[] = "ERROR: Rate input file name must contain either NTUP_TRIGRATE' or 'trig_rate'.";
        break;
      }
    }

    // CHECK TYPE
    if ($doRates == FALSE and $doCost == FALSE) $err[] = "ERROR: Must choose at least one of Rates or Cost mode";
    if ($doCost == TRUE and $hasCostFiles == FALSE) $err[] = "ERROR: Cannot do a cost processing without 'NTUP_TRIGCOST' input file(s).";
    if ($doRates == TRUE and $hasRateFiles == FALSE) $err[] = "ERROR: Cannot do a rates processing without 'NTUP_TRIGRATE' input file(s).";

    // CHECK JIRA
    if ($JIRA != NULL) {
      if (is_numeric($JIRA) == false) $err[] = "ERROR: Invalid JIRA given. Must be a number.";
      else $JIRAFull = "ATR-" . $JIRA;
    } 

    // CHECK AMI
    if ($amiTag == NULL) {
      $err[] = "ERROR: AMI Tag was not supplied!";
    } else if (!preg_match("/^r[0-9]*$/", $amiTag)) {
      $err[] = "ERROR: AMI Tag is in a wrong format! Supply HLT Ami tag, for example 'r12345'";
    }

    // CHECK RELEASE
    if ($release == NULL) {
      $err[] = "ERROR: Release number must be supplied.";
    } else {
      $releaseExplode = explode (".", $release);
      $releaseStyle = true;
      foreach ($releaseExplode as $element) {
        if (is_numeric($element) === FALSE) {
          $releaseStyle = false;
          break;
        }
      }

      if ($releaseStyle === FALSE && DateTime::createFromFormat('?Y#m#d', $release) === FALSE) { // Test time style
        $err[] = "ERROR: Invalid release number given. Should numbers separated by full-stops or of the format rYYYY-MM-DD";
      }
    }

    // CHECK DESCRIPTION
    if ($description != NULL) {
      if (strpos($description, " -") !== FALSE) $err[] = "ERROR: Description may not contain the pattern ' -'.";
    }

    if(!empty($err)) echoErrorMsgs();
    else $status = "validated";
    
  //\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
  } else if ( $doSubmit == TRUE ) {
  //\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

    // MAKE A LOOSER CHECK OF THE RESUBMITTED DATA
    if ($costFilesSubmit == NULL and $rateFilesSubmit == NULL) $err[] = "ERROR: Did not receive files.";
    if ($doCost == TRUE and $costInputFile == NULL) $err[] = "ERROR: Cost file path was not transmitted.";
    if ($doRates == TRUE and $rateInputFile == NULL) $err[] = "ERROR: Rate file path was not transmitted.";
    if ($commandRates == NULL and $commandCost == NULL) $err[] = "ERROR: Did not receive command to execute.";
    if ($commandRates != NULL and $ratesCommandFile == NULL) $err[] = "ERROR: Did not receive path to save rates command to.";
    if ($commandCost  != NULL and $costCommandFile == NULL) $err[] = "ERROR: Did not receive path to save cost command to.";
    if(!empty($err) and $isAdmin == FALSE) { // Admins get to skip this validation too as they can fix anything which goes wrong
      $err[] = "ERROR: Some data was not received. Please go back one page and make sure no text boxes are left empty";
      $status = "err"; // This will mean we don't show anything else below
    } else {
      $status = "submit";
    }
    echoErrorMsgs();


  //\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
  }
  //\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

  // IF VALIDATED - SHOW PROTO-COMMAND
  /////////////////////////////////////////////////////////////////////////////////////////////
  if ($status == "validated") {
  /////////////////////////////////////////////////////////////////////////////////////////////
    
    // OUTPUT FILE INFO
    $processingRequestViewTemplate = new TemplateWrapper("processing_request/processing_request_validated_form.html");
    $processingRequestViewTemplate->setParams(array("COST_FILES" => $costFiles, "RATE_FILES" => $rateFiles, "N_FILES" => $nFiles));

    // User details
    $userDetails = ""; 
    if ($description != NULL) $userDetails .= "--userDetails '{$description}'"; 
    if ($JIRAFull != NULL)  $userDetails .= " --jira '{$JIRAFull}'";
    if ($amiTag != NULL) $userDetails .= " --amiTag '{$amiTag}'";  

    // Prepare cost/rate command
    if ($doCost == TRUE) {
      $validatedCost = new TemplateWrapper("processing_request/processing_request_validated_cost.html");

      // Work out where the command will be written
      $costCommandFile = "{$commandsRoot}cost_{$JIRAFull}_{$timestamp}.sh";
      $validatedCost->setParam("OUTPUT_SCRIPT_FILENAME", $costCommandFile);

      # Create cost directory path
      $costDir = "costMonitoring_cost-reprocessing";
      if ($JIRAFull != NULL) $costDir .= "-" . $JIRAFull;

      // Check if Run for this JIRA already exists
      $nRunFiles = checkRun("CostProcessings-2022", $release, "cost-reprocessing-$JIRAFull*", $runNumber);
      if ($nRunFiles > 0) {
        $costDir .= "-" . ($nRunFiles + 1);
      }

      $costDir .= "_" . $runNumber;
      $costInputFile = "{$costRoot}{$release}/{$costDir}/TrigCostRoot_Results.root";
      $validatedCost->setParam("INPUT_FILE", $costInputFile);
      $validatedCost->setParam("OUTPUT_DIR", $costDir);

      // Construct Command
      $commandCost = "CostAnalysisPostProcessing.py --file {$costInputFile} {$userDetails}";
      $validatedCost->setParam("COMMAND", $commandCost);
      $processingRequestViewTemplate->setChild("VALIDATED_COST", $validatedCost);
    } else {
      $processingRequestViewTemplate->setParam("VALIDATED_COST", "");
    }

    if ($doRates == TRUE) {
      $validatedRates = new TemplateWrapper("processing_request/processing_request_validated_rates.html");

      // Work out where the command will be written
      $ratesCommandFile = "{$commandsRoot}rate_{$JIRAFull}_{$timestamp}.sh";
      $validatedRates->setParam("OUTPUT_SCRIPT_FILENAME", $ratesCommandFile);

      // Work out an output tag name
      $rateTag = "rate-prediction-";
      if ($JIRAFull != NULL) $rateTag .= $JIRAFull . "-";
      $formattedPrescaleTag =  str_replace(array("/", " ", "_"), "-", $prescaleTag); // Switch _ to - to perserve formatting
      $rateTag .= $formattedPrescaleTag;

      # Create cost directory path
      $rateDir = "costMonitoring_{$rateTag}_{$runNumber}";

      $rateInputFile = "{$ratesRoot}{$release}/{$rateDir}/TrigCostRoot_Results.root";
      $menuCopyArgs = "$release";
      $validatedRates->setParams(array("INPUT_FILE" => $rateInputFile, "OUTPUT_DIR" => $rateDir, "COPY_RATES_SCRIPT_ARGS" => $menuCopyArgs));

      // Construct Command
      $commandRates =  "RatesAnalysisPostProcessing.py --file {$rateInputFile} --outputTag {$rateTag} {$userDetails}";
      $validatedRates->setParam("COMMAND", $commandRates);
      $processingRequestViewTemplate->setChild("VALIDATED_RATES", $validatedRates);
    } else {
      $processingRequestViewTemplate->setParam("VALIDATED_RATES", "");
    }

    $processingRequestViewTemplate->setParam("RELEASE", $release);
    $processingRequestViewTemplate->render();

  /////////////////////////////////////////////////////////////////////////////////////////////    
  } else if ($status == "submit") {
  /////////////////////////////////////////////////////////////////////////////////////////////

    // DO THE SUBMISSION AND SHOW THE SUMMARY
    $processingRequestViewTemplate = new TemplateWrapper("processing_request/processing_request_submit_form.html");

    $athRelease = "Athena,24.0.69";

    // Save cost/rate command to file
    if ($commandCost != NULL and $costCommandFile != NULL) {
      $submitCost = new TemplateWrapper("processing_request/processing_request_submit_details.html");
      $submitCost->setParam("PROCESSING_TYPE", "Cost");

      // Create directories
      $costCommandTemplate = new TemplateWrapper("processing_request/command/command_base.txt");
      $costCommandTemplate->setParams(array("ATHENA_RELEASE" => $athRelease, "REPROCESSING_RELEASE" => $release, "COST_ROOT" => $costRoot, "DIR_NAME" => $costDirName));

      // Create command to merge input files
      if ($costFilesSubmit != NULL and $costInputFile != NULL) {
        $costFilesSubmit = str_replace("\r", " ", $costFilesSubmit); // We need unix line breaks!
        $costFilesSubmit = str_replace("\n", " ", $costFilesSubmit);

        $nFiles = str_word_count($costFilesSubmit, 0, "._-/1234567890"); // File name can contain that symbols

        $costInputFilesTemplate = new TemplateWrapper("processing_request/processing_request_submit_inputs.html");
        $costInputFilesTemplate->setParams(array("N_FILES" => $nFiles, "INPUT_FILE" => $costInputFile, "SUBMITTED_FILES" => $costFilesSubmit));
        $submitCost->setChild("INPUT_FILES", $costInputFilesTemplate);

        $commandMerge = ($nFiles == 1) ? "cp {$costFilesSubmit} {$costInputFile}" : "hadd {$costInputFile} {$costFilesSubmit}";
      } else {
        $submitCost->setParam("INPUT_FILES", "<p>Cost input files are not available</p>");
      }

      $costCommandTemplate->setParam("MOVE_INPUT_FILES", "{$commandMerge} \n{$commandCost} \n");
      
      // Move the result to the correct directory
      $onSuccess = new TemplateWrapper("processing_request/command/cost_success.txt");
      $onSuccess->setParams(array("REPROCESSING_RELEASE" => $release, "COST_ROOT" => $costRoot, "DIR_NAME" => $costDirName));
      $costCommandTemplate->setChild("SUCCESS", $onSuccess);

      $onFailure = new TemplateWrapper("processing_request/command/failure.txt");
      $onFailure->setParams(array("REPROCESSING_RELEASE" => $release, "COST_ROOT" => $costRoot, "DIR_NAME" => $costDirName));
      $costCommandTemplate->setChild("FAILURE", $onFailure);

      if (file_put_contents($costCommandFile, $costCommandTemplate->renderToString()) === FALSE) $costScriptSaveResult = new TemplateWrapper("processing_request/processing_request_save_script_failure.html");
      else $costScriptSaveResult = new TemplateWrapper("processing_request/processing_request_save_script_success.html");
      $costScriptSaveResult->setParam("PROCESSING_SCRIPT_NAME", $costCommandFile);
      $submitCost->setChild("SCRIPT_SAVE_RESULT", $costScriptSaveResult);

      $submitCost->setParam("COMMAND", $commandCost);
      $processingRequestViewTemplate->setChild("SUBMIT_COST", $submitCost);
    } else {
      $processingRequestViewTemplate->setParam("SUBMIT_COST", "");
    }

    if ($commandRates != NULL and $ratesCommandFile != NULL) {
      $submitRates = new TemplateWrapper("processing_request/processing_request_submit_details.html");
      $submitRates->setParam("PROCESSING_TYPE", "Rates");

      $ratesnputFilesTemplate = new TemplateWrapper("processing_request/command/command_base.txt");
      $ratesnputFilesTemplate->setParams(array("ATHENA_RELEASE" => $athRelease, "REPROCESSING_RELEASE" => $release, "COST_ROOT" => $ratesRoot, "DIR_NAME" => $rateDirName));

      if ($rateFilesSubmit != NULL and $rateInputFile != NULL) {
        $rateFilesSubmit = str_replace("\r", " ", $rateFilesSubmit); // We need unix line breaks!
        $rateFilesSubmit = str_replace("\n", " ", $rateFilesSubmit);

        $nFiles = str_word_count($rateFilesSubmit, 0, "._-/1234567890"); // File name can contain that symbols
        $rateInputFilesTemplate = new TemplateWrapper("processing_request/processing_request_submit_inputs.html");
        $rateInputFilesTemplate->setParams(array("N_FILES" => $nFiles, "INPUT_FILE" => $rateInputFile, "SUBMITTED_FILES" => $rateFilesSubmit));
        $submitRates->setChild("INPUT_FILES", $rateInputFilesTemplate);

        $commandMerge = ($nFiles == 1) ? "cp {$rateFilesSubmit} {$rateInputFile}" : "hadd {$rateInputFile} {$rateFilesSubmit}";

      } else {
        $submitRates->setParam("INPUT_FILES", "<p>Rates input files are not available</p>");
      }

      $ratesnputFilesTemplate->setParam("MOVE_INPUT_FILES", "{$commandMerge} \n{$commandRates} \n");

      // Move the result to the correct directory
      $onSuccess = new TemplateWrapper("processing_request/command/rates_success.txt");
      $onSuccess->setParams(array("REPROCESSING_RELEASE" => $release, "COST_ROOT" => $ratesRoot, "DIR_NAME" => $rateDirName,
                                  "COPY_RATES_SCRIPT_ARGS" => $copyToMenuScriptArgs));
      $ratesnputFilesTemplate->setChild("SUCCESS", $onSuccess);

      $onFailure = new TemplateWrapper("processing_request/command/failure.txt");
      $onFailure->setParams(array("REPROCESSING_RELEASE" => $release, "COST_ROOT" => $ratesRoot, "DIR_NAME" => $rateDirName));
      $ratesnputFilesTemplate->setChild("FAILURE", $onFailure);

      if (file_put_contents($ratesCommandFile, $ratesnputFilesTemplate->renderToString()) === FALSE) $ratesScriptSaveResult = new TemplateWrapper("processing_request/processing_request_save_script_failure.html");
      else $ratesScriptSaveResult = new TemplateWrapper("processing_request/processing_request_save_script_success.html");
      $ratesScriptSaveResult->setParam("PROCESSING_SCRIPT_NAME", $ratesCommandFile);
      $submitRates->setChild("SCRIPT_SAVE_RESULT", $ratesScriptSaveResult);

      $submitRates->setParam("COMMAND", $commandRates);
      $processingRequestViewTemplate->setChild("SUBMIT_RATES", $submitRates);
    } else {
      $processingRequestViewTemplate->setParam("SUBMIT_RATES", "");
    }

    $processingRequestViewTemplate->setParam("LOG_LINK", getLinkPage("logManual"));
    $processingRequestViewTemplate->render();
    // This file tells the trig cost to wake from sleep
    file_put_contents("/var/www/html/breakSleepManual", "breakSleepManual");
  /////////////////////////////////////////////////////////////////////////////////////////////
  } else if ($status == "unvalidated") { // Show data collection window
  /////////////////////////////////////////////////////////////////////////////////////////////
    $rateChecked = (($doRates == TRUE or $_SERVER['REQUEST_METHOD'] != 'POST') ? "checked='checked'" : "");
    $costChecked = (($doCost  == TRUE or $_SERVER['REQUEST_METHOD'] != 'POST') ? "checked='checked'" : "");
    $adminSkipValidation = ($isAdmin == TRUE ? "<input type='submit' class='formBtn' value='Skip Validation [ADMIN]' name='skipValidate'/>" : "");

    $processingRequestViewTemplate = new TemplateWrapper("processing_request/processing_request_unvalidated_form.html");
    $processingRequestViewTemplate->setParams(array(
      "COST_FILES_CHECKED" => $costChecked, "RATE_FILES_CHECKED"=> $rateChecked,
      "COST_FILES"=> $costFiles, "RATE_FILES" => $rateFiles,
      "RELEASE" => $release, "JIRA" => $JIRA, "AMI_TAG" => $amiTag,
      "RUN_NUMBER" => $runNumber, "PRESCALE_TAG" => $prescaleTag,
      "DESCRIPTION"=> $description, "SKIP_VALIDATION" => $adminSkipValidation
    ));
    $processingRequestViewTemplate->render();
  /////////////////////////////////////////////////////////////////////////////////////////////
  }
  /////////////////////////////////////////////////////////////////////////////////////////////

}


?>
