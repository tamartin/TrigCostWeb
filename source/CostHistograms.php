<?php
class CostHistograms {

  var $ROOTFile;
  var $directories = array();
  var $directoryStr;
  
  function __construct($ROOTFile) {
    $this->ROOTFile = $ROOTFile;
  }
  
  function addDirectory($dir) {
    $this->directories[] = $dir;
    $this->directoryStr .= $dir . "/";
  }

  function insertHistograms() {
    $histogramsView = new TemplateWrapper("item_detail/item_detail_histograms.html");
    $histogramsView->setParam("ROOT_FILE", $this->ROOTFile);
    $histogramsView->setParam("ROOT_DIRECTORY", $this->directoryStr);
    
    $histogramsList = "";
    foreach ($this->directories as &$directory) {
      $histogramsList .= "nav.push('{$directory}');\n";
    }

    $histogramsView->setParam("HISTOGRAMS_LIST", $histogramsList);

    return $histogramsView;
  }
  
}
?>
