<?php 

include_once 'source/compareFromDatabase.php';

/* Function to check if summary can be compared based on the defined list "availableComparisons"*/
function isSummaryComparable($summaryName){
    $json_columns = json_decode(file_get_contents("cost-comparison-columns.json"), true);

    foreach ($json_columns as $sName => $sArr) {
        if ($sName == $summaryName){
            return TRUE;
        }
    }
    return FALSE;
}

/* Function to return names of columns (corresponding to names in csv file) that will be compared */
function getComparableColumns($summaryName){
    $json_columns = json_decode(file_get_contents("cost-comparison-columns.json"), true);
    foreach ($json_columns as $sName => $sArr) {
        if ($sName == $summaryName){
            return $sArr;
        }
    }

    // Summary not found
    echo "<div class='err'>ERROR: Comparison for summary " . $summaryName . " not available!</div>";
    return array();
}

/* Function  to retrieve ids of columns to compare for a given summary*/
function getComparableColumnsIds($summaryName){
    $json_columns = json_decode(file_get_contents("cost-comparison-columns.json"), true);
    foreach ($json_columns as $sName => $sArr) {
        if ($sName == $summaryName){
            $ids = array();
            foreach($sArr as $sEl){
                array_push($ids, $sEl["col"]);
            }
            return $ids;
        }
    }

    // Summary not found
    echo "<div class='err'>ERROR: Comparison for summary " . $summaryName . " not available!</div>";
    return array();
}

/* Function to generate a header for comparison column */
function generateHeader($summaryName){
    if (!isSummaryComparable($summaryName)) return "";
    $columns = getComparableColumns($summaryName);

    $header = "Name, Matched";
    foreach ($columns as &$col) {
        $name = $col["name"];
        $unit = $col ["unit"];
        $header = $header . "," . $name . " <br>A " . $unit . ", " . $name . " <br>B " . $unit . ", " 
            . $name . "<br>diff (A-B) " . $unit . "<br>, ". $name ." <br> relative change [%] <br>";
    }

    return $header;
}

/* Function to generate comparison page */
function page_compareCost(){
    // Read any POST data
    $compA = isset($_POST['compA']) ? sanitise($_POST['compA']) : NULL;
    $compB = isset($_POST['compB']) ? sanitise($_POST['compB']) : NULL;
    if (strpos($compB, "Paste the") !== FALSE) $compB = NULL;
    $lumi = isset($_POST['lumi']) ? sanitise($_POST['lumi']) : NULL;

    $costTable = new CostDataTable();
    $costTable->setTitle("CompareCost");

    //Check if we want to load from the DB
    $saveKey = isset($_GET['key']) ? sanitise($_GET['key']) : NULL;
    $fromDB = FALSE;
    $loadFromDb = checkIfInDatabase($saveKey);
    if ($saveKey) {
        if ($loadFromDb !== NULL) {
        // We update from the DB
        $compA = $loadFromDb['COMPA'];
        $compB = $loadFromDb['COMPB'];
        $lumi = $loadFromDb['LUMI'];
        $fromDB = TRUE;
        } else {
        $compA = NULL;
        $compB = NULL;
        $lumi = NULL;
        }
    }

    if ((!isset($compA) and $compA != "" and isset($compB) and $compB != "")) {
        echo "<div class='err'>ERROR: Failed to parse cost data from URL A.</div>";
    }

    // Decode
    $arrayA = array();
    parse_str(parse_url(htmlspecialchars_decode($compA), PHP_URL_QUERY), $arrayA);
    $dir = isset($arrayA["dir"]) ? $arrayA["dir"] : NULL;
    $type = isset($arrayA["type"])  ? $arrayA["type"] : NULL;
    $tag = isset($arrayA["tag"]) ? $arrayA["tag"] : NULL;
    $run = isset($arrayA["run"]) ? $arrayA["run"] : NULL;
    $range = isset($arrayA["range"]) ? $arrayA["range"] : NULL;
    $level = isset($arrayA["level"]) ? $arrayA["level"] : NULL;
    $summary = isset($arrayA["summary"]) ? $arrayA["summary"] : NULL;

    $arrayB = array();
    parse_str(parse_url(htmlspecialchars_decode($compB), PHP_URL_QUERY), $arrayB);
    $dirB = isset($arrayB["dir"]) ? $arrayB["dir"] : NULL;
    $typeB = isset($arrayB["type"])  ? $arrayB["type"] : NULL;
    $tagB = isset($arrayB["tag"]) ? $arrayB["tag"] : NULL;
    $runB = isset($arrayB["run"]) ? $arrayB["run"] : NULL;
    $rangeB = isset($arrayB["range"]) ? $arrayB["range"] : NULL;
    $levelB = isset($arrayB["level"]) ? $arrayB["level"] : NULL;
    $summaryB = isset($arrayB["summary"]) ? $arrayB["summary"] : NULL;


    $doComparisonFormTemplate = new TemplateWrapper("comparison/update_comparison_form_cost.html");
    $doComparisonFormTemplate->setParam("SUMMARY_NAME", $summary);
    $doComparisonFormTemplate->setParam("COMP_A", $compA);
    $doComparisonFormTemplate->setParam("COMP_B", $compB);
    $doComparisonFormTemplate->render();

    // Validate the values
    if (!isset($dir) or !isset($type) or !isset($tag) or !isset($run) or !isset($range) or !isset($level) or !isset($summary)) {
        echo "<div class='err'>ERROR: Failed to parse cost data from URL A.</div>";
    } else if (!isset($dirB) or !isset($typeB) or !isset($tagB) or !isset($runB) or !isset($rangeB) or !isset($levelB) or !isset($summaryB)) {
        echo "<div class='err'>ERROR: Failed to parse cost data from URL B.</div>";
    } else if (checkCsv($dir, $type, $tag, $run, $range, $level, $summary) == 0) {
        echo "<div class='err'>ERROR: Cannot find data for URL A.</div>";
    } else if (checkCsv($dirB, $typeB, $tagB, $runB, $rangeB, $levelB, $summaryB) == 0) {
        echo "<div class='err'>ERROR: Cannot find data for URL B.</div>";
    } else if (strcmp($summary, $summaryB) !== 0) {
        echo "<div class='err'>ERROR: Summaries " . $summary . " and " . $summaryB . " don't match!</div>";
    } else {
        $saveKey = addToDB($saveKey, $compA, $compB, $lumi);
        if ($saveKey != NULL) {
            echo "<p><b>To share this comparison, please use <a href='?page=CompareCost&key={$saveKey}'>this link</a></b></p>";
        }

        $csv  = getCsv($dir,  $type,  $tag,  $run,  $range,  $level,  $summary);
        $csvB = getCsv($dirB, $typeB, $tagB, $runB, $rangeB, $levelB, $summaryB);

        $costTable->setDataSource( $csv,  $dir,  $type,  $tag,  $run,  $range,  $level,  $summary  );
        $costTable->setComparison( $csvB, $dirB, $typeB, $tagB, $runB, $rangeB, $levelB, $summaryB );
        $costTable->insertTable();
    }
}

?>