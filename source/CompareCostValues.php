
<?php 

/* Create json list for cost run directories */
function createRunSelect($runsDir, $selectedRuns){
    global $dirPostPatters;
    $avaiableRuns=array();
    $isSelected=array();

    foreach ($runsDir as $dir){
        if (strpos($dir, 'Rate') !== false) continue; // Skip Rate directories
        $dirName = str_replace('_', '-', explode('/', $dir)[2]); // Name to search in $dirPostPatters dictionary
        
        $runArray = array();
        if (isset($dirPostPatters[$dirName])) { // This directory has sub-directories
            foreach ( $dirPostPatters[$dirName] as $theType => $theSubDir) {
                $runArray = array_merge($runArray, getRuns($dirName, $theType));
            }
        } else {
            $runArray = getRuns($dirName);
        } 

        // Print run  + run range list
        foreach ($runArray as &$run) {
            $ranges = array();
            if (isset($dirPostPatters[$dirName])){
                foreach ( $dirPostPatters[$dirName] as $theType => $theSubDir) {
                    $ranges = array_merge($ranges, getRunRanges($dirName, $theType, $run[0], $run[1]));
                }
            } else {
                $ranges = getRunRanges($dir, "", $run[0], $run[1]);
            }

            foreach($ranges as $range){
                $fullName = $dirName . "__" . $run[1] . "__" . $run[0] . "__" . $range;
                $isSelected[$fullName] = in_array($fullName, $selectedRuns) ? "true" : "false";
                $avaiableRuns[$fullName] = 'Run '. $run[1] . ' ' . $range . ' ' . $run[0];
            }
        }
    }
    krsort($avaiableRuns, SORT_NATURAL);

    $selectList = "";
    foreach($avaiableRuns as $id => $name){
        $selectList .= "{id:'$id', name:'$name', disabled: false, groupName:'', groupId:'0', selected: $isSelected[$id]},";
    }
    return $selectList;
}

/* Create list of all available summaries */
function createSummariesList($selectedSummary){
    global $summaryList;
    $selectList="";

    foreach ($summaryList as $name => $summaryArr){
        if (strpos($name, 'Rate') !== false) continue;
        foreach ($summaryArr as &$item) {
            if ($item == $selectedSummary) $selectList .= "<option selected='' value='$item'>$item</option>";
            else $selectList .= "<option value='$item'>$item</option>";
        }
    }

    return $selectList;
}


/* Retrieve list of available columns:
    First look for first available csv file with given summary. If for a run Summary is not available display an error
    Read first row of summary's csv file, skip 'Name' column and parse column names into dropdown list
*/ 
function createColumnsList($selectedRuns, $summary, &$selectedColumn){
    global $dirPostPatters;

    // Find avaiable csv file
    foreach ($selectedRuns as $selectedRun){
        $runData = explode("__", $selectedRun);
        $dirName = $runData[0];

        // Check if the run is in subdirectory
        $subDirType = "";
        if (isset($dirPostPatters[$dirName])) {
            foreach ( $dirPostPatters[$dirName] as $theType => $theSubDir) {
                if (checkCSV($dirName, $theType, $runData[2], $runData[1], $runData[3], "HLT", $summary)){
                    $subDirType = $theType;
                    break;
                }
            }
        }

        echo "<script>console.debug('Create columns list based on " . getCsv($dirName, $subDirType, $runData[2], $runData[1], "LumiBlock_00000", "HLT", $summary). "')</script>";

        if (!checkCSV($dirName, $subDirType, $runData[2], $runData[1], $runData[3], "HLT", $summary)){
            global $err;
            $err[] = "ERROR: Cannot find Summary " . $summary . " for run $runData[1].";
            continue;
        } else {
            $csvFile = getCsv($dirName, $subDirType, $runData[2], $runData[1], $runData[3], "HLT", $summary);
            break;
        }
    }

    if (!isset($csvFile)) {
        global $err;
        $err[] = "ERROR: None of given runs contain Summary " . $summary;
        echoErrorMsgs(); 
        return "";
    }

    $csv = fopen($csvFile, 'r');
    $columnsArr = explode(",", fgets($csv));
    array_shift($columnsArr); // Remove "Name" column
    if ($summary == "Chain") array_shift($columnsArr); // Skip also "Groups" column

    fclose($csv);
    $columnsList = "";
    foreach ($columnsArr as $column){
        $columnFixed = rtrim(str_replace(">", "longer than", $column));

        if ($columnFixed == $selectedColumn) $columnsList .= "<option selected='' value='$columnFixed'>$columnFixed</option>";
        else $columnsList .= "<option value='$columnFixed'>$columnFixed</option>";
    }

    if ($selectedColumn == "") $selectedColumn = $columnsArr[0];

    return $columnsList;
}

/* Print column chart with values for given runs for a chosen column, value, summary */
function createChart($runs, $summary, $column, $name) {
    global $dirPostPatters;

    $valuesList = array();
    foreach($runs as $run){
        $runData = explode("__", $run);
        $dirName = $runData[0];

        // Check if the run is in subdirectory
        $subDirType = "";
        if (isset($dirPostPatters[$dirName])) {
            foreach ( $dirPostPatters[$dirName] as $theType => $theSubDir) {
                if (checkCSV($dirName, $theType, $runData[2], $runData[1], $runData[3], "HLT", $summary)){
                    $subDirType = $theType;
                    break;
                }
            }
        }

        if (!checkCSV($dirName, $subDirType, $runData[2], $runData[1], $runData[3], "HLT", $summary)){
            global $err;
            $err[] = "ERROR: Cannot find Summary " . $summary . " for run $runData[1].";
            continue;
        }

        $csvFile = getCsv($dirName, $subDirType, $runData[2], $runData[1], $runData[3], "HLT", $summary);
        $csv = fopen($csvFile, 'r');
        $columnsArr = explode(",", rtrim(fgets($csv)));
        $colIdx = array_search(str_replace("longer than", ">", $column), $columnsArr);

        // Skip header description line
        fgets($csv);

        $result = "";
        while ($row = fgetcsv($csv)) {
            if ($row[0] == $name) {
                $result = $row[$colIdx];
                break;
            }
        }

        if ($result == ""){
            global $err;
            $err[] = "ERROR: Cannot find $name in Summary $summary for run $runData[1].";
            continue;
        }

        echo "<script>console.debug('Adding to chart $runData[1] $result');</script>";
        $runDetails = $runData[1] . " " . $runData[2];

        $valuesList[$runDetails] = $result;
    }

    echoErrorMsgs();
    if (empty($valuesList)) return;
    
echo <<< PI_OUT
<script type="text/javascript">
// <![CDATA[

    window.onload = function () {

PI_OUT;
    echo "let costChart = JSROOT.createHistogram('TH1F', " . count($valuesList) . ");\n";
    echo "costChart.fXaxis.fLabels = JSROOT.create('THashList');\n";

    $binId = 1;
    foreach ($valuesList as $key => $val) {
        echo "
        var lbl = JSROOT.create('TObjString');\n
        lbl.fString = '$key'; \n
        lbl.fUniqueID = $binId; \n
        costChart.fXaxis.fLabels.Add(lbl, '');\n";

        echo "costChart.setBinContent($binId, " . $val . ");\n";
        ++$binId;
    }

echo <<< PI_OUT

        costChart.fName = "cost_chart";
        costChart.fTitle = 'Comparison for {$name}';
        costChart.fXaxis.fTitle = 'Run number';
        costChart.fYaxis.fTitle = '{$column}';

        JSROOT.redraw("chart_values", costChart, "colz");
      }
// ]]>    
</script>
PI_OUT;

}


/* Function to generate comparison values page to create chart with selected values for selected runs*/
function page_compareCostValues(){
    global $dataDirs2025, $dataDirs2024, $dataDirs2023, $dataDirs2022, $dataDirs2020, $dataDirs2018, $dataDirs2017, $dataDirs2016, $dataDirs2015, $otherLinks;

    $selectedSummary = isset($_POST['chosenSummary']) ? $_POST['chosenSummary'] : "Algorithm";
    $selectedColumn = isset($_POST['chosenColumn']) ? $_POST['chosenColumn'] : NULL;
    $selectedName = isset($_POST['chosenName']) ? $_POST['chosenName'] : NULL;
    $selectedRunsList = isset($_POST['runsList']) ? $_POST['runsList'] : NULL;
    $selectedRunsList = array_filter(explode (",", $selectedRunsList)); 
    $selectedRunsListStr = json_encode($selectedRunsList);

    if (isset($selectedSummary) and isset($selectedRunsList) and sizeof($selectedRunsList) > 0){
        $availableColumns = createColumnsList($selectedRunsList, $selectedSummary, $selectedColumn);
    } else {
        $availableColumns = "";
    }

    if (isset($selectedSummary) and isset($selectedRunsList) and sizeof($selectedRunsList) > 0 and isset($selectedColumn) and $selectedColumn != "" and isset($selectedName) and $selectedName != ""){
        createChart($selectedRunsList, $selectedSummary, $selectedColumn, $selectedName);
    }

    // Create list of available runs
    $availableData = createRunSelect($dataDirs2025, $selectedRunsList);
    $availableData .= createRunSelect($dataDirs2024, $selectedRunsList);
    $availableData .= createRunSelect($dataDirs2023, $selectedRunsList);
    $availableData .= createRunSelect($dataDirs2022, $selectedRunsList);
    $availableData .= createRunSelect($dataDirs2020, $selectedRunsList);
    $availableData .= createRunSelect($dataDirs2018, $selectedRunsList);
    $availableData .= createRunSelect($dataDirs2017, $selectedRunsList);
    $availableData .= createRunSelect($dataDirs2016, $selectedRunsList);
    $availableData .= createRunSelect($dataDirs2015, $selectedRunsList);
    $availableData .= createRunSelect($otherLinks, $selectedRunsList);

    $availableSummaries = createSummariesList($selectedSummary);
    
    $availableRuns = "";

    echo "<script>console.log('Compare Cost values:','$selectedRunsListStr', '$selectedSummary', '$selectedColumn', '$selectedName');</script>";
    
echo <<< EXPORT
    <div class='columnClear' style="overflow:unset;">
    <p><strong>
    Create chart for a chosen item from cost table for different runs.
    </strong></p>
    <form action="?page=CompareCostValues" method="post">
    <p>
    Runs: 
    <div class="selectRunDropdown">
        <select style="display:none" name="selectRun" multiple placeholder="Select"> </select>
    </div>

    <input type="hidden" id="runsList" name="runsList" value=""/>
    </p>
    <p>
    Cost Summary:
    <div class="selectSummary">
    <select style="display:none" placeholder="Select"> $availableSummaries </select>
    <input type="hidden" id="chosenSummary" name="chosenSummary" value=""/>
    </div>
    </p>
    <input type="submit" class="formBtn" value="Search for columns" onclick="onColumnsSearchSubmit()"/>

    <p>
    Summary Column:
    <div class="selectColumn">
    <select style="display:none" placeholder="Select"> $availableColumns </select>
    <input type="hidden" id="chosenColumn" name="chosenColumn" value=""/>
    </div>
    </p>
    <p>
    Item Name: <input class="dropdown-display inputForm" type="text" id="chosenName" name="chosenName" value="" />
    <div class='subtitle'>Enter a name of algorithm/chain/thread that you want to compare</div>
    </p>
    <input type="submit" class="formBtn" value="Do Comparison"/>
    </form>

    </div>
    <div class='columnClear' style="overflow:unset;">

    <br/>
    
    <div id="chart_values" style="width: 700px; height: 350px; display: block; margin: 0 auto;"></div>
    <div id='pngLink'></div>

    </div>
    <script>

    chosenRunsList=$selectedRunsListStr;
    document.getElementById("runsList").value = chosenRunsList.toString();
    document.getElementById("chosenSummary").value = "$selectedSummary";
    document.getElementById("chosenColumn").value = "$selectedColumn";
    document.getElementById("chosenName").value = "$selectedName";

    $('.selectRunDropdown').dropdown({
      data: [$availableData],
      multipleMode: 'label',
      choice: function () {
          if (arguments.length == 2 && arguments[1].selected == true) {
            chosenRunsList.push(arguments[1].id);
          } else {
            let runId = chosenRunsList.indexOf(arguments[0].target.attributes[1].value);
            chosenRunsList.splice(runId, 1);
          }
          document.getElementById("runsList").value = chosenRunsList.toString();

      }
    });

    $('.selectSummary').dropdown({
        readOnly: true,
        input: '<input type="text" maxLength="20" placeholder="Search">',
        choice: function () {
            document.getElementById("chosenSummary").value = arguments[0].target.textContent;
        }
      });
    
    $('.selectColumn').dropdown({
        readOnly: true,
        input: '<input type="text" maxLength="50" placeholder="Search">',
        choice: function () {
            document.getElementById("chosenColumn").value = arguments[0].target.textContent;
        }
    });

    function onColumnsSearchSubmit() {
        // Clean the values in further part of the form
        document.getElementById("chosenColumn").value = "";
        document.getElementById("chosenName").value = "";

    }
    </script>
EXPORT;
}

?>