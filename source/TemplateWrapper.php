<?php

// Simple class to read and fill in a template, html but not only
class TemplateWrapper {
    private $params = array();
    private $children = array();
    private $templateName = "";
    private $templateDir = "templates/";

    public function __construct($templateName) {
        $this->templateName = $templateName;
    }

    // Store the parameters to be filled in the template
    public function setParam ($paramName, $paramValue): void {
        // TODO additional checks if key not in the original dict
        $this->params[$paramName] = $paramValue;
    }

    public function getParam ($paramName): string {
        return array_key_exists($paramName, $this->params) ? $this->params[$paramName] : "";
    }

    // Pass new parameters as array
    public function setParams ($paramDict): void {
        // TODO additional checks if key not in the original dict
        $this->params = array_merge($this->params, $paramDict);
    }

    // Store children templates to be rendered
    public function setChild ($childName, $childValue): void {
        // TODO additional checks if key not in the original dict
        $this->children[$childName] = $childValue;
    }

    // Read template from file, apply parameters and children templates and print result to stdout
    public function render(){
        echo $this->renderToString();
    }

    // Read template from file, apply parameters and children templates and return result as string
    public function renderToString(){
        global $local;

        echo "<script>console.debug('Rendering " . $this->templateName . " ');</script>";
        $template = $this->readTemplate();
        if (empty($template)) {
            echo "<div class='err'>ERROR: Template " . $this->templateName . " not found!</div>";
            return;
        }

        foreach ($this->params as $paramName => $paramValue) {
            $template = str_replace("!!". $paramName . "!!", !is_null($paramValue) ? $paramValue : "", $template);
        }

        foreach ($this->children as $childName => $childValue) {
            if (is_array($childValue)){
                $renderedElements = "";
                foreach($childValue as $element) {
                    $renderedElements .= $element->renderToString();
                }

                $template = str_replace("!!". $childName . "!!", $renderedElements, $template); 
            } else {
                $renderedChild = $childValue->renderToString();
                $template = str_replace("!!". $childName . "!!", $renderedChild, $template);
            }
        }

        // Find all "leftovers" and remove them
        preg_match_all('/!![0-9A-Z_].+!!/', $template, $matches);
        if (count($matches[0]) != 0) {
            foreach ($matches[0] as $match) {
                echo "<script>console.error('Template " . $match . " not found!');</script>";
                if (!$local) {
                    $template = str_replace($match, "", $template); 
                }
            }
        }

        return $template;
    }

    // Read template from file
    private function readTemplate(){
        $templateFilename = $this->templateDir . $this->templateName;
        if (file_exists($templateFilename)) {
            return file_get_contents($templateFilename);
        }

        return "";
    }
}

?>