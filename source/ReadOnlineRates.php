<?php 


function page_readOnlineRates() {
    global $USERNAME;

    $runNumber = isset($_POST['runNumber']) ? sanitise($_POST['runNumber']) : NULL;
    $startLb = isset($_POST['startLb']) ? sanitise($_POST['startLb']) : NULL;
    $endLb = isset($_POST['endLb']) ? sanitise($_POST['endLb']) : NULL;
    $outputLocation = isset($_POST['outputLocation']) ? sanitise($_POST['outputLocation']) : NULL;

    if (!isset($runNumber) && empty($runNumber)) {
        // Display form to schedule the execution
        $ratesForm = new TemplateWrapper("online_rates/online_rates_form.html");
        $ratesForm->setParams(array("USER_LINK" => readlink("data/" . $USERNAME)));
        $ratesForm->render();
    } else if ((empty($runNumber) || empty($startLb) || empty($endLb) || empty($outputLocation))) {
        // Display result of the script
        echo "<div class='err'>ERROR: The form is missing information</div>";
        $ratesForm = new TemplateWrapper("online_rates/online_rates_form.html");
        $ratesForm->setParams(array("USER_LINK" => readlink("data/" . $USERNAME)));
        $ratesForm->render();
    } else {
        // Display result of the script
        $athRelease="Athena,24.0.32";
        $outputDir1 = "costMonitoring_OnlineTRPRates-onlinePS-LB" . $startLb . "-" . $endLb . "_" . $runNumber;
        $outputDir2 = "costMonitoring_OnlineTRPRates-noPS-LB" . $startLb . "-" . $endLb . "_" . $runNumber;
        $ratesCommand = new TemplateWrapper("online_rates/online_rates_command.txt");
        $ratesCommand->setParams(array("ATHENA_RELEASE" => $athRelease, 
                "RUN_NUMBER" => $runNumber, "START_LB" => $startLb, "END_LB" => $endLb, "OUTPUT_LOCATION" => $outputLocation,
                "PS_DIR" => $outputDir1, "UPS_DIR" => $outputDir2));
        
        $commandFile = "/var/www/html/commands/runRates_" . time() . ".sh";

        if (file_put_contents($commandFile, $ratesCommand->renderToString()) === FALSE) {
            echo "<div class='err'>Failed to save the script</div>";
            return;
        }

        $displayCommand = "RatesAnalysisOnlineProcessing.py --runNumber " . $runNumber . " --lbStart " . $startLb . " --lbEnd " . $endLb;
        echo "<p>Executing " . $displayCommand . "</p>";
        echo "<p>Outputs will be saved to " . $outputLocation . "/" . $outputDir1 . " and "  . $outputLocation . "/" . $outputDir2 . "</p>";
        echo "<p><a href='?page=ReadOnlineRatesLog'>See the script log</a></p>";

        $output1 = shell_exec("source " . $commandFile . " >> /data/onlineRates.log 2>&1 &");
        #echo $output1;
    }
}



?>