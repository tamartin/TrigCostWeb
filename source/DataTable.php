<?php
class DataTable {

  var $tableTitle;
  var $tableName;

  var $csvFile;
  var $dir;
  var $type;
  var $tag;
  var $run;
  var $range;
  var $level;
  var $summary;
  var $item;

  var $csvFileB;
  var $dirB;
  var $typeB;
  var $tagB;
  var $runB;
  var $rangeB;
  var $levelB;
  var $summaryB;
  var $itemB;
  
  var $searchable;
  var $toHide;
  
  var $sortDir;
  var $sortCol;
  
  var $doComp; // Do comparison between two tables
  var $compHeaderSetup;

  function __construct() {
    $this->searchable[0] = TRUE;
    $this->sortDir = "asc";
    $this->sortCol = 0;
    $this->doComp = false;
    $this->compHeaderSetup = "";
  }
  
  function addSearchCol($canBeSearced) {
    $this->searchable[intval($canBeSearced)] = TRUE;
  }

  function hideColumn($toHide) {
    $this->toHide[intval($toHide)] = TRUE;
  }
  
  function setSortCol($col, $order) {
    assert($order == 'asc' || $order == 'desc');
    $this->sortDir = $order;
    $this->sortCol = intval($col);
  }

  
  function setTitle($title) {
    $this->tableTitle = $title;
    $this->tableName = str_replace(' ', '', $title);
  }
  
  function setDataSource( $csvFile, $dir = "", $type = "", $tag = "", $run = "", $range = "", $level = "", $summary = "", $item = "" ) {
    $this->dir = $dir;
    $this->type = $type;
    $this->tag = $tag;
    $this->run = $run;
    $this->range = $range;
    $this->level = $level;
    $this->summary = $summary;
    $this->item = $item;
    $this->csvFile = $csvFile;
  }

  function setComparison( $csvFile, $dir = "", $type = "", $tag = "", $run = "", $range = "", $level = "", $summary = "", $item = "" ) {
    $this->dirB = $dir;
    $this->typeB = $type;
    $this->tagB = $tag;
    $this->runB = $run;
    $this->rangeB = $range;
    $this->levelB = $level;
    $this->summaryB = $summary;
    $this->itemB = $item;
    $this->csvFileB = $csvFile;
    $this->doComp = true;
  }
  
  function getHeaderArray() {
    if ($this->doComp == true) return str_getcsv($this->compHeaderSetup);   
    $csv = fopen($this->csvFile, 'r');
    $line = fgets($csv);
    fclose($csv);
    return str_getcsv($line);
  } 
  
  function getFooterArray() {
    if ($this->doComp == true) return str_getcsv($this->compHeaderSetup);    
    $csv = fopen($this->csvFile, 'r');
    $line = fgets($csv);
    $line = fgets($csv);
    fclose($csv);
    return str_getcsv($line);
  } 
  
  function insertItemDetail() {
    // We are going to call the table generation script inline!
    // Manually set the required GETs
    $_GET['rp'] = 1; // Results to return
    $_GET['query'] = $this->item;
    $_GET['qtype'] = 0; // Column to search
    $_GET['dir'] = $this->dir; 
    $_GET['type'] = $this->type;
    $_GET['tag'] = $this->tag;     
    $_GET['run'] = $this->run; 
    $_GET['csvfile'] = $this->csvFile;
    $_GET['inline'] = TRUE;
    $_GET['sortname'] = 0;
    $_GET['sortorder'] = 'asc';
    // Get the data
    ob_start(); // Start buffer to get result from request
    require("csv-to-json.php"); //execute request
    $jsonResult = ob_get_clean(); // Get result (json formatted string)
    if (count(json_decode($jsonResult)->{"rows"}) == 0) return ""; // No data to display
    $result = json_decode($jsonResult)->{"rows"}[0]->cell; // Convert to array - get data (only one entry)
    $headerArray = $this->getHeaderArray();
    $tooltipArray = $this->getFooterArray();

    $detailTable = "<table id='itemDetailsTable' class='resultDisplay itemDetailsTable'>\n";
    foreach ($headerArray as $key => &$header) {
      if ($key == 0) $detailTable .= "<tr><th class='rightText'>Summary Property</th><th>Value</th></tr>\n";
      $detailTable .= "<tr><td class='rightText' title='$tooltipArray[$key]'>{$header}</td><td>$result[$key]</td></tr>\n";
    }
    $detailTable .= "</table>";
    return $detailTable;
  }
  
  /* Insert the data table. Additional arguments can be passed to read from csv-to-json.php file*/
  function insertTable($additionalArgs="") {
    // Get just the first line from the CSV to get the column names
    $headerArray = $this->getHeaderArray();
    $tooltipArray = $this->getFooterArray();
    $columnSize = count($headerArray);
    $tableQuery = isset($_GET['query']) ? sanitise($_GET['query']) : NULL;
    if ($this->doComp == false) {
      echo "<p><div class='csvTableHeaderFlexContainer'>";
      echo "<div>Table parsed from CSV file: <a href='{$this->csvFile}'>{$this->csvFile}</a></div>";
      echo "<a id='linkGeneratorBtn' href='" . getLinkSummary($this->dir, $this->type, $this->tag, $this->run, $this->range, $this->level, $this->summary) . "' class='formBtn'>Create search link</a>";
      echo "</div></p>";
    }
echo <<< TABLE_OUT
<table class="{$this->tableName}" style="display: none"><tr><td></td></tr></table>
<script type="text/javascript">
// <![CDATA[

  var doOnce = 0;
	var tableCallback = function(table) { 
	  if (doOnce == 1) return;
	  doOnce = 1;
	  
    var nColumns = {$columnSize};
    //console.log(table);
    //table.autoResizeColumn(table);
    // Issue a resize command to all colls
    for (var n = 1; n < nColumns; ++n) {
      //console.debug("resize col " + n);
    
		  //var n = $('div', this.cDrag).index(obj),
			var \$th = $('th:visible div:eq(' + n + ')', table.hDiv),
			  ol = 100; //parseInt(obj.style.left, 10),
			  ow = \$th.width(),
			  nw = 0,
			  nl = 0,
			 \$span = $('<span />');
		  $('body').children(':first').before(\$span);
		 \$span.html(\$th.html());
		 \$span.css('font-size', '' + \$th.css('font-size'));
		 \$span.css('padding-left', '' + \$th.css('padding-left'));
		 \$span.css('padding-right', '' + \$th.css('padding-right'));
		  nw = \$span.width();
		  $('tr', table.bDiv).each(function () {
			  var \$tdDiv = $('td:visible div:eq(' + n + ')', table),
				  spanW = 0;
			 \$span.html(\$tdDiv.html());
			 \$span.css('font-size', '' + \$tdDiv.css('font-size'));
			 \$span.css('padding-left', '' + \$tdDiv.css('padding-left'));
			 \$span.css('padding-right', '' + \$tdDiv.css('padding-right'));
			  spanW = \$span.width();
			  nw = (spanW > nw) ? spanW : nw;
		  });
		 \$span.remove();
		  //nw = (table.minWidth > nw) ? table.p.minWidth : nw;
		  nl = ol + (nw - ow);
		  $('div:eq(' + n + ')', table.cDrag).css('left', nl);
		  table.colresize = {
			  nw: nw,
			  n: n
		  };
		  table.dragEnd();
    
    }
	}
	
  $('.$this->tableName').flexigrid({
    url : 'csv-to-json.php',
    method: 'GET',
    params: [
      {
        name: 'dir',
        value:'{$this->dir}'
      }, {
        name: 'type',
        value:'{$this->type}'
      }, {
        name: 'query',
        value:'{$tableQuery}'
      }, {
        name: 'tag',
        value:'{$this->tag}'
      }, {
        name: 'run',
        value:'{$this->run}'
      }, {
        name: 'range',
        value:'{$this->range}'
      }, {
        name: 'level',
        value:'{$this->level}'
      }, {
        name: 'summary',
        value:'{$this->summary}'
      }, {
        name: 'csvfile',
        value:'{$this->csvFile}'
      }
TABLE_OUT;
if ($this->doComp == true) {
echo <<< TABLE_COMPARISON
      , {
        name: 'dirB',
        value:'{$this->dirB}'
      }, {
        name: 'typeB',
        value:'{$this->typeB}'
      }, {
        name: 'tagB',
        value:'{$this->tagB}'
      }, {
        name: 'runB',
        value:'{$this->runB}'
      }, {
        name: 'rangeB',
        value:'{$this->rangeB}'
      }, {
        name: 'levelB',
        value:'{$this->levelB}'
      }, {
        name: 'summaryB',
        value:'{$this->summaryB}'
      }, {
        name: 'csvfileB',
        value:'{$this->csvFileB}'
      }, {
        name: 'doComp',
        value:'1'
      }
TABLE_COMPARISON;
} 
echo <<< TABLE_OUT
    {$additionalArgs} ],
    dataType : 'json',
    colModel : [

TABLE_OUT;
    // DEFINE COLUMN HEADERS
    foreach($headerArray AS $key => &$column) {
      if ($key == 0) {
        echo "      {\n";
      } else {
        echo "      }, {\n";
      }
      echo "        display : '$column',\n";
      echo "        name: $key,\n";
      echo "        tooltip: '$tooltipArray[$key]',\n";
      echo "        sortable : true,\n";
      if (isset($this->toHide[$key]) && $this->toHide[$key] == true) {
        echo "        hide : true,\n";
      }
      if ($key == 0) {
        echo "        width : 225,\n";
        echo "        align : 'left'\n";
      } else {
        echo "        width : 100,\n";
        echo "        align : 'center'\n";
      }
    }
echo <<< TABLE_OUT
    } ],
    searchitems : [ 

TABLE_OUT;
    // DEFINE WHAT CAN BE SEARCHED
    foreach($this->searchable as $key => &$entry) {
      if (array_key_exists($key, $headerArray) === FALSE) continue;
      if ($key == 0) {
        echo "      {\n";
      } else {
        echo "      }, {\n";
      }
      echo "        display : '$headerArray[$key]',\n";
      echo "        name: {$key},\n";
      if ($key == 0) {
        echo "        isdefault : true,\n";
      } else {
        echo "        isdefault : false,\n";
      }
    }
echo <<< TABLE_OUT
    } ],
    sortname : {$this->sortCol},
    sortorder : "{$this->sortDir}",
    usepager : true,
    title : '{$this->tableTitle}',
    useRp : true,
    rp : 50,
    rpOptions: [10, 50, 100, 200, 500, 1000, 5000],
    showTableToggleBtn : true,
    width : 'auto',
    dblClickResize : true,
    height : 'auto',
    onSuccess : tableCallback
  });
	

  
// ]]>    
</script>
TABLE_OUT;
  }
  
}
?>
