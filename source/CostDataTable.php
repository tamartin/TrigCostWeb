<?php
include_once 'DataTable.php';

/* Cost DataTable, including additional Rates Pi Chart generation and luminosity comaprison */
class CostDataTable extends DataTable {

  function __construct() {
    parent::__construct();
    $this->compHeaderSetup = "";
    $this->colsToCompare = array();
  }

  function setComparison( $csvFile, $dir = "", $type = "", $tag = "", $run = "", $range = "", $level = "", $summary = "", $item = "" ) {
    parent::setComparison($csvFile, $dir, $type, $tag, $run, $range, $level, $summary, $item);

    include_once 'CompareCost.php';
    if (!isSummaryComparable($summary)){
      echo "<div class='err'>ERROR: Comparison for summary " . $summaryName . " not available!</div>";
      $this->doComp = false;
    } else {
      $this->compHeaderSetup = generateHeader($summary);
      $this->colsToCompare = getComparableColumnsIds($summary);
    }
  }

  function insertCPUPiChart() {
    // We are going to call the table generation script inline!
    // This will get us the data we can parse to make a pi chart
    $_GET['rp'] = 500; // Results to return
    $_GET['query'] = false; //no search
    $_GET['qtype'] = -1; // Column to search
    $_GET['dir'] = $this->dir; 
    $_GET['type'] = $this->type;
    $_GET['tag'] = $this->tag;     
    $_GET['run'] = $this->run; 
    $_GET['csvfile'] = $this->csvFile;
    $_GET['inline'] = TRUE;
    // Get the data
    ob_start(); // Start buffer to get result from request
    require("csv-to-json.php"); //execute request
    $jsonResult = ob_get_clean(); // Get result (json formatted string)
    $result = json_decode($jsonResult)->{"rows"}; // Convert to array - get data (only one entry)
    if (count($result) == 0) return false; // No data to display
    $cpuTable = array();

    // Need to locate the correct column
    $arrayLocation = array_search("Alg Total Time [%]", $this->getHeaderArray());
    if ($arrayLocation === FALSE) $arrayLocation = array_search("Group Total Time [%]", $this->getHeaderArray());
    foreach($result as $row) {
      foreach($row as $key => $data) {
        if ($key == 'id') continue;
        $name = $data[0];
        $name = str_ireplace("BW_","",$name);
        $name = str_ireplace("BW:","",$name);
        $cpu = $data[  $arrayLocation ];
        $cpuTable[ $name ] = $cpu;
      }
    }
    if (count($cpuTable) == 0) return false;
    arsort($cpuTable); //Sort into order
echo <<< PI_OUT
<script type="text/javascript">
// <![CDATA[

  google.load("visualization", "1", {packages:["corechart"]});
  google.setOnLoadCallback(drawChart);
  function drawChart() {

    var data = google.visualization.arrayToDataTable([
      ['Group', 'CPU Usage [%]', {role: 'style'}],

PI_OUT;
    $lastKey = end(array_keys($cpuTable));
    foreach ($cpuTable as $name => $cpu) {
      $fractionOfMax = $cpu / 50.;
      if ($fractionOfMax > 1.) $fractionOfMax = 1.;
      $fractionOfMax = intval($fractionOfMax * 255);
      $aStr = str_pad( base_convert($fractionOfMax,       10, 16), 2, '0', STR_PAD_LEFT);
      $bStr = str_pad( base_convert(255 - $fractionOfMax, 10, 16), 2, '0', STR_PAD_LEFT);
      $colourStr = '#' . 'ff' .  $bStr . '00';
      echo "      ['{$name}', {$cpu}, '{$colourStr}']";
      if ( $name != $lastKey ) echo ",";
      echo "\n";
    }
    $title = "CPU Usage Per Slice";
echo <<< PI_OUT
    ]);


    var options = {
      fontSize: 14,
      fontName: 'Helvetica',
      chartArea: {'width': '65%', 'height': '80%'},
      title: '{$title}',
      legend: { position: 'none' },
      bar: {groupWidth: '50%'},
      hAxis: {title: 'CPU Usage [%]' },
      vAxis: {allowContainerBoundaryTextCufoff: true}
    };

    var chart = new google.visualization.BarChart(document.getElementById("chart_values"));

    // Wait for the chart to finish drawing before calling the getImageURI() method.
    google.visualization.events.addListener(chart, 'ready', function () {
      document.getElementById('pngLink').outerHTML = '<a href="' + chart.getImageURI() + '">Link to Graph</a>';
    });

    chart.draw(data, options);
  }

// ]]>    
</script>
<div id="chart_values" style="width: 700px; height: 600px; display: block; margin: 0 auto;"></div>
<div id='pngLink'></div>
PI_OUT;
  }
  
  
  function insertAlgLinkTable($algs, $tableName) {
    // We are going to call the table generation script inline!
    // Manually set the required GETs
    $linkedSummary = $this->summary . "_Algorithm";
    // Want to get the CSV not for this page (e.g. chain) but for the chain's als page
    $checkCSV = getCsv($this->dir, $this->type, $this->tag, $this->run, $this->range, $this->level, $linkedSummary);

    // If Chain_Algorithm summary (mode 1) is missing, use algorithm summary (mode 2)
    if(!file_exists($checkCSV)){
      $checkCSV = getCsv($this->dir, $this->type, $this->tag, $this->run, $this->range, $this->level, $tableName);
      $mode = 2;
    } else {
      $mode = 1;
    }

    $_GET['rp'] = 100000; // Results to return
    $_GET['query'] = false;
    $_GET['qtype'] = -1; // Column to search
    $_GET['dir'] = $this->dir; 
    $_GET['type'] = $this->type;
    $_GET['tag'] = $this->tag;     
    $_GET['run'] = $this->run; 
    $_GET['csvfile'] = $checkCSV;
    $_GET['inline'] = TRUE;

    ob_start(); // Start buffer to get result from request
    require("csv-to-json.php"); //execute request
    $jsonResult = ob_get_clean(); // Get result (json formatted string)
    $result = json_decode($jsonResult, true);
    $rows = $result['rows'];

    $checkCSV = getCsv($this->dir, $this->type, $this->tag, $this->run, $this->range, $this->level, $tableName);
    $_GET['csvfile'] = $checkCSV;

    ob_start(); // Start buffer to get result from request
    require("csv-to-json.php"); //execute request
    $jsonResult = ob_get_clean(); // Get result (json formatted string)
    $result_all = json_decode($jsonResult, true);
    $rows_all = $result_all['rows'];

    global $separator;

    $table = "<div><table class='resultDisplay itemDetailsTable'>\n";
    $table .= "<tr><th class='centerText' style='font-weight:bold'>{$this->summary} {$tableName}s</th></tr>\n";
    if (count($algs) == 0) { // No Algorithms to display!
      $table .= "<tr><td class='centerText'>None</td></tr>\n";
    } else {

      $tableData = array();
      foreach ($algs as $key => &$alg) {
        // Check if we actually have any data for this alg
        $foundData = false;
        $myCalls = 0;
        if (isset($rows)) {
          foreach ($rows as $row) {
            $cell = $row['cell'];
            $name = $cell[0];
            $myCalls = $cell[2];
            $checkName = $this->item . $separator . $alg;
            // echo "check $checkName vs $name</br>";
            if ($checkName == $name) {
              $foundData = true;
              break;
            }
          }
        }

        // Check if we have all-chain data for this alg
        $foundAnyData = false;
        $allCalls = 0; 
        $timePerEvent = 0;
        if (isset($rows_all)) {
          foreach ($rows_all as $row) {
            $cell = $row['cell'];
            $name = $cell[0];
            $allCalls = $cell[2];
            $timePerEvent = $cell[10];
            if ($mode == 1){
              $algName = $alg;
            } else if ($tableName == "Chain") {
              $algName = $alg;
            }
            else {
              $algName = explode("/", $alg);
              $algName = (count($algName) > 1) ? $algName[1] : $algName[0];  # Run 2
            }
            // echo "check $algName vs $name</br>";
            if ($algName == $name) {
              $foundAnyData = true;
              break;
            }
          }
        }

        $linkedItem = $this->item . $separator . $alg;
        $urlThisChain = getLinkItem($this->dir, $this->type, $this->tag, $this->run, $this->range, $this->level, $linkedSummary, $linkedItem);

        if ($mode == 1) {
          $urlAllChains = getLinkItem($this->dir, $this->type, $this->tag, $this->run, $this->range, $this->level, $tableName, $alg);
        } else {
          // For example T2CaloEgammaReFastAlgo/FastCaloL2EgammaAlg -> FastCaloL2EgammaAlg
          if ($tableName == "Chain") $name = $alg;
          else {
            $name = explode("/", $alg);
            $name = (count($name) > 1) ? $name[1] : $name[0]; # Run 2
          }
          $urlAllChains = getLinkItem($this->dir, $this->type, $this->tag, $this->run, $this->range, $this->level, $tableName, $name);
        }

        // Save the data to the table
        $tableData[$key] = array("alg"=>$alg, "foundData"=>$foundData, "foundAnyData"=>$foundAnyData, "urlAllChains"=>$urlAllChains,
                                  "urlThisChain"=>$urlThisChain, "allCalls"=>$allCalls, "timePerEvent"=>$timePerEvent);

      }

      // Sort algorithm's chains by AllCalls
      if ($this->summary == 'Algorithm') {
        usort($tableData, function($a, $b) {
          if (!$b["foundAnyData"]) return -1;
          if (!$a["foundAnyData"]) return 1;
          return $b['allCalls'] <=> $a['allCalls'];
        });
      }

      # Print the table
      foreach ($tableData as $key => $rowData ) {
        $alg = $rowData["alg"];
        $foundData = $rowData["foundData"];
        $foundAnyData = $rowData["foundAnyData"];
        $allCalls = $rowData["allCalls"];
        $timePerEvent = $rowData["timePerEvent"];
        $urlAllChains = $rowData["urlAllChains"];
        $urlThisChain = $rowData["urlThisChain"];
        $table .=  "<tr><td class='centerText'>{$key}. {$alg}";
        if ($foundAnyData && $this->summary == 'Chain') {
          $table .=  " <a href='{$urlAllChains}' class='linkExternal'>AllChains calls:{$allCalls}</a>"; // Active Events for an algorithm
          $table .=  " <a href='{$urlAllChains}' class='linkExternal'>Mean Time per event:{$timePerEvent} [ms]</a>";
        } else if ($foundAnyData && $this->summary == 'Algorithm') {
          $table .=  " <a href='{$urlAllChains}' class='linkExternal'>AllChains calls:{$allCalls}</a>";
        } else if ($foundAnyData && $this->summary == 'Sequence') {
          $table .=  " <a href='{$urlAllChains}' class='linkExternal'>AllSequences calls:{$allCalls}</a>";
        }
        if ($foundData && $this->summary == 'Chain' ) {
          $table .=  " <a href='{$urlThisChain}' class='linkExternal'>ThisChain calls:{$myCalls}</a>";
        } else if ($foundData && $this->summary == 'Algorithm') {
          $table .=  " <a href='{$urlThisChain}' class='linkExternal'>ThisAlgorithm calls:{$myCalls}</a>";
        } else if ($foundData && $this->summary == 'Sequence') {
          $table .=  " <a href='{$urlThisChain}' class='linkExternal'>ThisSequence calls:{$myCalls}</a>";
        }
      }
    }

    $table .=  "</table></div>\n";
    return $table;
  }

  function insertTable($additionalArgs="") {
    $costArgs="";
    if ($this->doComp == true) {
      $cols = "";
      foreach($this->colsToCompare as $col){
        $cols = $cols . $col . ",";
      }

      $costArgs = ", {
          name: 'compCols',
          value:'{$cols}'
        }";
    }

    parent::insertTable($costArgs);
}
  
}
?>
