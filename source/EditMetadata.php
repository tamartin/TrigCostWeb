<?php 

function page_editMetadata(){
    global $err;

    $dir = isset($_POST['dir']) ? $_POST['dir'] : "";
    $type = isset($_POST['type']) ? $_POST['type'] : "";
    $tag = isset($_POST['tag']) ? $_POST['tag'] : "";
    $run = isset($_POST['run']) ? $_POST['run'] : "";

    if (checkMetadata($dir, $type, $tag, $run) != false) {
        $runMetadata = json_decode ( file_get_contents(getMetadata($dir, $type, $tag, $run)), true );
        $md = $runMetadata["children"];
    } else {
        $err[] = "ERROR: Cannot find metadata for run " . $run;
        echoErrorMsgs();
        return;
    }

    $oldDetails = getSingleMetadata($dir, $type, $tag, $run, "Details");

    echo "<p><strong> Edit metadata</strong></p>";
    echo '<form action="?dir=' . $dir . '" method="post">';
    echo "<p><textarea name='metadata' rows='5' cols='100'>{$oldDetails}</textarea></p>";
    echo "<input type='hidden' name='action' value='saveMetadata'/>";
    echo "<input type='hidden' name='dir' value='$dir'>";
    echo "<input type='hidden' name='tag' value='$tag'>";
    echo "<input type='hidden' name='type' value='$type'>";
    echo "<input type='hidden' name='run' value='$run'>";
    echo "<p><input type='submit' class='formBtn' value='Save Details'/></p>";
    echo "</form>";
}



function saveDetails(){
   
    $dir = isset($_POST['dir']) ? $_POST['dir'] : "";
    $type = isset($_POST['type']) ? $_POST['type'] : "";
    $tag = isset($_POST['tag']) ? $_POST['tag'] : "";
    $run = isset($_POST['run']) ? $_POST['run'] : "";
    $details = isset($_POST['metadata']) ? $_POST['metadata'] : "";

    setSingleMetadata($dir, $type, $tag, $run, "Details", $details);

    $cacheTag=$dir . "_" . $tag . "_" . $run;
    removeFromCache($cacheTag);
}


function removeFromCache($cacheTag, $filename="runs_table_cache.json"){
    $runsCache = file_exists($filename) ? json_decode(file_get_contents("runs_table_cache.json"), true) : array();
    if (array_key_exists($cacheTag, $runsCache)) {
        unset($runsCache[$cacheTag]);
    }

    file_put_contents($filename, json_encode($runsCache));
}

?>