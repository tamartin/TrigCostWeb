<?php 
include_once 'source/compareFromDatabase.php';

function page_compareRates() {

  // Read any POST data
  $compA = isset($_POST['compA']) ? sanitise($_POST['compA']) : NULL;
  $compB = isset($_POST['compB']) ? sanitise($_POST['compB']) : NULL;
  if (strpos($compB, "Paste the") !== FALSE) $compB = NULL;
  $lumi = isset($_POST['lumi']) ? sanitise($_POST['lumi']) : NULL;

  $costTable = new RatesDataTable();
  $costTable->setTitle("CompareRates");

  // Check if we want to load from the DB
  $saveKey = isset($_GET['key']) ? sanitise($_GET['key']) : NULL;
  $fromDB = FALSE;
  $loadFromDb = checkIfInDatabase($saveKey);
  if ($saveKey) {
    if ($loadFromDb !== NULL) {
      // We update from the DB
      $compA = $loadFromDb['COMPA'];
      $compB = $loadFromDb['COMPB'];
      $lumi = $loadFromDb['LUMI'];
      $fromDB = TRUE;
    } else {
      $compA = NULL;
      $compB = NULL;
      $lumi = NULL;
    }
  }

  $doParseA = (isset($compA) and $compA != "");
  $doParseB = (isset($compB) and $compB != "");

  // Decode
  $lumiA = 0;
  $lumiB = 0;
  if ($doParseA == TRUE) {
    $arrayA = array();
    parse_str(parse_url(htmlspecialchars_decode($compA), PHP_URL_QUERY), $arrayA);
    $dir = isset($arrayA["dir"]) ? $arrayA["dir"] : NULL;
    $type = isset($arrayA["type"])  ? $arrayA["type"] : NULL;
    $tag = isset($arrayA["tag"]) ? $arrayA["tag"] : NULL;
    $run = isset($arrayA["run"]) ? $arrayA["run"] : NULL;
    $range = isset($arrayA["range"]) ? $arrayA["range"] : NULL;
    $level = isset($arrayA["level"]) ? $arrayA["level"] : NULL;
    $summary = isset($arrayA["summary"]) ? $arrayA["summary"] : NULL;

    // Get the lumi of the A run
    $cmtStrA = "unknown";
    $dummy = ""; // Don't care how the L was selected
    getRatesLumiPoint($dir, $type, $tag, $run, $lumiA, $dummy, $cmtStrA );
  }

  if ($doParseB == TRUE) {
    $arrayB = array();
    parse_str(parse_url(htmlspecialchars_decode($compB), PHP_URL_QUERY), $arrayB);
    $dirB = isset($arrayB["dir"]) ? $arrayB["dir"] : NULL;
    $typeB = isset($arrayB["type"])  ? $arrayB["type"] : NULL;
    $tagB = isset($arrayB["tag"]) ? $arrayB["tag"] : NULL;
    $runB = isset($arrayB["run"]) ? $arrayB["run"] : NULL;
    $rangeB = isset($arrayB["range"]) ? $arrayB["range"] : NULL;
    $levelB = isset($arrayB["level"]) ? $arrayB["level"] : NULL;
    $summaryB = isset($arrayB["summary"]) ? $arrayB["summary"] : NULL;

    // Get the lumi of the B run
    $cmtStrB = "unknown";
    $dummy = ""; // Don't care how the L was selected
    getRatesLumiPoint($dirB, $typeB, $tagB, $runB, $lumiB, $dummy, $cmtStrB );
  }

  if ((strpos($tag, "Online") !== false) || ($doParseB && (strpos($tagB, "Online") !== false))){
      $costTable->configureOnlineTable();
  }

  if ($lumi == NULL) $lumi = ($lumiA != 0 ? $lumiA : "2e34");


  $doComparisonFormTemplate = new TemplateWrapper("comparison/update_comparison_form_rates.html");
  $doComparisonFormTemplate->setParam("COMP_A", $compA);
  $doComparisonFormTemplate->setParam("COMP_B", $compB);
  $doComparisonFormTemplate->setParam("LUMI", $lumi);
  $doComparisonFormTemplate->render();

  echo <<< EXPORT
  <div class="compareRatesGridWrapper">
  <div class="compareGridDoComparisonBtn"><input type="submit" class='formBtn' value="Do Comparison"/></form></div>
  
EXPORT;

  if ($doParseA == TRUE) {
    if (!is_numeric($lumi) or floatval($lumi) <= 0) {
      echo "</div>";
      echo "<div class='err'>ERROR: Non-numeric or invalid luminosity given.</div>";
    } else if (!isset($dir) or !isset($type) or !isset($tag) or !isset($run) or !isset($range) or !isset($level) or !isset($summary)) {
      echo "</div>";
      echo "<div class='err'>ERROR: Failed to parse cost data from URL A.</div>";
    } else if ( $doParseB == TRUE and (!isset($dirB) or !isset($typeB) or !isset($tagB) or !isset($runB) or !isset($rangeB) or !isset($levelB) or !isset($summaryB))) {
      echo "</div>";
      echo "<div class='err'>ERROR: Failed to parse cost data from URL B.</div>";
    } else if (checkCsv($dir, $type, $tag, $run, $range, $level, $summary) == 0) {
      echo "</div>";
      echo "<div class='err'>ERROR: Cannot find data for URL A.</div>";
    } else if ($doParseB == TRUE and (checkCsv($dirB, $typeB, $tagB, $runB, $rangeB, $levelB, $summaryB) == 0)) {
      echo "</div>";
      echo "<div class='err'>ERROR: Cannot find data for URL B.</div>";
    } else if (strpos($summary, "Rate_") === FALSE or ($doParseB == TRUE and strpos($summaryB, "Rate_") === FALSE)) {
      echo "</div>";
      echo "<div class='err'>ERROR: Both URLs must point to rates pages.</div>";
    } else if ($lumiA <= 0 or ($doParseB == TRUE and $lumiB <= 0)) {
      echo "</div>";
      echo "<div class='err'>ERROR: Was not able to determine the L point of the specified rates from the TrigCost processing metadata.</div>";
    } else if ( $doParseB == FALSE ) {
      echo "</div>";
      $lumi = floatval($lumi);
      if ($lumi < 1e10) $lumi *= 1e30;
      echo "<p>Generating rates <b>${cmtStrA}</b> at <b>L = " . sprintf("%.2e",$lumi) . " cm<sup>-2</sup>s<sup>-1</sup></b></p>";

      $csv  = getCsv($dir,  $type,  $tag,  $run,  $range,  $level,  $summary);

      $costTable->setDataSource( $csv,  $dir,  $type,  $tag,  $run,  $range,  $level,  $summary  );
      $costTable->setCompLumi( $lumi, $lumiA, $lumiB );
      $costTable->addSearchCol(0); // Name
      $costTable->addSearchCol(2); // Group
      $costTable->addSearchCol(8); // ID
      $costTable->insertTable();

    } else {
      $csv  = getCsv($dir,  $type,  $tag,  $run,  $range,  $level,  $summary);
      $csvB = getCsv($dirB, $typeB, $tagB, $runB, $rangeB, $levelB, $summaryB);

      // Allow quick switch between L1, HLT and group
      if ($summary == "Rate_ChainL1") {
        echo '<div class="compareGridSwitchLeftBtn">';
        echo "  <form action='?page=CompareRates' method='post'>";
        echo "  <input type='hidden' name='compA' value='" . str_replace("Rate_ChainL1", "Rate_ChainHLT", $compA) . "' />";
        echo "  <input type='hidden' name='compB' value='" . str_replace("Rate_ChainL1", "Rate_ChainHLT", $compB) . "' />";
        echo "  <input type='hidden' name='compB' value='" . str_replace("Rate_ChainL1", "Rate_ChainHLT", $compB) . "' />";
        echo "  <input type='hidden' name='lumi' value='" . sprintf("%.2e", $lumi) . "'/>";
        echo "  <input type='submit' class='formBtn' value='Switch to HLT Rates'/></form>";
        echo '</div><div class="compareGridSwitchRightBtn">';
        echo "  <form action='?page=CompareRates' method='post'>";
        echo "  <input type='hidden' name='compA' value='" . str_replace("Rate_ChainL1", "Rate_Group", $compA) . "' />";
        echo "  <input type='hidden' name='compB' value='" . str_replace("Rate_ChainL1", "Rate_Group", $compB) . "' />";
        echo "  <input type='hidden' name='lumi' value='" . sprintf("%.2e", $lumi) . "'/>";
        echo "  <input type='submit' class='formBtn' value='Switch to Group Rates'/></form>";
        echo "</div>";
      } else if ($summary == "Rate_ChainHLT") {
        echo '<div class="compareGridSwitchLeftBtn">';
        echo "  <form action='?page=CompareRates' method='post'>";
        echo "  <input type='hidden' name='compA' value='" . str_replace("Rate_ChainHLT", "Rate_ChainL1", $compA) . "' />";
        echo "  <input type='hidden' name='compB' value='" . str_replace("Rate_ChainHLT", "Rate_ChainL1", $compB) . "' />";
        echo "  <input type='hidden' name='lumi' value='" . sprintf("%.2e", $lumi) . "'/>";
        echo "  <input type='submit' class='formBtn'  value='Switch to L1 Rates'/></form>";
        echo '</div><div class="compareGridSwitchRightBtn">';
        echo "  <form action='?page=CompareRates' method='post'>";
        echo "  <input type='hidden' name='compA' value='" . str_replace("Rate_ChainHLT", "Rate_Group", $compA) . "' />";
        echo "  <input type='hidden' name='compB' value='" . str_replace("Rate_ChainHLT", "Rate_Group", $compB) . "' />";
        echo "  <input type='hidden' name='lumi' value='" . sprintf("%.2e", $lumi) . "'/>";
        echo "  <input type='submit' class='formBtn'  value='Switch to Group Rates'/></form>";
        echo "</div>";
      } else if ($summary == "Rate_Group") {
        echo '<div class="compareGridSwitchLeftBtn">';
        echo "  <form action='?page=CompareRates' method='post'>";
        echo "  <input type='hidden' name='compA' value='" . str_replace("Rate_Group", "Rate_ChainL1", $compA) . "' />";
        echo "  <input type='hidden' name='compB' value='" . str_replace("Rate_Group", "Rate_ChainL1", $compB) . "' />";
        echo "  <input type='hidden' name='lumi' value='" . sprintf("%.2e", $lumi) . "'/>";
        echo "  <input type='submit' class='formBtn'  value='Switch to L1 Rates'/></form>";
        echo '</div><div class="compareGridSwitchRightBtn">';
        echo "  <form action='?page=CompareRates' method='post'>";
        echo "  <input type='hidden' name='compA' value='" . str_replace("Rate_Group", "Rate_ChainHLT", $compA) . "' />";
        echo "  <input type='hidden' name='compB' value='" . str_replace("Rate_Group", "Rate_ChainHLT", $compB) . "' />";
        echo "  <input type='hidden' name='lumi' value='" . sprintf("%.2e", $lumi) . "'/>";
        echo "  <input type='submit' class='formBtn'  value='Switch to HLT Rates'/></form>";
        echo "</div>";
      }

      $saveKey = addToDB($saveKey, $compA, $compB, $lumi);

      $lumi = floatval($lumi);
      if ($lumi < 1e10) $lumi *= 1e30;
      if ($saveKey != NULL) {
        echo '<div class="compareGridShareComparison">';
        echo "  <p><b>To share this comparison, please use <a href='?page=CompareRates&key={$saveKey}'>this link</a></b></p>";
        echo '</div>';
      }
      echo '<div class="compareGridLumi">';
      echo "  <p>Rates comparison at <b>L = " . sprintf("%.2e",$lumi) . " cm<sup>-2</sup>s<sup>-1</sup></b></p>";
      echo '</div><div class="compareGridCache">';
      echo "  <p>Comparing Cache A: <b>${cmtStrA}</b> to Cache B: <b>${cmtStrB}</b></p>";
      echo '</div>';

      $costTable->setDataSource( $csv,  $dir,  $type,  $tag,  $run,  $range,  $level,  $summary  );
      $costTable->setComparison( $csvB, $dirB, $typeB, $tagB, $runB, $rangeB, $levelB, $summaryB );
      $costTable->setCompLumi( $lumi, $lumiA, $lumiB );
      $costTable->addSearchCol(1); // Group
      $costTable->addSearchCol(2); // Matched
      $costTable->addSearchCol(3); // Matched ID
      $costTable->insertTable();
    }
  }
}

?>
