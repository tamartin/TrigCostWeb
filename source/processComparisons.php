<?php 
// Set of helper functions supporting the cost and rates comparisons

function checkIfOnline ($tag) {
    return (strpos($tag, "Online") !== false);
}

function findColumn ($csv, $colName) {
    $csvRow = $csv[0];
    foreach($csvRow as $colNumber => $colHeader){
        if (strcmp($colHeader, $colName) == 0) {
            return $colNumber;
        }
    }

    return -1;
}

function getRateColumn ($isOnline) {
    return $isOnline ? 2 : 3;
}

function getRateErrColumn ($isOnline) {
    return $isOnline ? 3 : 4;
}

function getGroupColumn ($isOnline) {
    return $isOnline ? 1 : 2;
}

// To match '-' and '.' with '_' 
function sanitizeL1Name($name) {
    return str_replace("-","_", str_replace(".","_", $name));
}

function isL1Item($name) {
    return (substr($name, 0, 3) == "L1_");
}

// Remove first row (title data) and second row (tooltip data)
function sanitizeCsvFile ($csv) {
    foreach($csv as $key => &$row){
        unset($csv[$key]); // Remove first row (title data) and second row (tooltip data)
        if ($key == 1) break;
    }
    return $csv;
}


  
?>