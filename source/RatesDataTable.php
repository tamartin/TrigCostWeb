
<?php
include_once 'DataTable.php';

/* Rates DataTable, including additional Rates Pi Chart generation and luminosity comaprison */
class RatesDataTable extends DataTable {
    
    var $compLumi; // Rates comparison target lumi
    var $lumiA; // Actual lumi of comparison A
    var $lumiB; // Actual lumi of comparison B

    function __construct() {
        parent::__construct();
        $this->compLumi = 0;
        $this->lumiA = 0;
        $this->lumiB = 0;
        $this->compHeaderSetup = "Name,Group,Matched,Same ID,Rate A [Hz],Rate Err A [Hz],Rate B [Hz],Rate Err B [Hz],Prescale A, Prescale B,Difference (A-B) [Hz],Abs. Difference Sig. [|σ|],Ratio (A/B),Ratio Err";
    }

    function setCompLumi($lumi, $lumiA, $lumiB) {
        $this->compLumi = $lumi;
        $this->lumiA = $lumiA;
        $this->lumiB = $lumiB;
    }

    function configureOnlineTable(){
      $this->compHeaderSetup = "Name,Group,Matched,Rate A [Hz],Rate Err A [Hz],Rate B [Hz],Rate Err B [Hz],Prescale A, Prescale B,Difference (A-B) [Hz],Abs. Difference Sig. [|σ|],Ratio (A/B),Ratio Err";
    }

    function insertRatesPiChart() {
        // We are going to call the table generation script inline!
        // This will get us the data we can parse to make a pi chart
        $_GET['rp'] = 500; // Results to return
        $_GET['query'] = false; //no search
        $_GET['qtype'] = -1; // Column to search
        $_GET['dir'] = $this->dir; 
        $_GET['type'] = $this->type;
        $_GET['tag'] = $this->tag;     
        $_GET['run'] = $this->run; 
        $_GET['csvfile'] = $this->csvFile;
        $_GET['inline'] = TRUE;
        // Get the data
        ob_start(); // Start buffer to get result from request
        require("csv-to-json.php"); //execute request
        $jsonResult = ob_get_clean(); // Get result (json formatted string)
        $result = json_decode($jsonResult)->{"rows"}; // Convert to array - get data (only one entry)
        if (count($result) == 0) return false; // No data to display
        $groupRates = array();
        $L0Rate = 0.;
        $L1Rate = 0.;
        $HLTRate = 0.;
        $PhysicsRate = 0.;
        // Need to locate the correct column
        $arrayLocation = array_search("Weighted PS Rate [Hz]", $this->getHeaderArray());
        foreach($result as $row) {
          foreach($row as $key => $data) {
            if ($key == 'id') continue;
            $name = $data[0];
            $rate = $data[  $arrayLocation ];
            if ( strpos($name, "CPS") !== FALSE) continue; // Don't put CPS groups in the graphic
            if ( strpos($name, "STREAM_") !== FALSE) continue; // Don't put Streams in the graphic
            if (abs($rate) < 1e-5) continue; // Is zero
            if ($name == 'RATE_GLOBAL_L0') {
              $L0Rate = number_format( $rate );
              continue;
            }
            if ($name == 'RATE_GLOBAL_L1') {
              $L1Rate = number_format( $rate );
              continue;
            }
            if ($name == 'RATE_GLOBAL_HLT') {
              $HLTRate = number_format( $rate );
              continue;
            }
            if ($name == 'RATE_GLOBAL_PHYSICS') {
              $PhysicsRate = number_format( $rate );
              continue;
            }
            if ($name == 'STREAM:Main'){
              $PhysicsRate = number_format( $rate );
              // Include STREAM:Main on the graph along with all other streams
            }
            $groupRates[str_ireplace("RATE_","",$name)] = $rate;
          }
        }
        //print_r($groupRates);
        if (count($groupRates) == 0) return false;
        asort($groupRates); //Sort into order
    echo <<< PI_OUT
    <script type="text/javascript">
    // <![CDATA[
    
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
    
        var data = google.visualization.arrayToDataTable([
          ['Group', 'Rate [Hz]', {role: 'style'}],
    
    PI_OUT;
        $keys = array_keys($groupRates);
        $lastKey = end($keys);
        foreach ($groupRates as $name => $rate) {
          $maxSliceRate = 250.;
          $fractionOfMax = $rate / $maxSliceRate;
          if ($fractionOfMax > 1.) $fractionOfMax = 1.;
          $fractionOfMax = intval($fractionOfMax * 255);
          $aStr = str_pad( base_convert($fractionOfMax,       10, 16), 2, '0', STR_PAD_LEFT);
          $bStr = str_pad( base_convert(255 - $fractionOfMax, 10, 16), 2, '0', STR_PAD_LEFT);
          $colourStr = '#' . $aStr . '00' . $bStr;
          if ( strpos($this->tag, "13TeV") !== FALSE) $colourStr = '#' . $aStr . $bStr . '00';
          echo "      ['{$name}', {$rate}, '{$colourStr}']";
          if ( $name != $lastKey ) echo ",";
          echo "\n";
        }
        if ($L0Rate > 0) $title = "Global Rate L0: {$L0Rate} Hz, L1: {$L1Rate} Hz, HLT: {$HLTRate} Hz";
        else if ($PhysicsRate > 0) $title = "Global Rate L1: {$L1Rate} Hz, HLT: {$HLTRate} Hz, Physics: {$PhysicsRate} Hz";
        else $title = "Global Rate L1: {$L1Rate} Hz, HLT: {$HLTRate} Hz";
    echo <<< PI_OUT
        ]);
    
    
        var options = {
          fontSize: 10,
          fontName: 'Helvetica',
          chartArea: {'width': '75%', 'height': '75%'},
          title: '{$title}',
          legend: { position: 'none' },
          bar: {groupWidth: '50%'},
          vAxis: {logScale: true, title: 'Rate [Hz]' },
          hAxis: {allowContainerBoundaryTextCufoff: true}
        };
    
        var chart = new google.visualization.ColumnChart(document.getElementById("chart_values"));
    
        // Wait for the chart to finish drawing before calling the getImageURI() method.
        google.visualization.events.addListener(chart, 'ready', function () {
          document.getElementById('pngLink').outerHTML = '<a href="' + chart.getImageURI() + '">Link to Graph</a>';
        });
    
        chart.draw(data, options);
      }
    
    // ]]>    
    </script>
    <br/>
    <div id='pngLink'></div>
    <div id="chart_values" style="width: 1200px; height: 700px; display: block; margin: 0 auto;"></div>
    PI_OUT;
      }

    function insertTable($additionalArgs="") {
        // Retrieve rates.json file
        if (checkXML($this->dir, $this->type, $this->tag, $this->run, $this->range, $this->level, $this->summary) !== 0) {
            $rateFile = getXML($this->dir, $this->type, $this->tag, $this->run, $this->range, $this->level, $this->summary);
            $rateFileB = getXML($this->dirB, $this->typeB, $this->tagB, $this->runB, $this->rangeB, $this->levelB, $this->summaryB);
        } else if (checkJSON($this->dir, $this->type, $this->tag, $this->run, $this->range, $this->level, $this->summary) !== 0){
            $rateFile = getJSON($this->dir, $this->type, $this->tag, $this->run, $this->range, $this->level, $this->summary);
            $rateFileB = getJSON($this->dirB, $this->typeB, $this->tagB, $this->runB, $this->rangeB, $this->levelB, $this->summaryB);
        }
        if (isset($rateFile)){
            $strA = "";
            if ($this->doComp == true) $strA = " A";

            if ($this->doComp == true && isset($rateFileB)) {
              $explodedFileA = explode("/", $rateFile);
              $rateFilename = $explodedFileA[count($explodedFileA)-2] . "/" . $explodedFileA[count($explodedFileA)-1];
              $explodedFileB = explode("/", $rateFileB);
              $rateFilenameB = $explodedFileB[count($explodedFileB)-2] . "/" . $explodedFileB[count($explodedFileB)-1];
              echo '<div class="compareGridFileRatesA">';
              echo "  <p>File of rates{$strA}: <a href='{$rateFile}'>{$rateFilename}</a></p>";
              echo '</div>';
              echo '<div class="compareGridFileRatesB">';
              echo "<p>File of rates B: <a href='{$rateFileB}'>{$rateFilenameB}</a></p>";
              echo '</div>';
            } else {
              echo '<div class="compareGridShareComparison">'; // The File A link should be displayed on the left side in non comparison mode
              echo "  <p>File of rates{$strA}: <a href='{$rateFile}'>{$rateFile}</a></p>";
              echo '</div>';
            }
        }

        echo '</div>';
        
        $lumiArgs=", {
            name: 'lumiA',
            value:'{$this->lumiA}'
          }, {
            name: 'compLumi',
            value:'{$this->compLumi}'
          }";
        if ($this->doComp == true) {
            $lumiArgs = $lumiArgs . ", {
                name: 'lumiB',
                value:'{$this->lumiB}'
              }";
        }

        parent::insertTable($lumiArgs);
    }
}
?>
