#!/bin/bash

RUNNUMBER=$1
DATATAG=$2
STARTTS=$3
ENDTS=$4

echo "Creating plot for run $RUNNUMBER datatag $DATATAG $STARTTS - $ENDTS"

RELEASE="23.0.26"
export SITE_NAME=CERN-PROD
export REPO=/cvmfs/atlas.cern.ch/repo
export ATLAS_LOCAL_ROOT_BASE=$REPO/ATLASLocalRootBase
echo "Execute] Source setup tdaq-10-00-00"
source /cvmfs/atlas.cern.ch/repo/sw/tdaq/tools/cmake_tdaq/bin/cm_setup.sh tdaq-10-00-00 # libpbeastpy library
echo "Execute] Source atlasLocalSetup.sh"
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
export LCG_RELEASE_BASE=/cvmfs/sft.cern.ch/lcg/releases
echo "Execute] Setup athena"
asetup Athena,$RELEASE --noLcgReleaseBase
export PATH=/cvmfs/sft.cern.ch/lcg/releases/LCG_102b/auth_get_sso_cookie/1.5.0-1/x86_64-centos7-gcc11-opt/bin:${PATH}
export PBEAST_SERVER_SSO_SETUP_TYPE=AutoUpdateKerberos # pbeast authentication

# Run python script
python performancePlotsScripts/TrigCostMakeTimeVsSlotsPlot.py --runNumber $RUNNUMBER --dataTag $DATATAG --timeStart "$STARTTS" --timeEnd "$ENDTS"
python performancePlotsScripts/TrigCostMakeMostExpensiveAlgsPlot.py --runNumber $RUNNUMBER --dataTag $DATATAG --timeStart "$STARTTS" --timeEnd "$ENDTS"
