<?php
include_once 'source/functions.php';
include_once 'source/processComparisons.php';

$page = isset($_GET['page']) ? sanitise($_GET['page']) : 1; // Current page number
$rp = isset($_GET['rp']) ? sanitise($_GET['rp']) : 10; // Number of records to return
$sortname = isset($_GET['sortname']) ? sanitise($_GET['sortname']) : 1; // Name of column to sort by
$sortorder = isset($_GET['sortorder']) ? sanitise($_GET['sortorder']) : 'desc'; // Sort order (asc or desc)
$query = isset($_GET['query']) ? sanitise($_GET['query']) : false; // Search text
$qtype = isset($_GET['qtype']) ? sanitise($_GET['qtype']) : -1; // Column to search

$inline = isset($_GET['inline']) ? sanitise($_GET['inline']) : NULL;
$doComp = isset($_GET['doComp']) ? sanitise($_GET['doComp']) : 0;
$compLumi = isset($_GET['compLumi']) ? sanitise($_GET['compLumi']) : 1;
$lumiA = isset($_GET['lumiA']) ? sanitise($_GET['lumiA']) : 1;
$lumiB = isset($_GET['lumiB']) ? sanitise($_GET['lumiB']) : 1;

$dir = isset($_GET['dir']) ? sanitise($_GET['dir']) : NULL; // Data source
$type = isset($_GET['type']) ? sanitise($_GET['type']) : ""; 
$tag = isset($_GET['tag']) ? sanitise($_GET['tag']) : NULL;
$run = isset($_GET['run']) ? sanitise($_GET['run']) : NULL;
$range = isset($_GET['range']) ? sanitise($_GET['range']) : NULL;
$level = isset($_GET['level']) ? sanitise($_GET['level']) : NULL;
$summary = isset($_GET['summary']) ? sanitise($_GET['summary']) : NULL;
$csvFile = isset($_GET['csvfile']) ? sanitise($_GET['csvfile']) : NULL;

// B is set if doing a comparison
$dirB = isset($_GET['dirB']) ? sanitise($_GET['dirB']) : NULL; // Data source
$typeB = isset($_GET['typeB']) ? sanitise($_GET['typeB']) : ""; 
$tagB = isset($_GET['tagB']) ? sanitise($_GET['tagB']) : NULL;
$runB = isset($_GET['runB']) ? sanitise($_GET['runB']) : NULL;
$rangeB = isset($_GET['rangeB']) ? sanitise($_GET['rangeB']) : NULL;
$levelB = isset($_GET['levelB']) ? sanitise($_GET['levelB']) : NULL;
$summaryB = isset($_GET['summaryB']) ? sanitise($_GET['summaryB']) : NULL;
$csvFileB = isset($_GET['csvfileB']) ? sanitise($_GET['csvfileB']) : NULL;

$compCols = isset($_GET['compCols']) ? sanitise($_GET['compCols']) : NULL;

$csv = array_map('str_getcsv', file($csvFile));

// Remove first and second rows
$psColumnA = findColumn($csv, "Prescale");
$idColumnA = findColumn($csv, "ID");
$csv = sanitizeCsvFile($csv);

//////////////////////////////////////////////////////////////////////////////
if ($doComp == 1 and strpos($summary, "Rate") !== FALSE) { // RATES COMPARISON MODE
//////////////////////////////////////////////////////////////////////////////
  $onlineModeA = checkIfOnline($tag);
  $onlineModeB = checkIfOnline($tagB);
  $onlineMode = $onlineModeA || $onlineModeB;

  $csvB = array_map('str_getcsv', file($csvFileB));
  $psColumnB = findColumn($csvB, "Prescale");
  $idColumnB = findColumn($csvB, "ID");
  $csvB = sanitizeCsvFile($csvB);

  // Calc scale factors
  $scaleA = floatval($compLumi) / floatval($lumiA);
  $scaleB = floatval($compLumi) / floatval($lumiB);

  $new =  array();
  foreach($csv as $keyA => &$rowA) {
    // Find match
    $found = false;
    $nameA = isL1Item($rowA[0]) ? sanitizeL1Name($rowA[0]) : $rowA[0];
    foreach($csvB as $keyB => &$rowB) {
      $nameB = isL1Item($rowB[0]) ? sanitizeL1Name($rowB[0]) : $rowB[0];
      
      if ( $nameA == $nameB ) {
        $name = $rowA[0];
        $group = $rowA[getGroupColumn($onlineModeA)];
        $matched = "Both";
        $rateA = floatval($rowA[getRateColumn($onlineModeA)]) * $scaleA;
        $rateErrA = floatval($rowA[getRateErrColumn($onlineModeA)]) * $scaleA;
        $prescaleA = $rowA[$psColumnA];
        if (strpos($prescaleA, "-1") !== false) {
          $rateA = 0;
          $rateErrA = 0;
        } 

        // var_dump(getRateColumn($onlineModeB));
        // var_dump(floatval($rowB[getRateColumn($onlineModeB)]));
        // var_dump($rowB);

        $rateB = floatval($rowB[getRateColumn($onlineModeB)]) * $scaleB;
        $rateErrB = floatval($rowB[getRateErrColumn($onlineModeB)]) * $scaleB;
        $prescaleB = $rowB[$psColumnB];
        if(strpos($prescaleB, "-1") !== false) {
          $rateB = 0;
          $rateErrB = 0;
        } 

        if (!$onlineMode){ // Online rates don't have id
          $idA = intval($rowA[$idColumnA]);
          $idB = intval($rowB[$idColumnB]);
          $idMatched = ($idA == $idB ? "Yes" : "No");
        } 

        $rateDiff = $rateA - $rateB;
        $rateRatio = "-";
        $rateRatioErr = "-";
        $totErr = sqrt( pow($rateErrA,2) + pow($rateErrB,2) );
        $significance = "-";
        if ($totErr > 0) $significance = abs($rateDiff / $totErr);
        if ($rateB > 0 and $rateA > 0) {
          $rateRatio = $rateA / $rateB;
          $rateRatioErr = $rateRatio * sqrt( pow($rateErrA/$rateA, 2) + pow($rateErrB/$rateB, 2) );
        } 

        if (!$onlineMode){
          $new[] = array($name, $group, $matched, $idMatched, sprintf("%.4f",$rateA), sprintf("%.4f",$rateErrA), sprintf("%.4f",$rateB), sprintf("%.4f",$rateErrB), $prescaleA, $prescaleB, sprintf("%.4f",$rateDiff), sprintf("%.3f",$significance), sprintf("%.3f",$rateRatio), sprintf("%.3f",$rateRatioErr));
        } else {
          $new[] = array($name, $group, $matched, sprintf("%.4f",$rateA), sprintf("%.4f",$rateErrA), sprintf("%.4f",$rateB), sprintf("%.4f",$rateErrB), $prescaleA, $prescaleB, sprintf("%.4f",$rateDiff), sprintf("%.3f",$significance), sprintf("%.3f",$rateRatio), sprintf("%.3f",$rateRatioErr));
        }
        
        $found = true;
        break;
      }
    }
    if ($found == false) {
      // Just have A
      $name = $rowA[0];
      $group = $rowA[getGroupColumn($onlineModeA)];
      $matched = "Only A";
      $rateA = floatval($rowA[getRateColumn($onlineModeA)]) * $scaleA;
      $rateErrA = floatval($rowA[getRateErrColumn($onlineModeA)]) * $scaleA;
      $prescaleA = $rowA[$psColumnA];
      if(strpos($prescaleA, "-1") !== false) {
        $rateA = 0;
        $rateErrA = 0;
      } 
      if (!$onlineMode){
        $new[] = array($name, $group, $matched, "-", sprintf("%.4f",$rateA), sprintf("%.4f",$rateErrA), "-", "-", $prescaleA, "-", "-", "-", "-", "-");
      } else {
        $new[] = array($name, $group, $matched, sprintf("%.4f",$rateA), sprintf("%.4f",$rateErrA),  "-", "-", $prescaleA, "-", "-", "-", "-", "-");
      }
    }
  }

  foreach($csvB as $keyB => &$rowB) {
    // Find match
    $found = false;
    $nameB = isL1Item($rowB[0]) ? sanitizeL1Name($rowB[0]) : $rowB[0];
    foreach($csv as $keyA => &$rowA) {
      $nameA = isL1Item($rowA[0]) ? sanitizeL1Name($rowA[0]) : $rowA[0];
      if ( $nameB == $nameA ) {
        $found = true;
        break;
      }
    }
    if ($found == false) {
      // Just have B
      $name = $rowB[0];
      $group = $rowB[getGroupColumn($onlineModeB)];
      $matched = "Only B";
      $rateB = floatval($rowB[getRateColumn($onlineModeB)]) * $scaleB;
      $rateErrB = floatval($rowB[getRateErrColumn($onlineModeB)]) * $scaleB;
      $prescaleB = $rowB[$psColumnB];
      if(strpos($prescaleB,"-1") !== false) {
        $rateB = 0;
        $rateErrB = 0;
      } 

      if (!$onlineMode){
        $new[] = array($name, $group, $matched, "-", "-", "-", sprintf("%.4f",$rateB), sprintf("%.4f",$rateErrB), "-", $prescaleB, "-", "-", "-", "-");
      } else {
        $new[] = array($name, $group, $matched, "-", "-", sprintf("%.4f",$rateB), sprintf("%.4f",$rateErrB), "-", $prescaleB, "-", "-", "-", "-");
      }
    
    }
  }

  $csv = $new;

} else if ($doComp == 1){ // COST COMPARISON MODE
  $colsToCompare = array_filter(explode(",", $compCols));
  $csvB = array_map('str_getcsv', file($csvFileB));
  $csvB = sanitizeCsvFile($csvB);

  $new =  array();
  foreach($csv as $keyA => &$rowA) {
    // Find match
    $found = false;
    $nameA = $rowA[0];

    foreach($csvB as $keyB => &$rowB) {
      $nameB = $rowB[0];

      if ( $nameA == $nameB ) {
        $matched = "Yes";
        $found = true;
        break;
      }
    }

    $newRow = array($nameA);
    if ($found == true){
      // Fill the new row with values from columns to compare
      array_push($newRow, "Yes"); // Matched
      foreach($colsToCompare as $col){
        $relChange = $rowA[$col] != 0 ? 100*abs($rowB[$col]-$rowA[$col])/$rowA[$col] : 0;
        array_push($newRow, sprintf("%.2f",$rowA[$col]), sprintf("%.2f",$rowB[$col]), sprintf("%.2f",$rowA[$col]-$rowB[$col]), sprintf("%.2f",$relChange));
      }
    } else {
      array_push($newRow, "Only A"); // Matched
      foreach($colsToCompare as $col){
        array_push($newRow, sprintf("%.2f",$rowA[$col]), "-", "-", "-");
      }
    }

    $new[] = $newRow;
  }

  foreach($csvB as $keyB => &$rowB) {
    $nameB = $rowB[0];
    $found = false;
    foreach($csv as $keyA => &$rowA) {
      $nameA = $rowA[0];
      if ( $nameB == $nameA ) {
        $found = true;
        break;
      }
    }

    if ($found == false){
      $newRow = array($nameB, "Only B"); // Name, Matched
      foreach($colsToCompare as $col){
        array_push($newRow, "-", sprintf("%.2f",$rowB[$col]), "-", "-");
      }

      $new[] = $newRow;
    }
  }

  $csv = $new;

} else if ($compLumi != $lumiA) { // just luminosity scaling
  $new =  array();
  $scale = floatval($compLumi) / floatval($lumiA);
  foreach($csv as $key => &$row) {
    $row[3] = number_format(floatval($row[3]) * $scale, 4, '.', '');
    $row[4] = number_format(floatval($row[4]) * $scale, 4, '.', '');
    $row[5] = number_format(floatval($row[5]) * $scale, 4, '.', '');
    $row[6] = number_format(floatval($row[6]) * $scale, 4, '.', '');
    $row[12] = number_format(floatval($row[12]) * $scale, 4, '.', '');
  }
}

// Do search of table 
//echo " searchgin for " . $qtype . " and " . $query . "\n";
if($qtype >= 0 && $query){
  $query = strtolower(trim($query));
  foreach($csv AS $key => $row){
    #echo "Checking if row " . $qtype . " with data " . strtolower($row[$qtype]) . " and key " . $key . " contains " . $query . "\n";
    #echo ((fnmatch($query, strtolower($row[$qtype])) === true) ? "t" : "f") . "<br>";
    
    if(strpos(strtolower($row[$qtype]),$query) !== false) {
      // Keep
    } else if (fnmatch($query, strtolower($row[$qtype])) === true) {
      // Keep
    } else {
      // Remove
      unset($csv[$key]);
    }
  }
}

//Make PHP handle the sorting
$sortArray = array();
foreach($csv AS $key => $row) {
   $sortArray[$key] = $row[$sortname];
}
$sortMethod = SORT_ASC;
if($sortorder == 'desc'){
  $sortMethod = SORT_DESC;
}
array_multisort($sortArray, $sortMethod, $csv);
$total = count($csv);

# Look for exact match
if ($rp == 1) {
  foreach ($csv as $key => $row) {
    if (strtolower($row[$qtype]) == $query) {
      $csv = [$row];
      break;
    }
  }
} 
$csv = array_slice($csv,($page-1)*$rp, $rp);


if ( !isset($inline) && isset($dir) && isset($tag) && isset($run) && isset($range) && isset($level) && isset($summary) ) {
  foreach($csv as $key => &$row) {
    //print_r($row[0]);
    //print "\n";
    $url = getLinkItem($dir, $type, $tag, $run, $range, $level, $summary, $row[0]);
    $new = "<a href='{$url}'>$row[0]</a>";
    $row[0] = $new;
    //print_r($row[0]);
    //print "\n";
  }
}

// Convert to JSON
$rowId = 0;
if (!isset($inline)) header("Content-type: application/json");
$jsonTableData = array('page'=>$page,'total'=>$total,'rows'=>array());
foreach($csv as &$row){
  //print_r($row);
  $entry = array('id'=>$rowId++, 'cell'=>$row   );
  //print_r($entry);
  //print"-----------------\n";
  $jsonTableData['rows'][] = $entry;
}
echo json_encode($jsonTableData);
?>
