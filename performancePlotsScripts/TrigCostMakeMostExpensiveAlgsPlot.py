#!/usr/bin/env python

import matplotlib.pyplot as plt
import matplotlib.ticker as tck
import numpy as np

from Util import *

# Read which lumiblocks were cost monitored to only include them on the plots
def getCostMonitoredLumiblocks(lbToMuMap, csvPath):
    monitoredLbs = []
    ranges = prepareCostRanges(lbToMuMap.keys())
    for lbRange in ranges:
        lb = lbRange*50 # Convert range id to the first lumiblock number -> start of range that is used in costmon
        rangeData = readDataFromCostFile(csvPath, lb, [], "Global")
        if rangeData.empty:
            continue

        for index, row in rangeData.iterrows():
            currentLb = int(row["Name"].replace("LumiBlock_", ""))

            if currentLb not in lbToMuMap:
                continue
            monitoredLbs.append(currentLb)
    return monitoredLbs


# Get Average per range of 50 lbs
def getAvgPerRange(lbToMuMap):
    rangeMu = []
    currentRange = -1
    rangeToMuMap = {}
    for lb in lbToMuMap:
        rangeId = getRangeId(lb)
        if rangeId != currentRange:
            if currentRange > -1 and len(rangeMu) > 0: 
                rangeToMuMap[currentRange] = sum(rangeMu) / len(rangeMu)
            currentRange = rangeId
            rangeMu = []
            
        #if rangeId in costMonRanges[runNumber] and lb in costMonRanges[runNumber][rangeId]:
        rangeMu.append(lbToMuMap[lb])
        
    return rangeToMuMap


def prepareAvailableSlots(lbToMuMap, lbToSlotsMap, monitoredLbs):
    wrappedData = []
    for lb in lbToMuMap:
        if lb in lbToSlotsMap and lb in monitoredLbs:
            wrappedData.append([lb, lbToMuMap[lb], lbToSlotsMap[lb]])

    return pd.DataFrame(wrappedData, columns=['LumiBlock', "Mu", 'Available Slots'])


def plotData(df, slotsDf, runNumber, outputDir, topAlgNames):
    fig, ax = plt.subplots(figsize=(12, 8), dpi=80)
    #colorlist = ["olive", "forestgreen", "lightseagreen", "mediumblue", "purple", "palevioletred", "pink"]
    colorlist=["#5480D0", "#F9A03F", "#9BC53D", "#C3423F", "#925DC1"]

    # Plot slots
    minX = math.floor(min(min(slotsDf["Mu"]), min(df["Mu"])))-1
    maxX = math.floor(max(max(slotsDf["Mu"]), max(df["Mu"])))+2
    g = plt.plot(slotsDf["Mu"], slotsDf["Mean Available Slots"], label="Mean Available Processing Slots", color="black")
        
    ax.legend()

    # Zero slots left
    x = [minX, maxX]
    y = [0, 0]
    g = plt.plot(x, y, color="red", label="Out of processing slots", linestyle="dashed")
    
    plt.xlim(minX, maxX)
    plt.ylim(-5000, 95000)
    #ax.set(ylim=(10, 40))
    
    # Set ticks
    ax.set_xticks(np.arange(minX-1, maxX+1, 2))

    yTicks = np.arange(0, 95000, 5000)
    yTicksLabels = ['{:}'.format(int(x)) + 'k' for x in yTicks/1000]
    ax.set_yticks(yTicks)
    ax.set_yticklabels(yTicksLabels)
    plt.title("Average mean execution time per event for most expensive algorithms as a function of pileup. Run {0}".format(runNumber))
    plt.xlabel("Integral pile-up")
    plt.ylabel("Mean Available Processing Slots")
    plt.grid()

    # Plot Algs
    ax2 = plt.twinx()
    ax2.set_yscale("log")
    colorIdx = 0
    for algName in topAlgNames:
        g2 = plt.scatter(x=df['Mu'], y=df[algName], label=algName, color=colorlist[colorIdx])

        colorIdx+=1
    
    # ax2.set(ylim=(0, 1600))
    # 
    yticks = []
    for dec in [10, 100, 1000]:
        for elem in [1, 1.25, 1.5, 2.0, 2.5, 5, 7.5]: #[1, 1.5, 2, 2.5, 3.5, 5, 7.5]:
            if elem*dec > 2500:
                break
            yticks.append(elem*dec)
        
    ax2.set_yticks(yticks)
    ax2.get_yaxis().set_major_formatter(tck.ScalarFormatter())
    ax2.set(ylabel='Mean algorithm time / event [ms]')
    
    legend_1 = ax2.legend(loc=2, borderaxespad=1.)
    legend_1.remove()
    ax.legend(loc=9, borderaxespad=1.)
    ax.add_artist(legend_1)
        
    plt.savefig('{1}/muVsTopAlgTimesAndSlots_{0}.png'.format(runNumber, outputDir))
    #plt.show()
    print("{1}/muVsTopAlgTimesAndSlots_{0}.png saved".format(runNumber, outputDir))


def main():
    from argparse import ArgumentParser
    parser = ArgumentParser()
    parser.add_argument('--timeStart', help='Time of start of range', required=True)
    parser.add_argument('--timeEnd', help='Time of end of range', required=True)
    parser.add_argument('--runNumber', help='Number of the run to process', required=True)
    parser.add_argument('--dataTag', help='Dataset tag')
    parser.add_argument('--csvPath', help='Path to directory with csv tables')
    parser.add_argument('--outputDir', help='Output direcotry to save the plot')
    args = parser.parse_args()

    if not args.csvPath and not (args.dataTag and args.runNumber):
        print("Provide run number and data tag (cost website) or path to directory with csv tables!")
        return

    # Read average mu per monitored range
    (muTRP, lbTRP, slotsTRP, eScanTRP) = readDetailsFromTRP(dateToTimestamp(args.timeStart), dateToTimestamp(args.timeEnd))
    emitScanLumiblock = prepareEmittanceScanLumiblock(eScanTRP, dateToTimestamp(args.timeStart), dateToTimestamp(args.timeEnd), lbTRP)

    lbToMuMap = getAvgPerLb(lbTRP, muTRP, emitScanLumiblock)
    lbToSlotsMap = getAvgPerLb(lbTRP, slotsTRP, emitScanLumiblock)

    csvPath = getCsvPath(args.dataTag, args.runNumber) if args.dataTag else args.csvPath
    meanSlotsDf = getAvgPerMuStep(prepareAvailableSlots(lbToMuMap, lbToSlotsMap, getCostMonitoredLumiblocks(lbToMuMap, csvPath)), "Available Slots")

    costMonRanges = prepareCostRanges(lbToMuMap.keys())

    runToRangeToMu = {}
    for run in costMonRanges.keys():
        muPerRange = getAvgPerRange(lbToMuMap)
        runToRangeToMu[run] = muPerRange

    # Read most expensive algorithms
    columns = ["Alg Total Time/Event [ms]", "Alg Total Time [s]", "Alg Total Time [%]", "ROS Data Request Time/Event [ms]"]
    sortedAlgs = []
    for monRange in costMonRanges:
        lb = monRange*50 # Convert range id to the first lumiblock number -> start of range that is used in costmon
        algData = readDataFromCostFile(csvPath, lb, columns, "Algorithm")
        if algData.empty:
            continue

        algData['Alg Total Time [s]'] = algData['Alg Total Time [s]'].astype(float)
        algData['Alg Total Time/Event [ms]'] = algData['Alg Total Time/Event [ms]'].astype(float)
        algData = algData.sort_values(by=['Alg Total Time [s]'], ascending=False)["Name"].head(5).values
        sortedAlgs.extend(algData)

    sortedAlgs.sort()
    from collections import Counter
    
    topAlgsNames = []
    for pair in Counter(sortedAlgs).most_common(5):
        topAlgsNames.append(pair[0])

    print(Counter(sortedAlgs).most_common(5))

    # Save data only for the top algs
    topAlgsData = []
    for monRange in costMonRanges:
        lb = monRange*50 # Convert range id to the first lumiblock number -> start of range that is used in costmon
        algData = readDataFromCostFile(csvPath, lb, columns, "Algorithm")
        if algData.empty:
            continue

        if getRangeId(lb) not in runToRangeToMu[run]:
            continue
        mu = runToRangeToMu[run][getRangeId(lb)]
        tmpRow = [mu]
        for algName in topAlgsNames:
            # change the name of the column to plot a different value
            tmpRow.append(float(algData[algData.Name == algName]["Alg Total Time/Event [ms]"]))

        topAlgsData.append(tmpRow)

    topAlgsDf = pd.DataFrame(topAlgsData, columns=['Mu', *topAlgsNames])

    outputDir = args.outputDir if args.outputDir else getCostDirPath(args.dataTag, args.runNumber)
    plotData(topAlgsDf, meanSlotsDf, args.runNumber, outputDir, topAlgsNames)

if __name__== "__main__":
    main()