#!/usr/bin/env python

import math
import matplotlib.pyplot as plt
import numpy as np

from Util import *

def plotDataWithReg(df, meanSlotsDf, meanAlgTimeDf, runNumber, outputDir):
    fig, ax = plt.subplots(figsize=(12, 8), dpi=80)
    #sns.set(rc={'figure.figsize':(12, 8)}, style="whitegrid")
    
    g = plt.scatter(x=df["Mu"], y=df["Available Slots"], label="Available Processing Slots")
    plt.plot(meanSlotsDf["Mu"], meanSlotsDf["Mean Available Slots"], label="Mean Available Processing Slots", color="black")
    
    # Zero slots left
    x = [min(df["Mu"]), 1+max(df["Mu"])]
    y = [0, 0]
    g = plt.plot(x, y, color="red", label="Out of processing slots", linestyle="dashed")
    
    plt.ylim(-5000, 95000)
    #ax.set(ylim=(10, 40))
    
    # Set ticks
    ax.set_xticks(np.arange(math.floor(min(df["Mu"])), 2+math.floor(max(df["Mu"])+1), 2))

    yTicks = np.arange(0, 95000, 5000)
    yTicksLabels = ['{:}'.format(int(x)) + 'k' for x in yTicks/1000]
    ax.set_yticks(yTicks)
    ax.set_yticklabels(yTicksLabels)
    plt.title("Mean global time per event as a function of pileup. Run {0}".format(runNumber))
    plt.xlabel("Integral pile-up")
    plt.ylabel("Available Processing Slots")
    plt.grid()
    
    ax2 = plt.twinx()
    g = plt.scatter(x=df['Mu'], y=df["Alg Time/Event [ms]"], label="Alg Time/Event [ms]", color = "purple")
    plt.plot(meanAlgTimeDf["Mu"], meanAlgTimeDf["Mean Alg Time/Event [ms]"], label="Mean Alg Time/Event [ms]", color="orange")
    ax2.set(ylabel='Global mean time / event [ms]')
    
    ax2.set(ylim=(0, 1250))

    h1, l1 = ax.get_legend_handles_labels()
    h2, l2 = ax2.get_legend_handles_labels()
    handles = h1+h2
    labels = l1+l2
    order=[1, 2, 0, 4, 3]
    ax.legend([handles[idx] for idx in order],[labels[idx] for idx in order], loc=6, borderaxespad=1.)
        
    plt.savefig('{1}/muVsAlgTimesAndSlots_{0}.png'.format(runNumber, outputDir))
    #plt.show()
    print("{1}/muVsAlgTimesAndSlots_{0}.png saved".format(runNumber, outputDir))


def main():
    from argparse import ArgumentParser
    parser = ArgumentParser()
    parser.add_argument('--timeStart', help='Time of start of range', required=True)
    parser.add_argument('--timeEnd', help='Time of end of range', required=True)
    parser.add_argument('--runNumber', help='Number of the run to process', required=True)
    parser.add_argument('--dataTag', help='Dataset tag')
    parser.add_argument('--csvPath', help='Path to directory with csv tables')
    parser.add_argument('--outputDir', help='Output direcotry to save the plot')
    args = parser.parse_args()

    if not args.csvPath and not (args.dataTag and args.runNumber):
        print("Provide run number and data tag (cost website) or path to directory with csv tables!")
        return

    csvPath = getCsvPath(args.dataTag, args.runNumber) if args.dataTag else args.csvPath

    (muTRP, lbTRP, slotsTRP, eScanTRP) = readDetailsFromTRP(dateToTimestamp(args.timeStart), dateToTimestamp(args.timeEnd))
    emitScanLumiblock = prepareEmittanceScanLumiblock(eScanTRP, dateToTimestamp(args.timeStart), dateToTimestamp(args.timeEnd), lbTRP)

    lbToMuMap = getAvgPerLb(lbTRP, muTRP, emitScanLumiblock)
    slotsToMuMap = getAvgPerLb(lbTRP, slotsTRP, emitScanLumiblock)

    ranges = prepareCostRanges(lbToMuMap.keys())

    wrappedData = []
    columns = ["Total Steering Time [s]", "Alg Total Time [s]", "Steering Time/Event [ms]", "Alg Time/Event [ms]", "Alg Calls/Event"]
    for lbRange in ranges:
        lb = lbRange*50 # Convert range id to the first lumiblock number -> start of range that is used in costmon
        rangeData = readDataFromCostFile(csvPath, lb, columns, "Global")
        if rangeData.empty:
            continue

        for index, row in rangeData.iterrows():
            currentLb = int(row["Name"].replace("LumiBlock_", ""))

            if currentLb not in lbToMuMap:
                continue
        
            mu = lbToMuMap[currentLb]
            slots = slotsToMuMap[currentLb]
            tmpRow = [currentLb, mu, slots]
            for colName in columns:
                tmpRow.append(float(row[colName]))

            wrappedData.append(tmpRow)

    wrappedDf = pd.DataFrame(wrappedData, columns=["LumiBlock", 'Mu', 'Available Slots', *columns])

    meanSlotsDf = getAvgPerMuStep(wrappedDf, "Available Slots")
    meanAlgTimeDf = getAvgPerMuStep(wrappedDf, "Alg Time/Event [ms]")

    outputDir = args.outputDir if args.outputDir else getCostDirPath(args.dataTag, args.runNumber)
    plotDataWithReg(wrappedDf, meanSlotsDf, meanAlgTimeDf, args.runNumber, outputDir)


if __name__== "__main__":
    main()
