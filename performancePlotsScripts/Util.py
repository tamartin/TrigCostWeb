#!/usr/bin/env python

import math
from datetime import datetime
import os.path
import pandas as pd

# Get path to csv cost directory for a run
def getCsvPath(dataTag, runNumber):
    return "{0}/csv".format(getCostDirPath(dataTag, runNumber))


# Get path on cost website to cost direcotry
def getCostDirPath(dataTag, runNumber):
    # for example /data/data23_13p6TeV/costMonitoring_data23-13p6TeV_00451618
    dataTagUnderscore = dataTag.replace("-", "_")
    return "/data/{2}/costMonitoring_{0}_00{1}".format(dataTag, runNumber, dataTagUnderscore)


# Read mu, lumiblock and available slots from TRP
def readDetailsFromTRP(startTimestamp, endTimestamp, server="https://atlasop.cern.ch"):
    try:
        import libpbeastpy
        pbeast = libpbeastpy.ServerProxy(server)

        muTRP = pbeast.get_data('OLC', 'OCLumi', 'Mu', 'OLC.OLCApp/ATLAS_PREFERRED_LBAv_PHYS', False, startTimestamp, endTimestamp, 0, True)
        
        if len(muTRP) == 0:
            print("Mu data not found")
        else:
            muTRP = muTRP[0].data["OLC.OLCApp/ATLAS_PREFERRED_LBAv_PHYS"]

        lbTRP = pbeast.get_data('ATLAS', 'LumiBlock', 'LumiBlockNumber', 'RunParams.LumiBlock', False, startTimestamp, endTimestamp, 0, True)
        
        if len(lbTRP) == 0:
            print("Lb data not found")
        else:
            lbTRP = lbTRP[0].data["RunParams.LumiBlock"]

        slotsTRP = pbeast.get_data('ATLAS', 'HLTSV', 'AvailableCores', 'DF.HLTSV.Events', False, startTimestamp, endTimestamp, 0, True)
        
        if len(slotsTRP) == 0:
            print("Slots data not found!")
        else:
            slotsTRP = slotsTRP[0].data["DF.HLTSV.Events"]

        # we don't want to include data from emittance scans
        eScan = pbeast.get_data('initial', 'Boolean', 'value', 'LHC.EmittanceScanOngoing', False, startTimestamp, endTimestamp, 0, True)
        
        if len(eScan) == 0:
            print("Emittance scan data not found!")
        else:
            eScan = eScan[0].data["LHC.EmittanceScanOngoing"]

        return (muTRP, lbTRP, slotsTRP, eScan)

    except ImportError as e:
        print("The pbeast python library was not found! Remember to setup tdaq release!")
    except RuntimeError as e:
        if "Sign in to your account" in str(e):
            print("PBeast authentication failed! Remember to export pbeast server sso: export PBEAST_SERVER_SSO_SETUP_TYPE=AutoUpdateKerberos")
        elif "cannot create CERN SSO cookie" in str(e):
            print("PBeast authentication requires the cookies, please setup")
        else:
            print("Error when reading from Pbeast! ")
            print(e)

    return ([], [], [], [])


def dateToTimestamp (dateStr) :
    datetime_object = datetime.strptime(dateStr, "%a %b %d %H:%M:%S %Y")
    return math.floor(datetime_object.timestamp()*1e6) 


# Timestamps of start and end of emittance scans
def prepareEmittanceScanLumiblock(eScanList, runStart, runEnd, lbList):
    emittanceScanRanges = []
    emittanceScanCounter = 0
    for entry in eScanList:
        if entry.value == False:
            if len(emittanceScanRanges) == 0:
                # Emittance scan started before given data range
                emittanceScanRanges.append([runStart, entry.ts])
            else:
                emittanceScanRanges[emittanceScanCounter][1] = entry.ts
            emittanceScanCounter += 1
        else: # entry.value == True
            #print("Adding new with start at {0}".format(entry.ts))
            emittanceScanRanges.append([entry.ts, 0])

    if emittanceScanRanges[-1][1] == 0:
        # Emittance scan finished after data range
        emittanceScanRanges[-1][1] = runEnd

    #print(emittanceScanRanges)

    # Convert to lumiblocks
    emittanceScanLumiBlocks = []
    emittanceScanCounter = 0
    for lbEntry in lbList:
        if emittanceScanCounter >= len(emittanceScanRanges):
            break
        if lbEntry.ts > emittanceScanRanges[emittanceScanCounter][0]:
            # We started the scan during the previous lumiblock
            emittanceScanLumiBlocks.append(lbEntry.value-1)
        if lbEntry.ts > emittanceScanRanges[emittanceScanCounter][1]:
            # We finished the scan during the previous lumiblock
            emittanceScanLumiBlocks.append(lbEntry.value)
            emittanceScanCounter+=1

    #print(emittanceScanLumiBlocks)

    return emittanceScanLumiBlocks            

def getAvgPerMuStep(df, colName):

    avgData = []
    
    muStep=math.floor(df["Mu"].min())
    stepSlots = df[(df.Mu > muStep) & (df.Mu < (muStep+1))][colName]
    avgData.append([muStep, stepSlots.mean()])

    while muStep < math.floor(df["Mu"].max()):
        stepSlots = df[(df.Mu > muStep) & (df.Mu < (muStep+5))][colName]
        avgData.append([muStep+2.5, stepSlots.mean()])
        muStep += 5

    # Include highest mu value
    muStep=math.floor(df["Mu"].max())
    stepSlots = df[(df.Mu > muStep) & (df.Mu < (muStep+1))][colName]
    avgData.append([muStep, stepSlots.mean()])
    
    return pd.DataFrame(avgData, columns=["Mu", 'Mean {0}'.format(colName)])

# Get average per one lumiblock ignoring data during emittance scans
def getAvgPerLb(lbArr, otherArr, emitScanLbs):
    otherIt = 0

    lbToOther = {}
    for i in range(0, len(lbArr)):
        lbPoint = lbArr[i]
        nextLbTs = lbArr[i+1].ts if i < (len(lbArr)-1) else -1
        # print("Current lb", lbPoint)
        # print("Next lb ts", nextLbTs)
        # print ("")
        # print(lbPoint)

        # Skip data during emittance scan
        if lbPoint.value in emitScanLbs:
            continue

        # Catch up with skipped lumiblocks
        while otherIt < len(otherArr) and otherArr[otherIt].ts < lbPoint.ts:
            otherIt+=1

        avgPerLb = 0
        counterPerLb = 0
        while otherIt < len(otherArr) and (otherArr[otherIt].ts < nextLbTs or nextLbTs == -1):
            #print (otherArr[otherIt])
            avgPerLb += otherArr[otherIt].value
            #print("     ", otherArr[otherIt])
            counterPerLb += 1
            otherIt+=1

        lbToOther[lbPoint.value] = avgPerLb/counterPerLb if counterPerLb > 0 else 0
        #print(lbToOther[lbPoint.value])
        #print("")
                
    return lbToOther

def getRangeId(lb):
    return math.floor(lb/50)


def prepareCostRanges(lbList):
    lbRanges = {}

    for lb in lbList:
        lbRange = getRangeId(lb)
        if lb not in lbRanges:
            lbRanges[lbRange] = []
        lbRanges[lbRange].append(lb)
    
    return lbRanges


def readDataFromCostFile(csvPath, lb, columns, name):
    if lb < 1000:
        filename = '{0}/Table_{2}_HLT_LumiBlock_00{1}.csv'.format(csvPath, lb, name)
    else:
        filename = '{0}/Table_{2}_HLT_LumiBlock_0{1}.csv'.format(csvPath, lb, name)

    if not os.path.isfile(filename):
        print("File {0} doesn't exist".format(filename) )
        return pd.DataFrame()
    df = pd.read_csv(filename)
    df = df[df.Name != "Algorithms name"] # Removed description
    filteredDf = df[df.Name != "Total"][["Name", *columns]]
    return filteredDf