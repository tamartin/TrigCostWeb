### Standalone use
#### Setup (22.05.2023)
```
RELEASE="23.0.26"
export SITE_NAME=CERN-PROD
export REPO=/cvmfs/atlas.cern.ch/repo
export ATLAS_LOCAL_ROOT_BASE=$REPO/ATLASLocalRootBase
source /cvmfs/atlas.cern.ch/repo/sw/tdaq/tools/cmake_tdaq/bin/cm_setup.sh tdaq-10-00-00 # libpbeastpy library
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
export LCG_RELEASE_BASE=/cvmfs/sft.cern.ch/lcg/releases
asetup Athena,$RELEASE --noLcgReleaseBase
export PATH=/cvmfs/sft.cern.ch/lcg/releases/LCG_102b/auth_get_sso_cookie/1.5.0-1/x86_64-centos7-gcc11-opt/bin:${PATH}
export PBEAST_SERVER_SSO_SETUP_TYPE=AutoUpdateKerberos # pbeast authentication
```

#### Execution
```
python TrigCostMakeTimeVsSlotsPlot.py --outputDir "." --runNumber 451611 --csvPath [/your/path] --timeStart "Sun May 7 22:43:21 2023" --timeEnd "Mon May 8 06:05:40 2023"
python TrigCostMakeMostExpensiveAlgsPlot.py  --outputDir "." --runNumber 451611 --csvPath [/your/path] --timeStart "Sun May 7 22:43:21 2023" --timeEnd "Mon May 8 06:05:40 2023"
```

`dataTag` can be replaced by providing a path to directory with cost csv files