import yaml

from constructs import Construct

from imports.kubectl.manifest import Manifest
from imports.kubernetes.service import Service, ServiceMetadata, ServiceSpecPort, ServiceSpec
from imports.kubernetes.deployment import Deployment, DeploymentMetadata, DeploymentSpec, DeploymentSpecSelector, \
    DeploymentSpecTemplate, DeploymentSpecTemplateMetadata, DeploymentSpecTemplateSpec, \
    DeploymentSpecTemplateSpecContainer, DeploymentSpecTemplateSpecContainerPort, \
    DeploymentSpecTemplateSpecContainerResources, DeploymentSpecStrategy, DeploymentSpecStrategyRollingUpdate, \
    DeploymentSpecTemplateSpecContainerReadinessProbe, DeploymentSpecTemplateSpecContainerReadinessProbeHttpGet, \
    DeploymentSpecTemplateSpecContainerLivenessProbe, DeploymentSpecTemplateSpecContainerLivenessProbeHttpGet, \
    DeploymentSpecTemplateSpecContainerEnv, DeploymentSpecTemplateSpecContainerEnvValueFrom, \
    DeploymentSpecTemplateSpecContainerEnvValueFromSecretKeyRef

from common.Environments import Environment


class TrigCostConstruct(Construct):
    """
    TrigCost construct containing all Kubernetes resources for single environment's application
    :param ns: Kubernetes namespace (name of OKD4 project in our case) of certain environment
    :param env_name: Environment name
    """

    def __init__(self,
                 scope: Construct,
                 id: str,
                 ns: str,
                 env_name: Environment
                 ):
        super().__init__(scope, id)

        host = "atlas-trig-cost.web.cern.ch" if env_name == Environment.Production else f"atlas-trig-cost-{env_name.value}.web.cern.ch"
        url = f"https://{host}" if env_name == Environment.Production else f"https://{host}"
        redirect_url = f"{url}/oauth2/callback"

        deployment_config_manifest = {
            "apiVersion": "apps.openshift.io/v1",
            "kind": "DeploymentConfig",
            "metadata": {
                "name": f"atlas-trig-cost-{env_name.value}",
                "namespace": ns
            },
            "spec": {
                "selector": {
                    "app": f"atlas-trig-cost-{env_name.value}",
                    "deploymentconfig": f"atlas-trig-cost-{env_name.value}"
                },
                "replicas": 1,
                "triggers": [
                    {
                        "type": "ConfigChange"
                    },
                    {
                        "type": "ImageChange",
                        "imageChangeParams": {
                            "automatic": True,
                            "containerNames": [
                                f"atlas-trig-cost-{env_name.value}"
                            ],
                            "from": {
                                "kind": "ImageStreamTag",
                                "namespace": ns,
                                "name": f"atlas-trig-cost-{env_name.value}:latest"
                            }
                        }
                    }
                ],
                "template": {
                    "metadata": {
                        "labels": {
                            "app": f"atlas-trig-cost-{env_name.value}",
                            "deploymentconfig": f"atlas-trig-cost-{env_name.value}"
                        },
                        "annotations": {
                            "eos.okd.cern.ch/mount-eos-with-credentials-from-secret": "eos-credentials"
                        }
                    },
                    "spec": {
                        "volumes": [
                            {
                                "name": "atlas-eos-mount",
                                "persistentVolumeClaim": {
                                    "claimName": "atlas-eos-mount"
                                }
                            },
                            {
                                "name": "cvmfs-mount",
                                "persistentVolumeClaim": {
                                    "claimName": "cvmfs-mount"
                                }
                            },
                            {
                                "name": "atlas-trig-conf-storage",
                                "persistentVolumeClaim": {
                                    "claimName": "atlas-trig-conf-storage"
                                }
                            },
                            {
                                "name": "atlas-trig-conf-storage-commands",
                                "persistentVolumeClaim": {
                                    "claimName": "atlas-trig-conf-storage-commands"
                                }
                            },
                            {
                                "name": "atlas-trig-conf-storage-links",
                                "persistentVolumeClaim": {
                                    "claimName": "atlas-trig-conf-storage-links"
                                }
                            }
                        ],
                        "containers": [
                            {
                                "name": f"atlas-trig-cost-{env_name.value}",
                                "imagePullPolicy": "Always",
                                "ports": [
                                    {
                                        "containerPort": 8080
                                    }
                                ],
                                "env": [
                                    {
                                        "name": "TRIG_COST_ENV",
                                        "value": f"{env_name.value}"
                                    },
                                ],
                                "volumeMounts": [
                                    {
                                        "name": "atlas-eos-mount",
                                        "mountPath": "/eos"
                                    },
                                    {
                                        "name": "cvmfs-mount",
                                        "mountPath": "/cvmfs",
                                        "mountPropagation": "HostToContainer"
                                    },
                                    {
                                        "name": "atlas-trig-conf-storage",
                                        "mountPath": "/data"
                                    },
                                    {
                                        "name": "atlas-trig-conf-storage-commands",
                                        "mountPath": "/var/www/html/commands"
                                    },
                                    {
                                        "name": "atlas-trig-conf-storage-links",
                                        "mountPath": "/var/www/html/data"
                                    }
                                ],
                                "resources": {
                                    "limits": {
                                        "cpu": "1000m",
                                        "memory": "3Gi"
                                    },
                                    "requests": {
                                        "cpu": "1000m",
                                        "memory": "3Gi"
                                    }
                                }
                            }
                        ],
                        "affinity": {
                            "nodeAffinity": {
                                "requiredDuringSchedulingIgnoredDuringExecution": {
                                    "nodeSelectorTerms": [
                                        {
                                            "matchExpressions": [
                                                {
                                                    "key": "cvmfs.okd.cern.ch/old-quota-limit",
                                                    "operator": "NotIn",
                                                    "values": [
                                                        "true"
                                                    ]
                                                }
                                            ]
                                        }
                                    ]
                                }
                            }
                        }
                    }
                }
            }
        }

        Manifest(
            self,
            f"{id}-deployment-config",
            yaml_body=yaml.dump(deployment_config_manifest, sort_keys=False)
        )

        Service(
            self,
            f"{id}-trig-cost-service",
            metadata=ServiceMetadata(
                labels={
                    "app": f"atlas-trig-cost-{env_name.value}",
                },
                name=f"atlas-trig-cost-{env_name.value}",
                namespace=ns
            ),
            spec=ServiceSpec(
                port=[
                    ServiceSpecPort(
                        name="8080-tcp",
                        protocol="TCP",
                        port=8080,
                        target_port="8080"
                    )
                ],
                type="ClusterIP",
                selector={
                    "deploymentconfig": f"atlas-trig-cost-{env_name.value}"
                }
            )
        )

        Service(
            self,
            f"{id}-sso-proxy-service",
            metadata=ServiceMetadata(
                name=f"atlas-trig-cost-sso-proxy-{env_name.value}",
                namespace=ns
            ),
            spec=ServiceSpec(
                port=[
                    ServiceSpecPort(
                        name="sso-proxy",
                        protocol="TCP",
                        port=4180,
                        target_port="4180"
                    )
                ],
                type="ClusterIP",
                selector={
                    "app.kubernetes.io/instance": f"atlas-trig-cost-sso-proxy-{env_name.value}"
                },
                session_affinity=None
            )
        )

        Deployment(
            self,
            f"{id}-sso-proxy-deployment",
            metadata=DeploymentMetadata(
                annotations={
                    "app.openshift.io/connects-to": f"atlas-trig-cost-{env_name.value}",
                    "image.openshift.io/triggers": '[{"from":{"kind":"ImageStreamTag","name":"oauth2-proxy:latest",'
                                                   '"namespace":"openshift"},'
                                                   '"fieldPath":"spec.template.spec.containers[?('
                                                   '@.name==\\"sso-proxy\\")].image","pause":"false"}] '
                },
                name=f"atlas-trig-cost-sso-proxy-{env_name.value}",
                namespace=ns
            ),
            spec=DeploymentSpec(
                replicas="1",
                selector=DeploymentSpecSelector(
                    match_labels={
                        "app.kubernetes.io/instance": f"atlas-trig-cost-sso-proxy-{env_name.value}",
                        "app.kubernetes.io/name": f"cern-auth-proxy"
                    }
                ),
                template=DeploymentSpecTemplate(
                    metadata=DeploymentSpecTemplateMetadata(
                        labels={
                            "app.kubernetes.io/instance": f"atlas-trig-cost-sso-proxy-{env_name.value}",
                            "app.kubernetes.io/name": f"cern-auth-proxy"
                        }
                    ),
                    spec=DeploymentSpecTemplateSpec(
                        container=[
                            DeploymentSpecTemplateSpecContainer(
                                name=f"sso-proxy",
                                env=[
                                    DeploymentSpecTemplateSpecContainerEnv(
                                        name="OAUTH2_PROXY_HTTP_ADDRESS",
                                        value=":4180"
                                    ),
                                    DeploymentSpecTemplateSpecContainerEnv(
                                        name="OAUTH2_PROXY_METRICS_ADDRESS",
                                        value=":44180"
                                    ),
                                    DeploymentSpecTemplateSpecContainerEnv(
                                        name="OAUTH2_PROXY_SILENCE_PING_LOGGING",
                                        value="true"
                                    ),
                                    DeploymentSpecTemplateSpecContainerEnv(
                                        name="OAUTH2_PROXY_REDIRECT_URL",
                                        value=redirect_url
                                    ),
                                    DeploymentSpecTemplateSpecContainerEnv(
                                        name="OAUTH2_PROXY_WHITELIST_DOMAINS",
                                        value=".cern.ch"
                                    ),
                                    DeploymentSpecTemplateSpecContainerEnv(
                                        name="OAUTH2_PROXY_ALLOWED_GROUPS",
                                        value="user"
                                    ),
                                    DeploymentSpecTemplateSpecContainerEnv(
                                        name="OAUTH2_PROXY_UPSTREAMS",
                                        value=f"http://atlas-trig-cost-{env_name.value}:8080/"
                                    ),
                                    DeploymentSpecTemplateSpecContainerEnv(
                                        name="OAUTH2_PROXY_PROXY_PREFIX",
                                        value="/oauth2"
                                    ),
                                    DeploymentSpecTemplateSpecContainerEnv(
                                        name="OAUTH2_PROXY_COOKIE_PATH",
                                        value="/"
                                    ),
                                    DeploymentSpecTemplateSpecContainerEnv(
                                        name="OAUTH2_PROXY_OIDC_GROUPS_CLAIM",
                                        value="cern_roles"
                                    ),
                                    DeploymentSpecTemplateSpecContainerEnv(
                                        name="OAUTH2_PROXY_PREFER_EMAIL_TO_USER",
                                        value="true"
                                    ),
                                    DeploymentSpecTemplateSpecContainerEnv(
                                        name="OAUTH2_PROXY_EMAIL_DOMAINS",
                                        value="*"
                                    ),
                                    DeploymentSpecTemplateSpecContainerEnv(
                                        name="OAUTH2_PROXY_SCOPE",
                                        value="openid"
                                    ),
                                    DeploymentSpecTemplateSpecContainerEnv(
                                        name="OAUTH2_PROXY_PROVIDER",
                                        value="oidc"
                                    ),
                                    DeploymentSpecTemplateSpecContainerEnv(
                                        name="OAUTH2_PROXY_OIDC_ISSUER_URL",
                                        value_from=DeploymentSpecTemplateSpecContainerEnvValueFrom(
                                            secret_key_ref=DeploymentSpecTemplateSpecContainerEnvValueFromSecretKeyRef(
                                                name="oidc-client-secret",
                                                key="issuerURL"
                                            )
                                        )
                                    ),
                                    DeploymentSpecTemplateSpecContainerEnv(
                                        name="OAUTH2_PROXY_CLIENT_ID",
                                        value_from=DeploymentSpecTemplateSpecContainerEnvValueFrom(
                                            secret_key_ref=DeploymentSpecTemplateSpecContainerEnvValueFromSecretKeyRef(
                                                name="oidc-client-secret",
                                                key="clientID"
                                            )
                                        )
                                    ),
                                    DeploymentSpecTemplateSpecContainerEnv(
                                        name="OAUTH2_PROXY_CLIENT_SECRET",
                                        value_from=DeploymentSpecTemplateSpecContainerEnvValueFrom(
                                            secret_key_ref=DeploymentSpecTemplateSpecContainerEnvValueFromSecretKeyRef(
                                                name="oidc-client-secret",
                                                key="clientSecret"
                                            )
                                        )
                                    ),
                                    DeploymentSpecTemplateSpecContainerEnv(
                                        name="OAUTH2_PROXY_COOKIE_SECRET",
                                        value_from=DeploymentSpecTemplateSpecContainerEnvValueFrom(
                                            secret_key_ref=DeploymentSpecTemplateSpecContainerEnvValueFromSecretKeyRef(
                                                name="oidc-client-secret",
                                                key="suggestedCookieSecret"
                                            )
                                        )
                                    ),
                                    DeploymentSpecTemplateSpecContainerEnv(
                                        name="OAUTH2_PROXY_COOKIE_SECURE",
                                        value="false"
                                    ),
                                    DeploymentSpecTemplateSpecContainerEnv(
                                        name="OAUTH2_PROXY_SKIP_PROVIDER_BUTTON",
                                        value="true"
                                    ),
                                ],
                                image="image-registry.openshift-image-registry.svc:5000/openshift/oauth2-proxy@sha256"
                                      ":534674a81129d95d4dc26b64e228e94600afe33854e21d0e04e3619fd90cfd60",
                                image_pull_policy="Always",
                                port=[
                                    DeploymentSpecTemplateSpecContainerPort(
                                        container_port=4180,
                                        name="http",
                                        protocol="TCP"
                                    ),
                                    DeploymentSpecTemplateSpecContainerPort(
                                        container_port=44180,
                                        name="metrics",
                                        protocol="TCP"
                                    )
                                ],
                                resources=DeploymentSpecTemplateSpecContainerResources(
                                    limits={
                                        "cpu": "100m",
                                        "memory": "100Mi"
                                    },
                                    requests={
                                        "cpu": "50m",
                                        "memory": "20Mi"
                                    }
                                ),
                                readiness_probe=DeploymentSpecTemplateSpecContainerReadinessProbe(
                                    http_get=DeploymentSpecTemplateSpecContainerReadinessProbeHttpGet(
                                        path="/ping",
                                        port="http",
                                        scheme="HTTP"
                                    ),
                                    timeout_seconds=1,
                                    period_seconds=10,
                                    success_threshold=1,
                                    failure_threshold=3
                                ),
                                termination_message_path="/dev/termination-log",
                                termination_message_policy="File",
                                liveness_probe=DeploymentSpecTemplateSpecContainerLivenessProbe(
                                    http_get=DeploymentSpecTemplateSpecContainerLivenessProbeHttpGet(
                                        path="/ping",
                                        port="http",
                                        scheme="HTTP"
                                    ),
                                    timeout_seconds=1,
                                    period_seconds=10,
                                    success_threshold=1,
                                    failure_threshold=3
                                )
                            )
                        ],
                        service_account_name="default",
                        restart_policy="Always",
                        termination_grace_period_seconds=30,
                        dns_policy="ClusterFirst"
                    )
                ),
                strategy=DeploymentSpecStrategy(
                    type="RollingUpdate",
                    rolling_update=DeploymentSpecStrategyRollingUpdate(
                        max_unavailable="25%",
                        max_surge="25%"
                    ),
                ),
                revision_history_limit=10,
                progress_deadline_seconds=600
            )
        )

        sso_proxy_oidc_return_uri_manifest = {
            "apiVersion": "webservices.cern.ch/v1alpha1",
            "kind": "OidcReturnURI",
            "metadata": {
                "name": f"atlas-trig-cost-sso-proxy-{env_name.value}",
                "namespace": ns
            },
            "spec": {
                "redirectURI": redirect_url
            }
        }

        Manifest(
            self,
            f"{id}-sso-proxy-oidc-return",
            yaml_body=yaml.dump(sso_proxy_oidc_return_uri_manifest, sort_keys=False)
        )

        route_manifest = {
            "apiVersion": "route.openshift.io/v1",
            "kind": "Route",
            "metadata": {
                "name": f"atlas-trig-cost-{env_name.value}",
                "namespace": ns,
                "labels": {
                    "app": f"atlas-trig-cost-{env_name.value}"
                },
                "annotations": {
                    "haproxy.router.openshift.io/ip_whitelist": ""
                }
            },
            "spec": {
                "host": host,
                "to": {
                    "kind": "Service",
                    "name": f"atlas-trig-cost-sso-proxy-{env_name.value}",
                    "weight": 100
                },
                "port": {
                    "targetPort": "sso-proxy"
                },
                "tls": {
                    "termination": "edge",
                    "insecureEdgeTerminationPolicy": "Redirect"
                },
                "wildcardPolicy": "None"
            }
        }

        Manifest(
            self,
            f"{id}-route-{env_name.value}",
            yaml_body=yaml.dump(route_manifest, sort_keys=False)
        )
