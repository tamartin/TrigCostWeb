import yaml

from constructs import Construct

from imports.kubectl.manifest import Manifest


class TrigCostImageStreamsCommonConstruct(Construct):
    """
    TrigCostImageStreamsCommon construct containing all ImageStreams, which are needed for all environments
    :param ns: Kubernetes namespace (name of OKD4 project in our case) of certain environment
    """
    def __init__(self,
                 scope: Construct,
                 id: str,
                 ns: str
                 ):
        super().__init__(scope, id)

        alma9_docker_php_nginx_image_stream_manifest = {
            "apiVersion": "image.openshift.io/v1",
            "kind": "ImageStream",
            "metadata": {
                "name": "alma9-docker-php-nginx",
                "namespace": ns
            },
            "spec": {
                "lookupPolicy": {
                    "local": False
                },
                "tags": [
                    {
                        "name": "latest",
                        "from": {
                            "kind": "DockerImage",
                            "name": "aporeba/alma9-docker-php-nginx:latest"
                        },
                        "generation": 2,
                        "importPolicy": {
                            "scheduled": True
                        },
                        "referencePolicy": {
                            "type": "Source"
                        }
                    }
                ]
            }
        }

        Manifest(
            self,
            f"{id}-alma9-docker-php-nginx-image-stream",
            yaml_body=yaml.dump(alma9_docker_php_nginx_image_stream_manifest, sort_keys=False)
        )
