import yaml

from constructs import Construct

from imports.kubectl.manifest import Manifest

from common.Environments import Environment
from common.utils import Quoted, quoted_presenter

yaml.add_representer(Quoted, quoted_presenter)

 
class TrigCostImageStreamsConstruct(Construct):
    """
    TrigCostImageStreams construct containing all ImageStreams for single environment
    :param ns: Kubernetes namespace (name of OKD4 project in our case) of certain environment
    :param env_name: Environment name
    :param branch_name: Name of branch, which will be used as source for Docker image
    """
    def __init__(self,
                 scope: Construct,
                 id: str,
                 ns: str,
                 env_name: Environment,
                 branch_name: str
                 ):
        super().__init__(scope, id)

        trig_cost_image_stream_manifest = {
            "apiVersion": "image.openshift.io/v1",
            "kind": "ImageStream",
            "metadata": {
                "name": f"atlas-trig-cost-{env_name.value}",
                "namespace": ns
            }
        }

        Manifest(
            self,
            f"{id}-image-stream",
            yaml_body=yaml.dump(trig_cost_image_stream_manifest, sort_keys=False)
        )

        trig_cost_build_config_manifest = {
            "apiVersion": "build.openshift.io/v1",
            "kind": "BuildConfig",
            "metadata": {
                "name": f"atlas-trig-cost-{env_name.value}",
                "namespace": ns
            },
            "spec": {
                "output": {
                    "to": {
                        "kind": "ImageStreamTag",
                        "name": f"atlas-trig-cost-{env_name.value}:latest"
                    }
                },
                "resources": {
                    "requests": {
                        "cpu": "1",
                        "memory": "1Gi"
                    },
                    "limits": {
                        "cpu": "1",
                        "memory": "2Gi"
                    }
                },
                "strategy": {
                    "type": "Docker",
                    "dockerStrategy": {
                        "from": {
                            "kind": "ImageStreamTag",
                            "name": "alma9-docker-php-nginx:latest"
                        }
                    }
                },
                "source": {
                    "type": "Git",
                    "git": {
                        "uri": "ssh://git@gitlab.cern.ch:7999/tamartin/TrigCostWeb.git",
                        "ref": branch_name
                    },
                    "sourceSecret": {
                        "name": "sshdeploykey"
                    }
                },
                "triggers": [
                    {
                        "type": "ImageChange"
                    },
                    {
                        "type": "ConfigChange"
                    },
                    {
                        "type": Quoted("GitLab"),
                        "gitlab": {
                            "secretReference": {
                                "name": Quoted(f"gitlab-push-webhook-{env_name.value}")
                            }
                        }
                    }
                ]
            }
        }

        Manifest(
            self,
            f"{id}-build-config",
            yaml_body=yaml.dump(trig_cost_build_config_manifest, sort_keys=False)
        )
