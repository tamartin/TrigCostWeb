import os

from cdktf import TerraformStack
from constructs import Construct

from applications.TrigCostImageStreamsCommon import TrigCostImageStreamsCommonConstruct

from imports.kubectl.provider import KubectlProvider
from imports.kubernetes.provider import KubernetesProvider

from volumes.TrigCostVolumes import TrigCostVolumes

token_key = f"CERN_ATLAS_TRIG_COST_OKD_DEPLOY_TOKEN"


class TrigCostCommonStack(TerraformStack):
    """
    TrigCostCommon stack containing all Kubernetes resources for all environments
    :param ns: Kubernetes namespace (name of OKD4 project in our case) of certain environment
    """
    def __init__(
            self,
            scope: Construct,
            ns: str,
    ):
        super().__init__(scope, f"{ns}-common")

        # Provider for Kubernetes on OKD4
        KubernetesProvider(self, f"{ns}-common-k8s-provider",
                           host="https://api.paas.okd.cern.ch",
                           token=os.environ[token_key]
                           )

        # Provider for kubectl on OKD4 (needed for custom resources)
        KubectlProvider(self, f"{ns}-common-kubectl-provider",
                        host="https://api.paas.okd.cern.ch",
                        load_config_file=False,
                        token=os.environ[token_key]
                        )

        TrigCostImageStreamsCommonConstruct(
            self,
            f"{ns}-common-image-streams",
            ns=ns
        )

        TrigCostVolumes(
            self,
            f"{ns}-common-volumes",
            ns=ns
        )
