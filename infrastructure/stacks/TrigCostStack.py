import os
from typing import Dict

from cdktf import TerraformStack
from constructs import Construct

from applications.TrigCostImageStreams import TrigCostImageStreamsConstruct
from applications.TrigCost import TrigCostConstruct

from common.Environments import Environment

from imports.kubectl.provider import KubectlProvider
from imports.kubernetes.provider import KubernetesProvider

token_key = f"CERN_ATLAS_TRIG_COST_OKD_DEPLOY_TOKEN"


class TrigCostStack(TerraformStack):
    """
    TrigCost stack containing all Kubernetes resources for single environment
    :param ns: Kubernetes namespace (name of OKD4 project in our case) of certain environment
    :param env_name: Environment name
    :param configuration: Environment configuration for current stack
    """
    def __init__(
            self,
            scope: Construct,
            ns: str,
            env_name: Environment,
            configuration: Dict[str, any],
    ):
        super().__init__(scope, f"{env_name.value}-{ns}")

        # Provider for Kubernetes on OKD4
        KubernetesProvider(self, f"{env_name.value}-{ns}-k8s-provider",
                           host="https://api.paas.okd.cern.ch",
                           token=os.environ[token_key]
                           )

        # Provider for kubectl on OKD4 (needed for custom resources)
        KubectlProvider(self, f"{env_name.value}-{ns}-kubectl-provider",
                        host="https://api.paas.okd.cern.ch",
                        load_config_file=False,
                        token=os.environ[token_key]
                        )

        # TrigCostImageStreams
        TrigCostImageStreamsConstruct(
            self, f"{env_name.value}-{ns}-image-streams",
            ns=ns,
            env_name=env_name,
            branch_name=configuration["branch_name"]
        )

        # TrigCost construct
        TrigCostConstruct(
            self, f"{env_name.value}-{ns}-construct",
            ns=ns,
            env_name=env_name
        )
