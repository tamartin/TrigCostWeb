from constructs import Construct

from imports.kubernetes.persistent_volume_claim import PersistentVolumeClaim, PersistentVolumeClaimMetadata, \
    PersistentVolumeClaimSpec, PersistentVolumeClaimSpecResources


class TrigCostVolumes(Construct):
    """
    TrigCostVolumes construct containing all other resources for single environment
    :param ns: Kubernetes namespace (name of OKD4 project in our case) of certain environment
    """
    def __init__(self,
                 scope: Construct,
                 id: str,
                 ns: str
                 ):
        super().__init__(scope, id)

        PersistentVolumeClaim(
            self,
            f"{ns}-cvmfs-mount-claim",
            metadata=PersistentVolumeClaimMetadata(
                name="cvmfs-mount",
                namespace=ns
            ),
            spec=PersistentVolumeClaimSpec(
                access_modes=[
                    "ReadOnlyMany"
                ],
                storage_class_name="cvmfs",
                resources=PersistentVolumeClaimSpecResources(
                    requests={
                        "storage": "1Mi"
                    }
                )
            )
        )

        PersistentVolumeClaim(
            self,
            f"{ns}-atlas-eos-mount-claim",
            metadata=PersistentVolumeClaimMetadata(
                name="atlas-eos-mount",
                namespace=ns
            ),
            spec=PersistentVolumeClaimSpec(
                access_modes=[
                    "ReadWriteMany"
                ],
                storage_class_name="eos",
                resources=PersistentVolumeClaimSpecResources(
                    requests={
                        "storage": "1Mi"
                    }
                )
            )
        )

        PersistentVolumeClaim(
            self,
            f"{ns}-atlas-trig-conf-storage-claim",
            metadata=PersistentVolumeClaimMetadata(
                name="atlas-trig-conf-storage",
                namespace=ns
            ),
            spec=PersistentVolumeClaimSpec(
                access_modes=[
                    "ReadWriteMany"
                ],
                resources=PersistentVolumeClaimSpecResources(
                    requests={
                        "storage": "1499Gi"
                    }
                )
            )
        )

        PersistentVolumeClaim(
            self,
            f"{ns}-atlas-trig-conf-storage-commands-claim",
            metadata=PersistentVolumeClaimMetadata(
                name="atlas-trig-conf-storage-commands",
                namespace=ns
            ),
            spec=PersistentVolumeClaimSpec(
                access_modes=[
                    "ReadWriteMany"
                ],
                resources=PersistentVolumeClaimSpecResources(
                    requests={
                        "storage": "50Mi"
                    }
                )
            )
        )

        PersistentVolumeClaim(
            self,
            f"{ns}-atlas-trig-conf-storage-links-claim",
            metadata=PersistentVolumeClaimMetadata(
                name="atlas-trig-conf-storage-links",
                namespace=ns
            ),
            spec=PersistentVolumeClaimSpec(
                access_modes=[
                    "ReadWriteMany"
                ],
                resources=PersistentVolumeClaimSpecResources(
                    requests={
                        "storage": "50Mi"
                    }
                )
            )
        )
