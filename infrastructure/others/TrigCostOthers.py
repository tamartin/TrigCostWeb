import yaml

from constructs import Construct

from imports.kubectl.manifest import Manifest

from common.Environments import Environment


class TrigCostOthers(Construct):
    """
    TrigCostOthers construct containing all other resources for single environment
    :param ns: Kubernetes namespace (name of OKD4 project in our case) of certain environment
    :param env_name: Environment name
    """
    def __init__(self,
                 scope: Construct,
                 id: str,
                 ns: str,
                 env_name: Environment,
                 ):
        super().__init__(scope, id)

        redirect_uri = "https://atlas-trig-cost.web.cern.ch/oauth2/callback" if env_name == Environment.Production else f"https://atlas-trig-cost-{env_name.value}.web.cern.ch/oauth2/callback"

        oidc_return_uri_manifest = {
            "apiVersion": "webservices.cern.ch/v1alpha1",
            "kind": "OidcReturnURI",
            "metadata": {
                "name": f"atlas-trig-cost-{env_name.value}-return-uri",
                "namespace": ns,
            },
            "spec": {
                "redirectURI": redirect_uri
            }
        }

        Manifest(
            self,
            f"{id}-redirect-uri",
            yaml_body=yaml.dump(oidc_return_uri_manifest, sort_keys=False)
        )
