from enum import Enum


class Environment(Enum):
    Dev = "dev"
    Production = "prod"
