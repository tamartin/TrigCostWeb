#!/usr/bin/env python
import os

from cdktf import App, S3Backend

from common.Environments import Environment

from stacks.TrigCostCommonStack import TrigCostCommonStack
from stacks.TrigCostStack import TrigCostStack

# Public configuration of all environments
env_configurations = {
    Environment.Dev: {
        "branch_name": "dev-okd"
    },
    Environment.Production: {
        "branch_name": "master-okd"
    }
}
ns = "atlas-trig-cost"  # Name of OKD4 project

app = App()

stack_image_streams = TrigCostCommonStack(
    app,
    ns=ns
)

stack_image_streams_s3_backend = S3Backend(
    stack_image_streams,
    access_key=os.environ["CERN_S3_ATLAS_ACCESS_KEY"],
    secret_key=os.environ["CERN_S3_ATLAS_SECRET_KEY"],
    bucket="atlas-trig-cost-infrastructure",
    region="main",
    endpoint="https://s3.cern.ch",
    key=f"atlas-trig-cost-common.tfstate",
    skip_credentials_validation=True,
    skip_metadata_api_check=True,
)
stack_image_streams_s3_backend.add_override("skip_region_validation", True)

for environment in Environment:
    stack = TrigCostStack(
        app,
        ns=ns,
        env_name=environment,
        configuration=env_configurations[environment]
    )

    stack_s3_backend = S3Backend(
        stack,
        access_key=os.environ["CERN_S3_ATLAS_ACCESS_KEY"],
        secret_key=os.environ["CERN_S3_ATLAS_SECRET_KEY"],
        bucket="atlas-trig-cost-infrastructure",
        region="main",
        endpoint="https://s3.cern.ch",
        key=f"atlas-trig-cost-{environment.value}.tfstate",
        skip_credentials_validation=True,
        skip_metadata_api_check=True,
    )
    stack_s3_backend.add_override("skip_region_validation", True)

app.synth()
