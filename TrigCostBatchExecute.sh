# This script runs the analysis
echo "Execute] Filelist to process is: $1" # Must be supplied
echo "Execute] Directory to output is: $2" # Must be supplied
echo "Execute] Tag is: $3"
echo "Execute] Details is: $4"

################################################## ##################################################
HOME=/data/trig-cost-sw
################################################## ##################################################

if [ -z "$1" ]; then # No run dir - fatal
  echo "Execute] No file list supplied, exit."
  exit
fi

if [ -z "$2" ]; then # No our dir - fatal
  echo "Execute] No output directory supplied, exit."
  exit
fi

# Go home
cd $HOME

#setup eos
# echo "Execute] Setup EOS"
# export EOS_MGM_URL=root://eosatlas.cern.ch

# kinit -5 attradm@CERN.CH -k -t $HOME/keytabs/attradm.keytab # Authenticate

# Setup release
RELEASE="24.0.69"
export SITE_NAME=CERN-PROD
export REPO=/cvmfs/atlas.cern.ch/repo
export ATLAS_LOCAL_ROOT_BASE=$REPO/ATLASLocalRootBase
echo "Execute] Source setup tdaq-11-02-00"
source /cvmfs/atlas.cern.ch/repo/sw/tdaq/tools/cmake_tdaq/bin/cm_setup.sh tdaq-11-02-00 # libpbeastpy library
echo "Execute] Source atlasLocalSetup.sh"
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
echo "Execute] Setup athena"
asetup Athena,$RELEASE
export PBEAST_SERVER_SSO_SETUP_TYPE=AutoUpdateKerberos # pbeast authentication

# Retrieve run number from the first file name
COST_FILE=$(head -n 1 $1)
RUN_NUMBER=$(basename $COST_FILE | cut -d '.' -f2)

COST_DIR="$2costMonitoring_$3_$RUN_NUMBER"

# Create new directory
echo "Execute] NEW RUN DIRECTORY: $COST_DIR"
mkdir -p "$COST_DIR/csv"

# Copy merged ntuple
cp $COST_FILE $COST_DIR/TrigCostRoot_Results.root 

# Run postprocessing
echo "Execute] RUN POSTPROCESSING"

CostAnalysisPostProcessing.py --trpDetails=True --file $COST_DIR/TrigCostRoot_Results.root --userDetails "$4"

# Move the results
mv Table* $COST_DIR/csv 
mv metadata.json $COST_DIR
