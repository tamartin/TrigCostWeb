#!/bin/bash
sleep 30
# Remove EOS workaround as test
nohup bash -c /var/www/html/TrigCostEOSCredentialsCron.sh &
if [ $TRIG_COST_ENV == "prod" ]; then
  nohup bash -c /var/www/html/TrigCostBatchCronReplacement.sh &
  nohup bash -c /var/www/html/TrigCostBatchCronReplacementManual.sh &
fi
